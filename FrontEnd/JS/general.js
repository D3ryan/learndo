$(document).ready(function () {
    /* Proceso de cargar y cambiar una imagen en un input */

    var reader = new FileReader();
    var imgToChange;

    reader.onload = function (e) {
        $(imgToChange).attr("src", e.target.result);
    }

    function readURL(input) { 
        if (input.files && input.files[0]) {
            reader.readAsDataURL(input.files[0]);
        }  


    }

    $("#formFile").change(function (e) { 
        e.preventDefault();
        imgToChange = $("#df_img");
        readURL(this);
    });

    $("#formFileSchool").change(function (e) { 
        e.preventDefault();
        imgToChange = $("#df_school-img");
        readURL(this);
    });

    /* -------------------------------------------------- */
});
