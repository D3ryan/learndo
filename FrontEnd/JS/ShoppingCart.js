$(document).ready(function () {

    $("#df_completeCourse").click(function (e) { 
        toggleLevelsCheckbox($("#df_completeCourse"));
        calculateTotalPrice();
    });

    $("input[name='levels[]']").click(function (e) {
        checkLevelCheckboxes();
        calculateTotalPrice();
    });

    $(".df_checkbox-label").click(function (e) { 
        e.preventDefault();
    });

});

function toggleLevelsCheckbox() { 
    let levelsCheckbox = $("input[name='levels[]']");
    if($("#df_completeCourse").prop("checked")){
        for (const level of levelsCheckbox) {
            $(level).prop("checked", false);
            $(level).hide();
        }
    } else {
        for (const level of levelsCheckbox) {
            $(level).show();
        }
    }
}

function checkLevelCheckboxes() {
    let levelsCheckbox = $("input[name='levels[]']");
    let levelsChecked = 0;

    for (const level of levelsCheckbox) {
        if(level.checked){
            levelsChecked++;
        }
    }

    if(levelsChecked > 0){
        $("#df_completeCourse").prop("checked", false);
        $("#df_completeCourse").hide();
    }
    else{
        $("#df_completeCourse").show();
    }
}


function calculateTotalPrice() {
    let checkboxes = $("input[type='checkbox']");
    let sum = 0;
    for (const checkbox of checkboxes) {
        if(checkbox.checked){
            sum += parseInt($(checkbox).attr('levelprice'));
        }
    }
    $("#df_totalPrice").text(`$${sum.toString()} MXN`);
}