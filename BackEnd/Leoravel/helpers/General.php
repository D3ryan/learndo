<?php

abstract class Rol
{
    const Escuela = 1;
    const Estudiante = 2;
}

class General
{

    public function __construct()
    {
    }

    static public function startSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    static public function getPerfilEditadoCorrectamente()
    {
        self::startSession();
        $perfilEditadoCorrectamente = null;
        if (isset($_SESSION["perfilEditadoCorrectamente"])) {
            $perfilEditadoCorrectamente = $_SESSION["perfilEditadoCorrectamente"];
        }
        return $perfilEditadoCorrectamente;
    }

    static public function setPerfilEditadoCorrectamente($perfilEditadoCorrectamente)
    {
        self::startSession();
        $_SESSION["perfilEditadoCorrectamente"] = $perfilEditadoCorrectamente;
    }



    static public function isTipoRol($TipoRol)
    {
        self::startSession();
        $isTipoRol = false;
        if (isset($_SESSION["IdUsuarioActivo"]) && isset($_SESSION["RolUsuarioActivo"])) {
            if ($_SESSION["RolUsuarioActivo"] == $TipoRol) {
                $isTipoRol = true;
            }
        }
        return $isTipoRol;
    }
static public function validarPorExtensiones($extensiones,$filename){
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if (!in_array($ext, $extensiones)) {
        return false;
    }else{
        return true;
    }
}
    static public function isUsuarioActivo($UsuarioActivo)
    {
        self::startSession();
        $isUsuarioActivo = false;
        if (isset($_SESSION["IdUsuarioActivo"]) && isset($_SESSION["RolUsuarioActivo"])) {
            if ($_SESSION["IdUsuarioActivo"] == $UsuarioActivo) {
                $isUsuarioActivo = true;
            }
        }
        return $isUsuarioActivo;
    }

    static public function getIdUsuarioActivo()
    {
        self::startSession();
        $IdUsuarioActivo = -1;
        if (isset($_SESSION["IdUsuarioActivo"]) && isset($_SESSION["RolUsuarioActivo"])) {
            $IdUsuarioActivo = $_SESSION["IdUsuarioActivo"];
        }
        return $IdUsuarioActivo;
    }


    static public function getRolUsuarioActivo()
    {
        self::startSession();
        $RolUsuarioActivo = -1;
        if (isset($_SESSION["IdUsuarioActivo"]) && isset($_SESSION["RolUsuarioActivo"])) {
            $RolUsuarioActivo = $_SESSION["RolUsuarioActivo"];
        }
        return $RolUsuarioActivo;
    }

    static public function setIdUsuarioActivo($IdUsuarioActivo)
    {
        self::startSession();
        $_SESSION["IdUsuarioActivo"] = $IdUsuarioActivo;
    }


    static public function setRolUsuarioActivo($RolUsuarioActivo)
    {
        self::startSession();
        $_SESSION["RolUsuarioActivo"] = $RolUsuarioActivo;
    }

    static public function isSetUsuarioActivo()
    {
        self::startSession();
        $isSetUsuarioActivo = false;
        if (isset($_SESSION["IdUsuarioActivo"]) && isset($_SESSION["RolUsuarioActivo"])) {
            $isSetUsuarioActivo = true;
        }
        return $isSetUsuarioActivo;
    }

    static public function isExistingRow($row, $rowName)
    {
        $Field = null;
        if (isset($row[$rowName]))
            $Field = $row[$rowName];
        return $Field;
    }


    static public function isInteger($number){
        return !is_int($number) ? (ctype_digit($number)) : true;
    }

    static public function isPositiveNumber($number){
        if (is_numeric($number)){
            if ($number >= 0){
                return true;
            }
        }
        return false;
    }
}
