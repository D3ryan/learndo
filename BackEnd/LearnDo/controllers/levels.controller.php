<?php

    require_once "controllers/template.controller.php";
    require_once "models/LevelModel.php";
    require_once "models/PDFFilesModel.php";
    require_once "models/ImagesModel.php";
    require_once "models/UsersBuyLevelsModel.php";
    require_once "helpers/action.php";
    require_once "helpers/files.php";
    require_once "helpers/userSession.php";
    require_once "helpers/validations.php";

class LevelsController extends Controller {
    // CONTROLLER ROUTE
    public const ROUTE = "levels";

    //ACTIONS ROUTE
    public const INDEX = "index";
    public const CREATE = "create";
    public const PROGRESS = "progress";
    public const EDIT_LEVELS = "editlevels";


    public $actions;
    
    public function __construct(){
        //parent::__construct($pathName);
        $this->actions = array(self::INDEX => new Action("Index", null),
                                self::CREATE => new Action("CreateLevels", "CreateLevelsPost"),
                                self::PROGRESS => new Action(null, "UpdateProgressPost"),
                                self::EDIT_LEVELS => new Action("EditLevels", "EditLevelsPost"));
    }

    public function ShowContent($paths) {
        //Determine wich method call
        Action::ValidateActionsPath($paths, $this);
    }

    public function Index($paths) {     
        // include "views/users/index.php";
    }

    public function EditLevels(){
        $idCourse = UserSession::getCourseId();
        $levels = LevelModel::getLevelsByIdCourseForEdition($idCourse);
        include "views/levels/editlevels.php";
    }

    public function EditLevelsPost(){
        try {

            $idCourse = UserSession::getCourseId();
            $level = new LevelModel();
            $level->setIdCourse($idCourse);
            
            if(!Validations::validateNull($_POST["leveltitle"]) && !Validations::validateNull($_POST["leveldescription"]) 
            && !Validations::validateNull($_FILES["videolevel"]) && !Validations::validateNull($_POST["price"])){
                
                $numberOfLevels = count($_POST["leveltitle"]);
                $IdLevels = $_POST["IdLevel"]; 
                $levelsTitle = $_POST["leveltitle"];
                $levelsDescription = $_POST["leveldescription"];
                $levelsLinks = $_POST["levellinks"];
                
                $files_photos = File::reArrayFiles($_FILES["multimediafiles"]);
                $files_pdf = File::reArrayFiles($_FILES["pdffiles"]);

                $levelsVideo = File::reArrayFiles($_FILES["videolevel"]);
                $levelsPrice = $_POST["price"];

                for ($i=0; $i < $numberOfLevels; $i++) { 
                    
                    //Checamos si el id del nivel esta vacio, si no lo esta, lo seteamos al modelo del nivel
                    //si esta vacio entonces seteamos nulo 
                    if(!empty($IdLevels[$i])) $level->setIdlevel($IdLevels[$i]);
                    else $level->setIdlevel(null);

                    if(File::ValidateVideoType($levelsVideo[$i])){
                        $urlVideo = File::UploadVideo($levelsVideo[$i]);
                        $level->setUrlVideo($urlVideo);
                    } else Validations::throwInputError();

                    !Validations::validateNullEmptyString($levelsTitle[$i]) ? 
                    $level->setTitle($levelsTitle[$i]) : 
                    Validations::throwInputError();

                    !Validations::validateNullEmptyString($levelsDescription[$i]) ?
                    $level->setTextlevel($levelsDescription[$i]) : 
                    Validations::throwInputError();

                    !Validations::validateNumeric($levelsPrice[$i]) ?
                    $level->setPrice($levelsPrice[$i]) :
                    Validations::throwInputError();

                    // !Validations::validateNullEmptyString($levelsLinks[$i]) ? 
                    // $level->settextLinks($levelsLinks[$i]) :
                    // Validations::throwInputError();

                    if(isset($levelsLinks)) $level->settextLinks($levelsLinks[$i]);
                    
                    //Updateamos e insertamos segun sea el caso
                    $level->UpdateInsertLevels();

                    //Si en el modelo del nivel el id esta nulo entonces insertamos un nivel nuevo y necesitamos traernoslo de la bd
                    //Si no esta en nulo entonces quiere decir que updeateamos el nivel y el id lo tenemos a disposicion
                    if(is_null($level->getIdlevel())) $insertedlevel = LevelModel::getIdlevelByIdCourse($level->getIdCourse());
                    else  $insertedlevel = $level;

                    //si no se subio ningun archivo o el nivel no esta seteado entonces no se hace esto
                    //Esto se valida por cada nivel
                    if($files_pdf[$i]["error"][0] != UPLOAD_ERR_NO_FILE && isset($insertedlevel)){

                        $pdf = new PDFFilesModel();
                        $level_files_pdf = File::reArrayFiles($files_pdf[$i]);

                        $pdf->setIdLevel($insertedlevel->getIdlevel());
                        
                        foreach ($level_files_pdf as $file_pdf) {
                           
                            if(File::ValidatePDFType($file_pdf)) {
                                $pdf->setNameFile($file_pdf["name"]); 
                                $urlPDF = File::UploadPDF($file_pdf);
                                $pdf->setUrlFile($urlPDF);
                            } else Validations::throwInputError();
    
                            $pdf->InsertPDFFile();
    
                        }
                    }
                
                    //si no se subio ningun archivo o el nivel no esta seteado entonces no se hace esto
                    //Esto se valida por cada nivel
                    if($files_photos[$i]["error"][0] != UPLOAD_ERR_NO_FILE && isset($insertedlevel)){
                        $level_files_photos = File::reArrayFiles($files_photos[$i]);
                        $image = new ImagesModel();
                        $image->setIdLevel($insertedlevel->getIdlevel());

                        foreach($level_files_photos as $file_photo){
                            if(File::ValidateType($file_photo)){
                                $binaryImage = File::MakeFileToBinary($file_photo);
                                $image->setImage($binaryImage);
                            } else Validations::throwInputError();
    
                            $image->InsertImage();
                        }
                    }

                }
                
                ob_end_clean();
                header("Location: " . Template::Route(CoursesController::ROUTE, CoursesController::SHOW_ALL_COURSES_FABRIC)); 
                
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function UpdateProgressPost(){
        $user = UserSession::getCurrentUserId();
        $level = $_POST["IdLevel"];
        if(isset($user) && isset($level)){
            
            $userprogress = new UsersBuyLevelsModel();
            $userprogress->setIdLevel($level);
            $userprogress->setIdUser($user);
            $userprogress->setProgress(true);

            $userprogress->updateProgress();

            $response = array("result" => "true");
        }

        ob_end_clean();
        $json_response = json_encode($response);
        echo $json_response;
        die();
    }

    public function CreateLevels(){
        include "views/levels/createlevels.php";
    }

    public function CreateLevelsPost(){

        try {
            
            $level = new LevelModel();
            $level->setIdCourse(UserSession::getCourseId());
            
            if(!Validations::validateNull($_POST["leveltitle"]) && !Validations::validateNull($_POST["leveldescription"]) 
            && !Validations::validateNull($_FILES["videolevel"]) && !Validations::validateNull($_POST["price"])){
                
                $numberOfLevels = count($_POST["leveltitle"]);
                $levelsTitle = $_POST["leveltitle"];
                $levelsDescription = $_POST["leveldescription"];
                $levelsLinks = $_POST["levellinks"];
                
                $files_photos = File::reArrayFiles($_FILES["multimediafiles"]);
                $files_pdf = File::reArrayFiles($_FILES["pdffiles"]);

                $levelsVideo = File::reArrayFiles($_FILES["videolevel"]);
                $levelsPrice = $_POST["price"];

                for ($i=0; $i < $numberOfLevels; $i++) { 

                    if(File::ValidateVideoType($levelsVideo[$i])){
                        $urlVideo = File::UploadVideo($levelsVideo[$i]);
                        $level->setUrlVideo($urlVideo);
                    } else Validations::throwInputError();

                    !Validations::validateNullEmptyString($levelsTitle[$i]) ? 
                    $level->setTitle($levelsTitle[$i]) : 
                    Validations::throwInputError();

                    !Validations::validateNullEmptyString($levelsDescription[$i]) ?
                    $level->setTextlevel($levelsDescription[$i]) : 
                    Validations::throwInputError();

                    !Validations::validateNumeric($levelsPrice[$i]) ?
                    $level->setPrice($levelsPrice[$i]) :
                    Validations::throwInputError();

                    // !Validations::validateNullEmptyString($levelsLinks[$i]) ? 
                    // $level->settextLinks($levelsLinks[$i]) :
                    // Validations::throwInputError();

                    if(isset($levelsLinks)) $level->settextLinks($levelsLinks[$i]);

                    $level->InsertLevel();

                    $insertedlevel = LevelModel::getIdlevelByIdCourse($level->getIdCourse());

                    //si no se subio ningun archivo o el nivel no esta seteado entonces no se hace esto
                    //Esto se valida por cada nivel
                    if($files_pdf[$i]["error"][0] != UPLOAD_ERR_NO_FILE && isset($insertedlevel)){

                        $pdf = new PDFFilesModel();
                        $level_files_pdf = File::reArrayFiles($files_pdf[$i]);

                        $pdf->setIdLevel($insertedlevel->getIdlevel());
                        
                        foreach ($level_files_pdf as $file_pdf) {
                           
                            if(File::ValidatePDFType($file_pdf)) {
                                $pdf->setNameFile($file_pdf["name"]); 
                                $urlPDF = File::UploadPDF($file_pdf);
                                $pdf->setUrlFile($urlPDF);
                            } else Validations::throwInputError();
    
                            $pdf->InsertPDFFile();
    
                        }
                    }
                
                    //si no se subio ningun archivo o el nivel no esta seteado entonces no se hace esto
                    //Esto se valida por cada nivel
                    if($files_photos[$i]["error"][0] != UPLOAD_ERR_NO_FILE && isset($insertedlevel)){
                        $level_files_photos = File::reArrayFiles($files_photos[$i]);
                        $image = new ImagesModel();
                        $image->setIdLevel($insertedlevel->getIdlevel());

                        foreach($level_files_photos as $file_photo){
                            if(File::ValidateType($file_photo)){
                                $binaryImage = File::MakeFileToBinary($file_photo);
                                $image->setImage($binaryImage);
                            } else Validations::throwInputError();
    
                            $image->InsertImage();
                        }
                    }

                    // $level_files_pdf = File::reArrayFiles($files_pdf[$i]);
                    // $level_files_photos = File::reArrayFiles($files_photos[$i]);

                    // $pdf = new PDFFilesModel();
                    // $image = new ImagesModel();

                    // $insertedlevel = LevelModel::getIdlevelByIdCourse($level->getIdCourse());

                    // if(isset($insertedlevel)){
                    //     $pdf->setIdLevel($insertedlevel->getIdlevel());
                    //     $image->setIdLevel($insertedlevel->getIdlevel());
                    // }
                    
                    // foreach ($level_files_pdf as $file_pdf) {
                       
                    //     if(File::ValidatePDFType($file_pdf)) {
                    //         $pdf->setNameFile($file_pdf["name"]); 
                    //         $urlPDF = File::UploadPDF($file_pdf);
                    //         $pdf->setUrlFile($urlPDF);
                    //     } else Validations::throwInputError();

                    //     $pdf->InsertPDFFile();

                    // }

                    // foreach($level_files_photos as $file_photo){
                    //     if(File::ValidateType($file_photo)){
                    //         $binaryImage = File::MakeFileToBinary($file_photo);
                    //         $image->setImage($binaryImage);
                    //     } else Validations::throwInputError();

                    //     $image->InsertImage();
                    // }

                }
                
                ob_end_clean();
                header("Location: " . Template::Route(CoursesController::ROUTE, CoursesController::SHOW_ALL_COURSES_FABRIC)); 
            
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
        

    }
}