<?php

    require_once "controllers/template.controller.php";
    require_once "models/CourseModel.php";
    require_once "models/CoursesCategoryModel.php";
    require_once "models/CommentariesModel.php";
    require_once "models/UsersBuyCoursesModel.php";
    require_once "models/UsersBuyLevelsModel.php";
    require_once "models/UsersRegisterCoursesModel.php";
    require_once "helpers/action.php";
    require_once "helpers/files.php";
    require_once "helpers/userSession.php";
    require_once "helpers/validations.php";
    require_once "FPDF/fpdf.php";

class CoursesController extends Controller {
    // CONTROLLER ROUTE
    public const ROUTE = "courses";

    //ACTIONS ROUTE
    public const INDEX = "index";
    public const CREATE = "create";
    public const SHOW_ALL_COURSES_FABRIC = "showfabric";
    public const CREATE_CATEGORY = "category";
    public const SHOPPING_CART = "shoppingcart";
    public const MY_COURSES = "mycourses";
    public const VISUALIZE_COURSE = "visualize";
    public const SEARCH_COURSE = "search";
    public const DETAIL_COURSE = "detail";
    public const COMMENT_COURSE = "comment";
    public const EDIT_COURSE = "edit";
    public const DEACTIVATE_COURSE = "deactivate";
    public const DOWNLOAD = "download";

    public $actions;
    
    public function __construct(){
        //parent::__construct($pathName);
        $this->actions = array(self::INDEX => new Action("Index", null),
                                self::CREATE => new Action("CreateCourse", "CreateCoursePost"),
                                self::SHOW_ALL_COURSES_FABRIC => new Action("ShowAllCoursesFabric", null),
                                self::CREATE_CATEGORY => new Action(null, "InsertCategoryPost"),
                                self::SHOPPING_CART => new Action("ShoppingCart", "ShoppingCartPost"),
                                self::MY_COURSES => new Action("MyCourses", null),
                                self::VISUALIZE_COURSE => new Action("VisualizeCourse", null),
                                self::SEARCH_COURSE => new Action("SearchCourses", "SearchCoursePost"),
                                self::DETAIL_COURSE => new Action("DetailCourse", null),
                                self::EDIT_COURSE => new Action("EditCourse", "EditCoursePost"),
                                self::DEACTIVATE_COURSE => new Action(null, "DeactivatePost"),
                                self::COMMENT_COURSE => new Action(null, "ActionComment"),
                                self::DOWNLOAD => new Action("dowloadDiploma", null));
    }

    public function ShowContent($paths) {
        //Determine wich method call
        Action::ValidateActionsPath($paths, $this);
    }

    public function EditCoursePost(){
        try {
            $course = new CourseModel();
            
            $idCourse = UserSession::getCourseId();
            $course->setIdCourse($idCourse);

            if(isset($_FILES["image"]) && File::ValidateType($_FILES["image"])){
                $binaryImage = File::MakeFileToBinary($_FILES["image"]);
                $course->setImage($binaryImage);
            }

            !Validations::validateNullEmptyString($_POST["title"]) ?
            $course->setTitle($_POST["title"]) : 
            Validations::throwInputError();

            !Validations::validateNullEmptyString($_POST["description"]) ?
            $course->setDescription($_POST["description"]) : 
            Validations::throwInputError();

            !Validations::validateNumeric($_POST["totalprice"]) ?
            $course->setTotalprice($_POST["totalprice"]) : 
            Validations::throwInputError();

            !Validations::validateNull($_POST["category"]) ? 
            $categories = $_POST["category"] :
            Validations::throwInputError();

            $course->UpdateCourse();

            $courseCategory = new CoursesCategoryModel();
            $courseCategory->setIdCourse($idCourse);

            foreach ($categories as $category) {
                $courseCategory->setIdCategory($category);
                $courseCategory->InsertCourseCategory();
            }

            ob_end_clean();
            header("Location: " . Template::Route(LevelsController::ROUTE, LevelsController::EDIT_LEVELS)); 

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function EditCourse($paths){
        try {
            $idCourse = UserSession::getCourseId();
            if(isset($idCourse)){
                $course = CourseModel::getCourseById($idCourse);
                $courseCategories = CoursesCategoryModel::getCategoriesByIdCourse($idCourse);
                $categories = json_decode(file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php"), true);

                foreach ($categories as $category => $value){
                    foreach($courseCategories as $selectedCategory => $value){
                        if(intval($categories[$category]["IdCategory"]) == intval($courseCategories[$selectedCategory]["IdCategory"])){
                            unset($categories[$category]);
                            break;
                        }
                    }
                }

                include "views/course/editcourse.php";
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function DeactivatePost(){
        try {

            $idCourse = UserSession::getCourseId();
            if(isset($idCourse)){
                CourseModel::setActivateDeactivate($idCourse);
                ob_end_clean();
                header("Location: " . Template::Route(CoursesController::ROUTE, CoursesController::DETAIL_COURSE) . "/" . $idCourse); 

            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function DetailCourse($paths){
        try {
            $school = UserSession::getCurrentSchoolId();
            if(isset($paths[2]) && is_numeric($paths[2]) && isset($school)){

                $course = CourseModel::getCourseDetailByIdCourse($paths[2], $school);
                $registeredUsers = CourseModel::getStudentsRegisteredByIdCourse($paths[2], $school);
                $usersbylevels = CourseModel::getStudentsBoughtLevelsByIdCourse($paths[2], $school);
                $usersbycourses = CourseModel::getStudentsBoughtCourseByIdCourse($paths[2], $school);
                
                UserSession::setCourseId($course["IdCourse"]);

                if(is_null($course)) Validations::throwGeneralError();
                include "views/course/detailcourses.php";

            } else Validations::throwInputError();
        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function SearchCourses($paths){
        try {

            if(sizeof($paths) == 7 && strpos($paths[2], '-') != false && strpos($paths[3], '-') != false 
            && strpos($paths[4], '-') != false && strpos($paths[5], '-') != false && strpos($paths[6], '-') != false){

                $title =      substr(    $paths[2],    strpos($paths[2], '-') + 1,    strlen($paths[2]));
                $startDate =  substr(    $paths[3],    strpos($paths[3], '-') + 1,    strlen($paths[3]));
                $endDate =    substr(    $paths[4],    strpos($paths[4], '-') + 1,    strlen($paths[4]));
                $idCategory = substr(    $paths[5],    strpos($paths[5], '-') + 1,    strlen($paths[5]));
                $schoolName = substr(    $paths[6],    strpos($paths[6], '-') + 1,    strlen($paths[6]));

                if (empty($title))      $title = null;
                if (empty($startDate))  $startDate = null;
                if (empty($endDate))    $endDate = null;
                if (empty($idCategory)) $idCategory = null;
                if (empty($schoolName)) $schoolName = null;
                
            } else {
                $title = null;
                $startDate = null;
                $endDate = null;
                $idCategory = null;
                $schoolName = null;
            }

            $courses = CourseModel::getSearchedCourses($startDate, $endDate, $idCategory, $schoolName, $title);
            $categories = json_decode(file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php"), true);

            include "views/course/searchcourses.php";
        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function SearchCoursePost(){
        try {

            $title = isset($_POST["title"]) && !empty($_POST["title"]) ? "title-" . $_POST["title"] : "title-";
            $startDate = isset($_POST["startDate"]) && !empty($_POST["startDate"]) ? "startdate-" . $_POST["startDate"] : "startdate-";
            $endDate = isset($_POST["finalDate"]) && !empty($_POST["finalDate"]) ? "endDate-" . $_POST["finalDate"] : "endDate-";
            $idCategory = isset($_POST["category"]) && !empty($_POST["category"]) ? "category-" . $_POST["category"] : "category-";
            $schoolName = isset($_POST["schoolname"]) && !empty($_POST["schoolname"]) ? "school-" . $_POST["schoolname"] : "school-";
            
            header("Location: " . Template::Route(CoursesController::ROUTE, CoursesController::SEARCH_COURSE) . "/" . $title . "/" . $startDate . "/" . $endDate . "/" . $idCategory . "/" . $schoolName); 


        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function VisualizeCourse($paths){
        try {
            $user = UserSession::getCurrentUserId();
            //Esta seteado el curso? y el usuario?
            if(isset($paths[2]) && is_numeric($paths[2]) && isset($user)){

                //Nos traemos los niveles que tiene comprado el usuario
                $levels = UsersBuyLevelsModel::getUserBoughtLevels($paths[2], $user);
                //Si no tiene niveles comprados mandamos excepcion
                if(empty($levels)) Validations::throwGeneralError();

                //Checamos si esta seteado el 4 parametro que se usa para visualizar un nivel en especifico
                if(isset($paths[3]) && is_numeric($paths[3])){
                    //buscamos en la tabla de usuarios compran nivel si el nivel requerido se puede visualizar
                    $visualizedLevel = UsersBuyLevelsModel::getVisualizedBoughtLevel($paths[3], $user);
                } else { //Si no esta, entonces el primer nivel sera mostrado traido desde la bd
                    $visualizedLevel = UsersBuyLevelsModel::getVisualizedBoughtLevel($levels[0]["IdLevel"], $user);
                }

                //Si el nivel a visualizar es nulo, mandamos excepcion
                if(is_null($visualizedLevel)) Validations::throwGeneralError();
                
                $lastVisitDate = new UsersBuyLevelsModel();
                $lastVisitDate->setIdUser($user);
                $lastVisitDate->setIdLevel($visualizedLevel["IdLevel"]);
                $lastVisitDate->UpdateLastVisitDate();
                
                //Extraemos los links que esten separados por un espacio y los guardamos en un array de links que mostraremos en la vista
                $levelLinks =  explode(" ", $visualizedLevel["TextLinks"]);

                //Ahora nos traemos los pdf de ese nivel
                $pdfFiles = PDFFilesModel::getPDFFilesByIdLevel($visualizedLevel["IdLevel"]);
                $images = ImagesModel::getImagesByIdLevel($visualizedLevel["IdLevel"]);

                //Finalmente seteamos el progreso a terminado 

                //checamos los niveles cursados, está completo? Boton para descargar PDF
                $finish = CommentariesModel::HasUserCompletedTheCourse($user, $paths[2]);

                include "views/course/visualizecourse.php";
                
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function MyCourses(){
        try {
            $user = UserSession::getCurrentUserId();
            $courses = UsersRegisterCoursesModel::getCoursesByUserRegisteredId($user);
            include "views/course/mycourses.php";

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function ShoppingCartPost(){
        try {

            $user = UserSession::getCurrentUserId();
            if(isset($user)){
                if(isset($_POST["Course"]) && isset($_POST["Levels"]) ){
                    
                    //Nos traemos los parametros
                    $pCourse = $_POST["Course"];
                    $levels = $_POST["Levels"];
    
                    //Nos traemos los datos de ese curso a comprar
                    $course = CourseModel::getCourseById($pCourse["IdCourse"]);
                    
                    //Checamos si ese usario ya esta registrado
                    $isRegistered = UsersRegisterCoursesModel::IsUserRegistered($user, $course->getIdCourse());
                    
                    //Si no esta registrado lo registramos
                    if(!boolval($isRegistered[0])){
                        //Instanciamos para el registro del usuario para ese curso TODO: VALIDAR QUE SI ESTA REGISTRADO NO SE VUELVA A REGISTRAR
                        $userRegisterCourse = new UsersRegisterCoursesModel();
        
                        //Seteamos sus variables para la insercion
                        $userRegisterCourse->setIdUser($user);
                        $userRegisterCourse->setIdCourse($course->getIdCourse());
        
                        //Lo registramos
                        $userRegisterCourse->InsertUserRegisterCourse();
                    }
    
                    //Aqui entrara si el usuario quiere el curso completo
                    if(strcmp($pCourse["Checked"], "true") == 0){   
                        
                        //Instanciamos la compra de ese usuario por el curso y lo registramos
                        $userbuycourse = new UsersBuyCoursesModel();
                        $userbuycourse->setIdUser($user);
                        $userbuycourse->setIdCourse($course->getIdCourse());
                        $userbuycourse->setPaymentMethod($_POST["PaymentMethod"]);
                        $userbuycourse->setPurchasePrice($course->getTotalprice());
                        $userbuycourse->InsertUsersBuyCourses();
    
                    } else { //Aqui entrara si quiere niveles
    
                        //Instanciamos la compra por nivel
                        $userbuylevel = new UsersBuyLevelsModel();
                        $userbuylevel->setIdUser($user);
    
                        //El foreach para ver cual nivel esta seleccionado para compra
                        foreach ($levels as $plevel) {
                            if(strcmp($plevel["Checked"], "true") == 0){
                                //Nos traemos la info de cada nivel
                                $level = LevelModel::getLevelById($plevel["IdLevel"]);
                                $userbuylevel->setIdLevel($level->getIdlevel());
                                $userbuylevel->setPurchasePrice($level->getPrice());
                                $userbuylevel->setPaymentMethod($_POST["PaymentMethod"]);
                                //Los insertamos
                                $userbuylevel->InsertUsersBuyLevels();
                            }
                        }
                    }
    
                } else Validations::throwGeneralError();
                
                $response = array("result" => "true");

            } else $response = array("redirection" => "true");

            ob_end_clean();
            $json_response = json_encode($response);
            echo $json_response;
            die();

        } catch (Exception $e) {
            include "views/error.php";
        }


    }

    public function ShoppingCart($paths){
        try {
            $user = UserSession::getCurrentUserId();
            if(isset($user)){
                if(isset($paths[2]) && is_numeric($paths[2])){

                    $hasTheCourse = UsersBuyCoursesModel::getUserHasTheCourse($user, $paths[2]);
                    $userIsRegistered = UsersRegisterCoursesModel::IsUserRegistered($user, $paths[2]);
                    $course = CourseModel::getCourseById($paths[2]);
                    $levels = UsersBuyLevelsModel::getUserNotBoughtLevels($user, $paths[2]);

                    if(is_null($course) || is_null($levels)) Validations::throwGeneralError();
                    else include "views/course/shoppingcart.php";
                    
                } else Validations::throwGeneralError();
            } else {
                ob_end_clean();
                header("Location: " . Template::Route(UsersController::ROUTE, UsersController::LOGIN)); 
            }

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function Index($paths) {     
        try {
            if(isset($paths[2]) && is_numeric($paths[2])){

                $course = CourseModel::getCourseById($paths[2]);
                $levels = LevelModel::getLevelsByIdCourse($paths[2]);
                $categories = CoursesCategoryModel::getCategoriesByIdCourse($paths[2]);
                $cheapestLevel = LevelModel::getCheapestLevelByIdCourse($paths[2]);
                $commentaries = CommentariesModel::getCommentariesByIdCourse($paths[2]);
                $userComment = (new CommentariesModel)->getCommentByIdUserCourse($paths[2]);


                $userId = UserSession::getCurrentUserId();

                if(isset($userId)){
                    $user = UserModel::getUserById(UserSession::getCurrentUserId());
                    $userCanComment = CommentariesModel::HasUserCompletedTheCourse(UserSession::getCurrentUserId(), $paths[2]);
                }
                
                if(is_null($course) || is_null($levels) || is_null($categories) || is_null($cheapestLevel)) Validations::throwGeneralError();
                else include "views/course/index.php";
                
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function ShowAllCoursesFabric(){
        $IdOwner = UserSession::getCurrentSchoolId();
        $courses = CourseModel::getCoursesBySchoolId($IdOwner);
        $sales = SchoolModel::getSchoolSales($IdOwner);
        include "views/course/coursesfabric.php";
    }

    public function CreateCourse(){
        $categories = json_decode(file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php"), true);
        include "views/course/create.php";
    }

    public function CreateCoursePost(){
        try {
            $course = new CourseModel();
            
            $course->setIdOwner(UserSession::getCurrentSchoolId());

            if(File::ValidateType($_FILES["image"])){
                $binaryImage = File::MakeFileToBinary($_FILES["image"]);
                $course->setImage($binaryImage);
            } else Validations::throwInputError();

            !Validations::validateNullEmptyString($_POST["title"]) ?
            $course->setTitle($_POST["title"]) : 
            Validations::throwInputError();

            !Validations::validateNullEmptyString($_POST["description"]) ?
            $course->setDescription($_POST["description"]) : 
            Validations::throwInputError();

            !Validations::validateNumeric($_POST["totalprice"]) ?
            $course->setTotalprice($_POST["totalprice"]) : 
            Validations::throwInputError();

            !Validations::validateNull($_POST["category"]) ? 
            $categories = $_POST["category"] :
            Validations::throwInputError();

            $course->InsertCourse();
            
            $course = CourseModel::getLastIdCourseByIdOwner($course->getIdOwner());

            $courseCategory = new CoursesCategoryModel();
            $courseCategory->setIdCourse($course->getIdCourse());
            UserSession::setCourseId($course->getIdCourse());

            foreach ($categories as $category) {
                $courseCategory->setIdCategory($category);
                $courseCategory->InsertCourseCategory();
            }

            ob_end_clean();
            header("Location: " . Template::Route(LevelsController::ROUTE, LevelsController::CREATE)); 

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function InsertCategoryPost(){
        $postdata = http_build_query(
            array(
                'name' => $_POST["category-title"],
                'description' => $_POST["category-description"]
            )
        );

        $opts = array( 'http' =>
            array(
                'method' => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $result = json_decode(file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php"), true);

        $context = stream_context_create($opts);
        $result = file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php", false, $context);

        header("Location: " . Template::Route(CoursesController::ROUTE, CoursesController::CREATE));
    }


    public function ActionComment($paths){
        $userComment = (new CommentariesModel)-> getCommentByIdUserCourse($paths[2]);

        if (isset($_POST['btn_comentar'])) {   

            if (empty($userComment)){
                $commentInsert = CommentariesModel::insertComment($paths[2], $_POST["rating"], $_POST["commentary"]);
            }else{
                $userUp = CommentariesModel::updateCommenteById($userComment[0]["IdCommentary"], $_POST["rating"], $_POST["commentary"], 1);
            }

        } else if (isset($_POST['btn_delete'])) {
            //cambiar el active 

            if(empty($userComment)){

            }
            else{
                $userUp = CommentariesModel::updateCommenteById($userComment[0]["IdCommentary"], $_POST["rating"], $_POST["commentary"], 0);
            }
        } else {
            //No deberia entrar aqui
        }
        
        header("Location: " . Template::Route(CoursesController::ROUTE, CoursesController::INDEX) . "/" . $paths[2]);
    }
    

    public function dowloadDiploma($paths){
        $UserLog = UserSession::getCurrentUserId();
        $Alumno = (new UserModel) -> getUserById($UserLog);
        $course = CourseModel::getCourseById($paths[2]);
        
        ob_clean();
        $pdf = new FPDF('L', 'mm', 'A4');
        $pdf->AddPage();

        $pdf->Image('https://pbs.twimg.com/media/FEXN1NbVQAMKyx6?format=jpg&name=large',0,0, 298, 210,'JPG');
        $pdf->Image('https://pbs.twimg.com/media/FEXO4tqUYAEOMvn?format=png&name=240x240',118, 38, 0, 0,'PNG');
        $pdf->SetFont('Arial','B',19);
        $pdf->Ln(56);
        $pdf->SetTextColor(227,209,145);
        $pdf->Cell(0,10,'C E R T I F I C A D O    C U R S O    C O M P L E T A D O', 0, 0, 'C', false,);

        $pdf->Ln(20);
        $pdf->SetFont('Arial','B',13);
        $pdf->SetTextColor(32,58,83);
        $pdf->Cell(0,10,'R E C O N O C E M O S    A', 0, 0, 'C', false,);

        $pdf->Ln(20);
        $pdf->SetFont('Arial','B',38);
        $pdf->SetTextColor(229,161,142);
        $pdf->Cell(0,10, utf8_decode($Alumno->getNames() . ' ' . $Alumno->getFirstSurname() . ' '. $Alumno->getSecondSurname()), 0, 0, 'C', false,);
        
        $pdf->Ln(17);
        $pdf->SetFont('Arial','B',12);
        $pdf->SetTextColor(32,58,83);
        $pdf->Cell(0,10,utf8_decode('Por completar el curso de '.  $course->getTitle()) , 0, 0, 'C', false,);

        $pdf->Ln(25);
        $pdf->SetFont('Arial','B',14);
        $pdf->SetTextColor(32,58,83);
        $pdf->Cell(136, 7,  utf8_decode($course->getSchoolName()) , 0, 0, 'C', false,);
        $pdf->Cell(136, 7,'LEARN DO!', 0, 0, 'C', false,);

        $pdf->Ln(6);
        $pdf->SetFont('Arial', null ,11);
        $pdf->SetTextColor(79,194,248);
        $pdf->Cell(136, 6,  'Educador', 0, 0, 'C', false,);
        $pdf->Cell(136, 6,'Escuela Virtual', 0, 0, 'C', false,);

        $pdf->Output('D', 'Diploma_'. utf8_decode($course->getTitle()).'.pdf', false);
        ob_end_flush(); 
    }
}