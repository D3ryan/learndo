<?php

require_once "controllers/template.controller.php";
require_once "models/UserModel.php";
require_once "models/SchoolModel.php";
require_once "helpers/action.php";
require_once "helpers/files.php";
require_once "helpers/userSession.php";
require_once "helpers/validations.php";

class UsersController extends Controller {
    // CONTROLLER ROUTE
    public const ROUTE = "users";

    //ACTIONS ROUTE
    public const INDEX = "index";
    public const SIGNUP = "signup";
    public const PROFILE = "profile";
    public const SCHOOL = "school";
    public const LOGOUT = "logout";
    public const LOGIN = "login";

    public $actions;
    
    public function __construct(){
        //parent::__construct($pathName);
        $this->actions = array(self::INDEX => new Action("Index", null),
                                self::SIGNUP => new Action(null, "InsertUserPost"),
                                self::PROFILE => new Action("UserProfile", "UserProfilePost"),
                                self::SCHOOL => new Action(null, "InsertSchoolPost"),
                                self::LOGOUT => new Action("Logout", null),
                                self::LOGIN => new Action("Login", "LoginPost"));
    }

    public function ShowContent($paths) {
        //Determine wich method call
        Action::ValidateActionsPath($paths, $this);
    }

    public function Index($paths) {     
        include "views/users/index.php";
    }

    public function Login(){        
        include "views/users/login.php";
    }

    public function Logout(){
        UserSession::closeSession();
        ob_end_clean();
        header("Location: " . Template::Route(HomeController::ROUTE, HomeController::INDEX)); 
        die();
    }

    public function LoginPost(){
        $user = SchoolModel::getUserIdByEmailAndPassword($_POST["email"], $_POST["password"]);
        if (isset($user)){
            UserSession::setCurrentUserId($user->getId());
            UserSession::setCurrentSchoolId($user->getIdSchool());
            ob_end_clean();
            header("Location: " . Template::Route(HomeController::ROUTE, HomeController::INDEX));  
        }else{
            $getS = true;
            include "views/users/login.php";
        }
    }

    public function UserProfile(){

        try {
            $userId = UserSession::getCurrentUserId();
            $schoolId = UserSession::getCurrentSchoolId();
    
            if(isset($userId)){
                $user = UserModel::getUserById($userId);
                if(isset($schoolId)) $school = SchoolModel::getSchoolById($schoolId);
                include "views/users/profile.php";
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }

       
    }

    public function UserProfilePost(){

        //TODO: PONER TRY CATCH AQUI
        try {
            
            $user = new UserModel();
            $user->setId(UserSession::getCurrentUserId());
            
            !Validations::validateUserName($_POST["names"]) ?
            $user->setNames($_POST["names"]) : 
            Validations::throwInputError();

            if(isset($_FILES["avatar"]) && File::ValidateType($_FILES["avatar"])){
                $binaryAvatar = File::MakeFileToBinary($_FILES["avatar"]);
                $user->setAvatar($binaryAvatar);
            }
            
            !Validations::validateUserSurname($_POST["firstsurname"]) ?
            $user->setFirstSurname($_POST["firstsurname"]) :
            Validations::throwInputError();

            !Validations::validateUserSurname($_POST["secondsurname"]) ?
            $user->setSecondSurname($_POST["secondsurname"]) :
            Validations::throwInputError();

            !Validations::validateUserGender($_POST["gender"]) ?
            $user->setGender($_POST["gender"]) :
            Validations::throwInputError();

            !Validations::validateUserBirthdate($_POST["birthdate"]) ?
            $user->setBirthdate($_POST["birthdate"]) :
            Validations::throwInputError();

            !Validations::validateUserEmail($_POST["email"]) ?
            $user->setEmail($_POST["email"]) : 
            Validations::throwInputError();

            !Validations::validatePassword($_POST["password"]) ? 
            $user->setPassword($_POST["password"]) :
            Validations::throwInputError();
    
            $user->UpdateUser();
    
            ob_end_clean();
            header("Location: " . Template::Route(UsersController::ROUTE, UsersController::PROFILE)); 

        } catch (Exception $e) {
            include "views/error.php";
        }
        die();
    }

    public function InsertUserPost(){

        try {

            $user = new UserModel();
    
            if(File::ValidateType($_FILES["avatar"])){
                $binaryAvatar = File::MakeFileToBinary($_FILES["avatar"]);
                $user->setAvatar($binaryAvatar);
            } else Validations::throwInputError();

            //If Ternarios para validacion.
            !Validations::validateUserName($_POST["names"]) ?
            $user->setNames($_POST["names"]) : 
            Validations::throwInputError();
    
            !Validations::validateUserSurname($_POST["firstsurname"]) ?
            $user->setFirstSurname($_POST["firstsurname"]) :
            Validations::throwInputError();

            !Validations::validateUserSurname($_POST["secondsurname"]) ?
            $user->setSecondSurname($_POST["secondsurname"]) :
            Validations::throwInputError();


            !Validations::validateUserGender($_POST["gender"]) ?
            $user->setGender($_POST["gender"]) :
            Validations::throwInputError();

            !Validations::validateUserBirthdate($_POST["birthdate"]) ?
            $user->setBirthdate($_POST["birthdate"]) :
            Validations::throwInputError();

            !Validations::validateUserEmail($_POST["email"]) ?
            $user->setEmail($_POST["email"]) : 
            Validations::throwInputError();
            
            !Validations::validatePassword($_POST["password"]) ? 
            $user->setPassword($_POST["password"]) :
            Validations::throwInputError();

            $user->InsertUser();
    
            $user = UserModel::getUserIdByEmailAndPassword($_POST["email"], $_POST["password"]);
            UserSession::setCurrentUserId($user->getId());

            ob_end_clean();
            header("Location: " . Template::Route(HomeController::ROUTE, HomeController::INDEX));

        } catch (Exception $e) {
            include "views/error.php";
        }
    

        die();
    }

    public function InsertSchoolPost(){
        //TODO: PONER EN LA SESION EL ID de la escuela, traersela de mysql, falta hacer el query y la logica aqui en php

        try {
            $school = new SchoolModel();

            $userId = UserSession::getCurrentUserId();
            $schoolId = UserSession::getCurrentSchoolId();

            $school->setIdSchool($schoolId);
            $school->setId($userId);
            
            if(isset($_FILES["avatar"]) && File::ValidateType($_FILES["avatar"])){
                $binaryAvatar = File::MakeFileToBinary($_FILES["avatar"]);
                $school->setAvatarSchool($binaryAvatar);
            }
            
            !Validations::validateAlphanumericAndSpaces($_POST["schoolname"]) ?
            $school->setNameSchool($_POST["schoolname"]) :
            Validations::throwInputError();
            
            $school->InsertUpdateSchool();

            if(!isset($schoolId)){
                $school = SchoolModel::getSchoolByUserId($userId);
                UserSession::setCurrentSchoolId($school->getIdSchool());
            }

            ob_end_clean();
            header("Location: " . Template::Route(UsersController::ROUTE, UsersController::PROFILE)); 

        } catch (Exception $e) {
            include "views/error.php";
        }

        die();
    }

    
}