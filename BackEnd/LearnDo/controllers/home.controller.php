<?php

require_once "controllers/template.controller.php";
require_once "models/CourseModel.php";
require_once "helpers/action.php";

class HomeController extends Controller {
    // CONTROLLER ROUTE
    public const ROUTE = "home";

    //ACTIONS ROUTE
    public const INDEX = "index";


    public $actions;
    
    public function __construct(){
        $this->actions = array(self::INDEX => new Action("Index", null));
    }

    public function ShowContent($paths) {
        //Determine wich method call
       Action::ValidateActionsPath($paths, $this);
    }

    public function Index($paths) {
        $recentCourses = CourseModel::getRecentCourses();
        $bestSellerCourses = CourseModel::getBestSellerCourses();
        $bestRatedCourses = CourseModel::getBestRatedCourses();
        $categories = json_decode(file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php"), true);
        include "views/home/index.php";
    }

}