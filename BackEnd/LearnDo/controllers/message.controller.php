<?php

require_once "controllers/template.controller.php";
require_once "models/ChatModel.php";
require_once "helpers/action.php";
require_once "helpers/userSession.php";
require_once "helpers/validations.php";


class MessageController extends Controller{
    public const ROUTE = "chat";
    
    //ACTIONS ROUTE
    public const INDEX = "index";
    public const SEARCH = "search";
    public const BUZON = "buzon";
    public const SEND = "send";
    public const CLICK = "click";

    public $actions;
    
    public function __construct(){
        $this->actions = array(self::INDEX => new Action("Index", null),
                self::SEARCH => new Action(null, "SearchUSer"),
                self::BUZON => new Action("buzon", null),
                self::SEND => new Action(null, "SendMessage"),
                self::CLICK => new Action("ClickUserInbox", null));
    }

    public function ShowContent($paths) {
        //Determine wich method call
        Action::ValidateActionsPath($paths, $this);
    }

    public function Index($paths) {     
        // include "views/users/index.php";
    }

    
    public function buzon(){        
        
        $userIn = ChatModel::inbox();
        if(empty($userIn)){
            $messages = null;
        }
        else{
            $messages = (new ChatModel)->getConversationsById($userIn[0]["InboxUserID"]);
        }
        
        include "views/chat/chat.php";
    }

    
    public function ClickUserInbox($paths) {     
        try {
            if(isset($paths[2])){

                $userIn = ChatModel::inbox();
                $userC = ChatModel::getUserByID($paths[2]);
                $messages = (new ChatModel)->getConversationsById($paths[2]);

                if(is_null($userC)) Validations::throwGeneralError();
                else include "views/chat/chat.php";
                
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }

    public function SendMessage($paths){
        try {
            if(isset($paths[2])){
                $userC = ChatModel::getUserByID($paths[2]);
                if(($_POST["messageInput"])!= null){
                    $messagesInsert = (new ChatModel)->sendMessagesInsert($paths[2], $_POST["messageInput"]);
                }
                $messages = (new ChatModel)->getConversationsById($paths[2]);
                $userIn = ChatModel::inbox();

                if(is_null($userC)) Validations::throwGeneralError();
                else include "views/chat/chat.php";
                
            } else Validations::throwGeneralError();

        } catch (Exception $e) {
            include "views/error.php";
        }
    }


    public function SearchUSer(){        
        ob_end_clean();
        $valueIn = (new ChatModel)->getUsersAndSchools($_POST['UserSchool']);
    
        $echoR =json_encode($valueIn);
        echo  $echoR;
        die();
    }
}


