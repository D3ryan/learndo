<?php

    require_once "../models/CategoriesModel.php";

    ob_end_clean();

    try{
        //TODO: Probar que pasa si no hay ninguna categoria
        if($_SERVER["REQUEST_METHOD"] == "GET"){
            $categoriesList = CategoryModel::getAllCategories();
            $jsonCategories = json_encode($categoriesList);
        
        } else if($_SERVER["REQUEST_METHOD"] == 'POST'){
            $category = array(
                    'name' => $_POST["name"],
                    'description' => $_POST["description"]);

            CategoryModel::InsertCategory($category);
            $jsonCategories = '{"message": "sucesss"}';
        }

    }catch(Exception $e){
        $jsonCategories = '{"error": "' . $e->getMessage() . '"}';
    }

    echo $jsonCategories;

    die();