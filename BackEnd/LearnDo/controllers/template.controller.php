<?php
require_once "controllers/controller.php";
require_once "controllers/home.controller.php";
require_once "controllers/users.controller.php";
require_once "controllers/courses.controller.php";
require_once "controllers/levels.controller.php";
require_once "controllers/message.controller.php";

class Template {
    private $controllers;
    //It must come from the DB
    public const ROOT_PATH = "/";
    public function __construct(){
        $this->InitializeControllers();
    }
    public function ShowTemplate(){
        $userId = UserSession::getCurrentUserId();
        if(isset($userId)) $user = UserModel::getUserById($userId);
        $userSchoolId = UserSession::getCurrentSchoolId();
        $categories = json_decode(file_get_contents("http://learndo.edukt.studio" . Template::ROOT_PATH . "controllers/apicategories.php"), true);
        include "views/main-template.php";
    }
    public function InitializeControllers(){

        $this->controllers = array(HomeController::ROUTE => new HomeController(),
                                    UsersController::ROUTE=> new UsersController(),
                                    CoursesController::ROUTE=> new CoursesController(),
                                    LevelsController::ROUTE=> new LevelsController(),
                                    MessageController::ROUTE=> new MessageController());

    }
    
    //Determinar que controlador se va a usar para cada pagina 
    public function DeterminePage(){
        Controller::ValidateControllersPath($this->controllers);
    }

    public function CallScripts(){
        if(function_exists("Scripts")){
            call_user_func("Scripts");
        }
    }

    public static function Route($controller, $action){
        $route = self::ROOT_PATH;
        if($action != null)
            $route = self::ROOT_PATH.$controller."/".$action;
        else
            $route = self::ROOT_PATH.$controller;
        return $route;
    }
}