CALL `learndo`.`sp_Users`("I", NULL,"Eduardo", "Garcia", "Gonzalez", NULL, 1, "a@a.com", "123", "2020-12-01", current_timestamp(), current_timestamp(), NULL);
CALL `learndo`.`sp_Users`("X", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL `learndo`.`sp_Users`("X", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL `learndo`.`sp_Users`("L", NULL, NULL, NULL, NULL, NULL, NULL, "a@a.com", 'aaaaa1A!', NULL, NULL, NULL, NULL);
CALL `learndo`.`sp_Users`("ID", 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL `learndo`.`sp_Categories`('I', NULL, "Tecnologia", "Categoria que representa cursos relacionados a los dispositivos electronicos", NULL);
CALL `learndo`.`sp_Categories`('I', NULL, "Cocina", "Categoria que representa cursos relacionados a la preparación de comida", NULL);

CALL `learndo`.`sp_Categories`('X', NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_Schools`('X', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_courses`('X', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
CALL `learndo`.`sp_courses`('RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_coursesCategories`("X", NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_levels`("X", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_levels`("LC", NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_pdfFiles`("X", NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_images`("X", NULL, NULL, NULL, NULL);


CALL `learndo`.`sp_usersregistercourses`("I", NULL, 2, 1, CURRENT_TIMESTAMP, NULL, NULL);
CALL `learndo`.`sp_usersregistercourses`("X", NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_usersbuylevels`("I", NULL, 2, 1, 100, NULL, NULL, NULL);
CALL `learndo`.`sp_usersbuylevels`("I", NULL, 2, 2, 100, NULL, NULL, NULL);

CALL `learndo`.`sp_usersbuylevels`("X", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_usersbuycourses`("I", NULL, 2, 1, 1, 3000, NULL);
CALL `learndo`.`sp_usersbuycourses`("X", NULL, NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_advancedsearch`("B", NULL, NULL, NULL, NULL, NULL);

CALL `learndo`.`sp_courses`('D', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

SELECT `learndo`.`HasCompletedTheCourse`(2, 2) as HasCompletedCourse;


SELECT `learndo`.`IsUserRegisteredInTheCourse`(1, 2) as IsRegistered;


SELECT `learndo`.`StudentProgressPercentage`(2, 3) as Percentage;



SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table schools; 
SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table users; 
SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table courses; 
SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table coursescategories; 
SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table levels; 
SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table pdffiles; 
SET FOREIGN_KEY_CHECKS = 1;

SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table images; 
SET FOREIGN_KEY_CHECKS = 1;



SELECT * FROM schools;


-- Sin tipo de index
select distinct
	c.table_name AS 'Table',
    c.column_name AS 'Column',
    c.data_type AS 'Data type',
    c.character_maximum_length AS 'MAX LENGTH',
    r.referenced_table_name AS 'References',
    c.column_key AS 'Key',
    c.is_nullable AS 'Required',
    c.extra AS 'Extra',
    c.column_comment AS 'Description',
    c.privileges AS 'Privileges'
FROM information_schema.tables AS t
INNER JOIN information_schema.columns AS c
	ON t.table_name = c.table_name AND t.table_schema = c.table_schema
LEFT JOIN information_schema.key_column_usage AS r
    ON t.table_name = r.table_name AND (c.column_name = r.column_name)
WHERE t.table_type IN ('BASE TABLE')
AND t.table_schema = 'learndo'
ORDER BY 1, --c.column_name, c.ordinal_position;

-- Con index
select distinct
    c.table_name AS 'Table',
    c.column_name AS 'Column',
    c.data_type AS 'Data type',
    c.character_maximum_length AS 'MAX LENGTH',
    r.referenced_table_name AS 'References',
    c.column_key AS 'Key',
    c.is_nullable AS 'Required',
    c.extra AS 'Extra',
    c.column_comment AS 'Description',
    INDEX_NAME 'Index Name',
    c.privileges AS 'Privileges'
FROM information_schema.tables AS t
INNER JOIN information_schema.columns AS c
    ON t.table_name = c.table_name AND t.table_schema = c.table_schema
LEFT JOIN information_schema.key_column_usage AS r
    ON t.table_name = r.table_name AND (c.column_name = r.column_name)
    LEFT JOIN information_schema.STATISTICS AS st
    ON t.table_name = st.table_name AND (c.column_name = st.column_name)
WHERE t.table_type IN ('BASE TABLE')
AND t.table_schema = 'learndo'
ORDER BY 1, --c.column_name, c.ordinal_position;