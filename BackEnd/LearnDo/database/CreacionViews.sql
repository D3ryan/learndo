USE learndo;

-- Usuarios y sus escuelas SI SE USA
CREATE VIEW `v_usersAndSchools` AS
	SELECT U.IdUser, U.Names, U.FirstSurname, U.SecondSurname, U.Avatar, U.Gender, U.Email, U.Password, U.Birthdate, U.Signupdate, U.LastUpdateDate, U.Active,
			S.IdSchool, S.Avatar as SchoolAvatar, S.Name as SchoolName, S.Signupdate as SchoolSignupdate, S.LastUpdateDate as SchoolLastUpdateDate
	FROM users as U
    LEFT JOIN schools as S
    ON U.IdUser = S.IdUser;

-- Cursos con sus estudiantes y sus ventas SI SE USA
CREATE VIEW `v_coursesStudentsAndSales` AS
	SELECT  C.IdCourse, C.IdOwner, C.Title, C.Image, COUNT(DISTINCT URC.IdUser) as Students,  SumCourseEarnings(C.IdCourse) as TotalSales, C.Active
		FROM courses as C
		LEFT JOIN usersregistercourses as URC
		ON C.IdCourse = URC.IdCourse
        INNER JOIN levels as L
        ON C.IdCourse = L.IdCourse
        LEFT JOIN usersbuylevels as UBL
        ON L.IdLevel = UBL.IdLevel
		GROUP BY C.IdCourse;

-- Escuelas, sus cursos, sus alumnos y su calificacion TODO: CHECAR ESTA VIEW SI SE USA
CREATE VIEW `v_SchoolCoursesStudentsRating` AS
	SELECT S.IdSchool, S.Name, C.IdCourse, C.Title, C.Image, C.Description, C.TotalPrice, C.CreationDate, COUNT(DISTINCT URC.IdUser) as Students, FORMAT(AVG(CM.Rating), 1) as Rating, C.Active
		FROM courses as C
        INNER JOIN schools as S
        ON C.IdOwner = S.IdSchool
        LEFT JOIN usersregistercourses as URC
		ON C.IdCourse = URC.IdCourse 
        LEFT JOIN commentaries as CM
		ON C.IdCourse = CM.IdCourse
	-- 	WHERE CM.Active = 1 pasar esto al query
        GROUP BY C.IdCourse, URC.IdCourse, CM.IdCourse;

        
-- Cursos y su cantidad de niveles SI SE USA
CREATE VIEW `v_CoursesNumberOfLevels` AS
    SELECT C.IdCourse, C.Title, COUNT(L.IdLevel) AS NumberOfLevels
		FROM courses as C
        INNER JOIN levels as L
        ON L.IdCourse = C.IdCourse
        GROUP BY C.IdCourse;
	
-- Estudiantes que compraron el curso completo
CREATE VIEW `v_StudentDetailedBoughtCourse` AS
	SELECT UBC.IdCourse, C.IdOwner, U.IdUser, U.Avatar, U.Names, U.FirstSurname, U.SecondSurname, StudentProgressPercentage(UBC.IdCourse, U.IdUser) as Progress, UBC.PurchasePrice, UBC.PaymentMethod, URC.RegistrationDate
		FROM usersbuycourses as UBC
        INNER JOIN users AS U
        ON UBC.IdUser = U.IdUser 
        INNER JOIN usersregistercourses AS URC
        ON URC.IdUser = U.IdUser AND URC.IdCourse = UBC.IdCourse
        INNER JOIN courses as C
        ON UBC.IdCourse = C.IdCourse;
     
     

-- TODO CHECAR FUNCIONAMIENTO Estudiantes que compraron niveles
CREATE VIEW `v_StudentDetailedBoughtLevels` AS
	SELECT  C.IdCourse, C.IdOwner, L.Title, U.IdUser, U.Avatar, U.Names, U.FirstSurname, U.SecondSurname, UBL.Progress, UBL.PurchasePrice, UBL.PaymentMethod, URC.RegistrationDate
		FROM usersbuylevels as UBL
        INNER JOIN users AS U
		ON UBL.IdUser = U.IdUser
        INNER JOIN levels AS L
        ON UBL.IdLevel = L.IdLevel
        INNER JOIN courses AS C
        ON L.IdCourse = C.IdCourse
		INNER JOIN usersregistercourses AS URC
		ON C.IdCourse = URC.IdCourse AND U.IdUser = URC.IdUser
        GROUP BY UBL.IdUser, UBL.IdLevel
	ORDER BY UBL.IdUser;
        
-- Estudiantes que registraron el curso
CREATE VIEW `v_StudentRegisteredDetailed` AS
	SELECT URC.IdCourse, C.IdOwner, U.IdUser, U.Avatar, U.Names, U.FirstSurname, U.SecondSurname, StudentProgressPercentage(URC.IdCourse, U.IdUser) as Progress, 
		   SumUserTotalPayments(U.IdUser, URC.IdCourse) as TotalPayments, URC.RegistrationDate
		FROM usersregistercourses as URC
        INNER JOIN users AS U
        ON URC.IdUser = U.IdUser
        INNER JOIN courses as C
        ON C.IdCourse = URC.IdCourse;
        
		
        