USE LearnDo;


DROP PROCEDURE IF EXISTS `sp_Categories`;
DELIMITER $$
CREATE PROCEDURE `sp_Categories` (
	IN p_Opc						CHAR(3),
	IN p_IdCategory					BIGINT,
    IN p_Name						VARCHAR(30),
	IN p_Description				VARCHAR(100),
	IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCategory, Name, Description, Active
			FROM categories;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdCategory, Name, Description, Active
			FROM categories
            WHERE IdCategory = p_IdCategory;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO categories(Name, Description)
					VALUES(p_Name, p_Description);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE categories
			SET Name = p_Name,
				Description = p_Description
			WHERE IdCategory = p_IdCategory;
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_commentaries`;
DELIMITER $$
CREATE PROCEDURE `sp_commentaries`(
	IN p_Opc						CHAR(3),
    IN p_IdCommentary				BIGINT,
    IN p_IdUser						BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_TextCommentary 			TEXT,
    IN p_Rating						SMALLINT,
    IN p_DateCommentary				DATE,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCommentary, IdUser, IdCourse, TextCommentary, Rating, DateCommentary, Active
			FROM commentaries;
    END IF;
    
    -- Trae la información de el comentario donde coincide el usuario y el curso (id)
    IF p_Opc = 'ID'
    THEN
		SELECT IdCommentary, IdUser, IdCourse, TextCommentary, Rating, p_DateCommentary, Active
			FROM commentaries
            WHERE IdUser = p_IdUser
            ANd IdCourse = p_IdCourse;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO commentaries(IdUser, IdCourse, TextCommentary, Rating)
					VALUES(p_IdUser, p_IdCourse, p_TextCommentary, p_Rating);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE commentaries
			SET TextCommentary = p_TextCommentary,
				Rating = p_Rating,
                DateCommentary = CURRENT_TIMESTAMP(),
                Active = p_Active
			WHERE IdCommentary = p_IdCommentary;
    END IF;
    
    -- Se usa para los comentarios de un curso
    IF p_Opc = 'CC'
    THEN 
		SELECT IdCommentary, U.IdUser, U.Avatar, U.Names, U.FirstSurname, U.SecondSurname, IdCourse, TextCommentary, Rating, DateCommentary, C.Active
			FROM commentaries as C
            INNER JOIN users as U
            ON C.IdUser = U.IdUser
		WHERE IdCourse = p_IdCourse
        AND C.Active = 1;
    END IF;
    
    -- SE USA PARA CHECAR SI PUEDE COMENTAR
    IF p_Opc = 'UCC'
    THEN
        SELECT HasCompletedTheCourse(p_IdCourse, p_IdUser) as HasCompletedCourse; 
    END IF;
    
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_courses`;
DELIMITER $$
CREATE PROCEDURE `sp_courses`(
	IN p_Opc						CHAR(3),
    IN p_IdCourse					BIGINT,
    IN p_IdOwner					BIGINT,
    IN p_Title						VARCHAR(400),
    IN p_Image						MEDIUMBLOB,
    IN p_Description				VARCHAR(400),
    IN p_TotalPrice					DECIMAL(15,2),
    IN p_CreationDate				DATE,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCourse, IdOwner, Title, Image, Description, TotalPrice, CreationDate, Active
			FROM courses;
    END IF;
    
    IF p_Opc = 'O'
    THEN
		SELECT IdCourse
			FROM courses
		WHERE IdOwner = p_IdOwner
        ORDER BY IdCourse DESC LIMIT 1;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdCourse, IdOwner, Title, Image, Description, TotalPrice, CreationDate, Active
			FROM courses
            WHERE IdCourse = p_IdCourse;
    END IF;
    
    -- Este se usa para mostrar la presentacion del curso
    IF p_Opc = 'SH'
    THEN
		SELECT Name as SchoolName, IdCourse, Title, Image, Description, TotalPrice, Students, Rating, CreationDate
			FROM v_schoolcoursesstudentsrating
		WHERE IdCourse = p_IdCourse
        AND Active = TRUE;
    END IF;

    IF p_Opc = 'I'
    THEN
		INSERT INTO courses(IdOwner, Title, Image, Description, TotalPrice)
					VALUES(p_IdOwner, p_Title, p_Image, p_Description, p_TotalPrice);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE courses
			SET Title = p_Title,
				Image = IFNULL(p_Image, Image),
                Description = p_Description,
                TotalPrice = p_TotalPrice
			WHERE IdCourse = p_IdCourse;
    END IF;
    IF p_Opc = 'CS'
    THEN
		SELECT IdCourse, IdOwner, Title, Image, Students, TotalSales, Active
			FROM v_coursesstudentsandsales
		WHERE IdOwner = p_IdOwner;
    END IF;
    
    -- Esta es para los cursos mas recientes
    IF p_Opc = 'RC'
    THEN
		SELECT C.IdCourse, S.Name as SchoolName, C.Title, C.Image, C.CreationDate
			FROM courses as C
            INNER JOIN schools as S
            ON C.IdOwner = S.IdSchool
		WHERE C.Active = true
		ORDER BY C.CreationDate DESC, C.IdCourse DESC
        LIMIT 10;
    END IF;
    
    -- Este es para traerse los mas vendidos
    IF p_Opc = 'SC'
    THEN
		SELECT CSS.IdCourse, S.Name as SchoolName, CSS.Title, CSS.Image, CSS.TotalSales
			FROM v_coursesstudentsandsales as CSS
			INNER JOIN schools as S
            ON CSS.IdOwner = S.IdSchool
		WHERE CSS.Active = true
		ORDER BY CSS.TotalSales DESC
        LIMIT 10;
    END IF;
    
    -- Este es para traerse los mejores en rating
    
    IF p_Opc = 'R'
    THEN
		SELECT C.IdCourse, S.Name as SchoolName, C.Title, C.Image, SUM(IFNULL(CM.Rating, 0)) as Rating
			FROM courses as C
            INNER JOIN schools as S
            ON C.IdOwner = S.IdSchool
            LEFT JOIN commentaries as CM
            ON C.IdCourse = CM.IdCourse
		WHERE C.Active = true
		GROUP BY  C.IdCourse
		ORDER BY Rating DESC
        LIMIT 10;
    END IF;
    
	-- Este se usa para traerse los detalles de un curso
    IF p_Opc = 'DC'
    THEN
		SELECT CSS.IdCourse, CSS.Title, CSS.Students, SumCourseEarnings(CSS.IdCourse) as TotalSales, CSS.Active
			FROM v_coursesstudentsandsales as CSS
		WHERE CSS.IdCourse = p_IdCourse AND CSS.IdOwner = p_IdOwner;
    END IF;
    
    -- Usado para ver los estudiantes registrados a un curso
    IF p_Opc = 'SRD'
    THEN
		SELECT IdCourse	, IdUser, Avatar, Names, FirstSurname, SecondSurname, Progress, TotalPayments, RegistrationDate 
			FROM v_studentregistereddetailed
		WHERE IdCourse = p_IdCourse AND IdOwner = p_IdOwner;
    END IF;
    
	-- Usado para ver los detalles de los usuarios que compraron por nivel
    IF p_Opc = 'SBL'
    THEN 
		SELECT IdCourse, Title, IdUser, Avatar, Names, FirstSurname, SecondSurname, IF(Progress = 1, 'Terminado', 'Sin Empezar') AS Progress, PurchasePrice, 
			IF (PaymentMethod = 1, 'Paypal', 'Tarjeta Debito/Credito') AS PaymentMethod, RegistrationDate
			FROM v_studentdetailedboughtlevels
		WHERE IdCourse = p_IdCourse AND IdOwner = p_IdOwner;
	END IF;
    
	-- Usado para ver los detalles de los usuarios que compraron por curso
	IF p_Opc = 'SBC'
    THEN 
		SELECT IdCourse, IdUser, Avatar, Names, FirstSurname, SecondSurname, Progress, PurchasePrice, 
		IF (PaymentMethod = 1, 'Paypal', 'Tarjeta Debito/Credito') AS PaymentMethod, RegistrationDate
			FROM v_studentdetailedboughtcourse
		WHERE IdCourse = p_IdCourse AND IdOwner = p_IdOwner;
	END IF;
    
    IF p_Opc = 'D'
    THEN
		UPDATE courses
			SET Active = NOT Active
		WHERE IdCourse = p_IdCourse;
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_coursesCategories`;
DELIMITER $$
CREATE PROCEDURE `sp_coursesCategories`(
	IN p_Opc						CHAR(3),
    IN p_IdCoursesCategory			BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_IdCategory					BIGINT,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCoursesCategory, IdCourse, IdCategory, Active
			FROM coursescategories;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdCoursesCategory, IdCourse, IdCategory, Active
			FROM coursescategories
            WHERE IdCoursesCategory = p_IdCoursesCategory;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO coursescategories(IdCourse, IdCategory)
					VALUES(p_IdCourse, p_IdCategory);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE coursescategories
			SET IdCourse = p_IdCourse,
				IdCategory = p_IdCategory
			WHERE IdCoursesCategory = p_IdCoursesCategory;
    END IF;
    
	-- Con este agarramos las categorias de un curso
    IF p_Opc = 'C'
    THEN
		SELECT CC.IdCategory, C.Name
			FROM coursescategories AS CC
            INNER JOIN categories AS C
            ON CC.IdCategory = C.IdCategory
            INNER JOIN courses AS CS
			ON CC.IdCourse = CS.IdCourse
		WHERE CS.IdCourse = p_IdCourse;
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_formasDePago`;
DELIMITER $$
CREATE PROCEDURE `sp_formasDePago`(
	IN p_Opc						CHAR(3),
    IN p_IdFormaDePago				TINYINT,
    IN p_Nombre						VARCHAR(20),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdFormaDePago, Nombre, Active
			FROM formasdepago;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdFormaDePago, Nombre, Active
			FROM formasdepago
            WHERE IdFormaDePago = p_IdFormaDePago;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO formasdepago(Nombre)
					VALUES(p_Nombre);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE formasdepago
			SET Nombre = p_Nombre
			WHERE IdFormaDePago = p_IdFormaDePago;
    END IF;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_images`;
DELIMITER $$
CREATE PROCEDURE `sp_images`(
	IN p_Opc						CHAR(3),
    IN p_IdImages					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_Image						MEDIUMBLOB,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdImages, IdLevel, Image, Active
			FROM images;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdImages, IdLevel, Image, Active
			FROM images
            WHERE IdImages = p_IdImages;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO images(IdLevel, Image)
					VALUES(p_IdLevel, p_Image);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE images
			SET Image = p_Image
			WHERE IdImage = p_IdImage;
    END IF;
    
    IF p_Opc = 'L'
    THEN
		SELECT IdImages, IdLevel, Image
			FROM images
		WHERE IdLevel = p_IdLevel;
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_levels`;
DELIMITER $$
CREATE PROCEDURE `sp_levels`(
	IN p_Opc						CHAR(3),
    IN p_IdLevel					BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_Title						VARCHAR(400),
    IN p_Price						DECIMAL(15,2),
    IN p_TextLevel					VARCHAR(500),
	IN p_TextLinks					VARCHAR(800),
    IN p_UrlVideo					VARCHAR(500),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdLevel, IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo, Active
			FROM levels;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdLevel, IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo, Active
			FROM levels
            WHERE IdLevel = p_IdLevel;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO levels(IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo)
					VALUES(p_IdCourse, p_Title, p_Price, p_TextLevel, p_TextLinks, p_UrlVideo);
    END IF;
    
    IF p_Opc = 'IU'
    THEN
		INSERT INTO levels(IdLevel, IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo)
				   VALUES(p_IdLevel, p_IdCourse, p_Title, p_Price, p_TextLevel, p_TextLinks, p_UrlVideo)
                   ON DUPLICATE KEY UPDATE 
						Title = p_Title,
						Price = p_Price,
						TextLevel = p_TextLevel,
						TextLinks = p_TextLinks,
						UrlVideo = p_UrlVideo;
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE levels
			SET Title = p_Title,
				Price = p_Price,
                TextLevel = p_TextLevel,
                TextLinks = p_TextLinks,
                UrlVideo = p_UrlVideo
			WHERE IdLevel = p_IdLevel;
    END IF;
    
    -- Se usa para traerse el nivel mas barato
    IF p_Opc = 'CH'
    THEN
		SELECT IdLevel, Title, Price 
			FROM levels
		WHERE IdCourse = p_IdCourse
        ORDER BY Price ASC
		LIMIT 1;
        
    END IF;
    
    -- Se usa para traerse los niveles de cada curso
    IF p_Opc = 'L'
    THEN
		SELECT IdLevel, title, price, textlevel
			FROM levels
		WHERE IdCourse = p_IdCourse;
    END IF;
    
     -- Se usa para traerse los niveles de cada curso
    IF p_Opc = 'EL'
    THEN
		SELECT IdLevel, title, price, TextLevel, TextLinks
			FROM levels
		WHERE IdCourse = p_IdCourse;
    END IF;
    
    -- se usa para traerse un nivel recien registrado
    IF p_Opc = 'LC'
    THEN
		SELECT IdLevel
			FROM levels
		WHERE IdCourse = p_IdCourse
        ORDER BY IdLevel DESC LIMIT 1;
    END IF;
    
END$$
DELIMITER ;

-- ESTE PROCEDURE YA NO SE VA A USAR
DROP PROCEDURE IF EXISTS `sp_links`;
DELIMITER $$
CREATE PROCEDURE `sp_links`(
	IN p_Opc						CHAR(3),
    IN p_IdLinks					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_Link						VARCHAR(300),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdLinks, IdLevel, Link, Active
			FROM Links;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdLinks, IdLevel, Link, Active
			FROM Links
            WHERE IdLinks = p_IdLinks;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO Links(IdLevel, Link)
					VALUES(p_IdLevel, p_Link);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE Links
			SET Link = p_Link
			WHERE IdLinks = p_IdLinks;
    END IF;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_messages`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_messages`(
	IN p_Opc						CHAR(3),
    IN p_IdMessages					BIGINT,
    IN p_IdUserSender				BIGINT,
    IN p_IdUserAddressee			BIGINT,
    IN p_TextMessage				TEXT,
    IN p_MessageDate				DATETIME,
    IN p_Active						BOOL,
    IN p_searchStr					CHAR(30)
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdMessages, IdUserSender, IdUserAddressee, TextMessage, MessageDate, Active
			FROM messages;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdMessages, IdUserSender, IdUserAddressee, TextMessage, MessageDate, Active
			FROM messages
            WHERE IdMessages = p_IdMessages;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO messages(IdUserSender, IdUserAddressee, TextMessage)
		VALUES (p_IdUserSender, p_IdUserAddressee, p_TextMessage);
                    
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE messages
			SET TextMessage = p_TextMessage,
				MessageDate = p_MessageDate
			WHERE IdMessages = p_IdMessages;
    END IF;
    
    IF p_Opc = 'B'
    THEN

SELECT U.IdUser as SenderID, U.Names as SenderName, U.FirstSurname as SenderFS, U.SecondSurname as SenderSS, U.Avatar as SenderAvatar, U.Active as SenderActive,
		Us.IdUser as AddID, Us.Names as addName, Us.FirstSurname as addFS, Us.SecondSurname as addSS, Us.Avatar as addAvatar, U.Active as addActive,
		M.IdMessages, M.IdUserSender, M.IdUserAddressee, M.TextMessage, M.MessageDate, M.Active as ActiveMss
	FROM users as U
    JOIN users as Us
    LEFT JOIN messages as M
    ON U.IdUser = M.IdUserSender
    AND Us.IdUser = M.IdUserAddressee
    WHERE U.IdUser = p_IdUserSender
	AND Us.IdUser = p_IdUserAddressee 
	AND  M.TextMessage IS NOT NULL
    OR   U.IdUser = p_IdUserAddressee 
	AND Us.IdUser = p_IdUserSender 
    AND  M.TextMessage IS NOT NULL
    ORDER BY M.IdMessages;
    
    END IF;
    
    	IF p_Opc = 'L'
    THEN
	SELECT  U.IdUser as InboxUserID, U.Names as InboxUserName, U.FirstSurname as InboxUserFName, U.SecondSurname as InboxUserSName, U.Avatar as InboxUserAvatar, U.Active as InboxUserAct
	FROM messages as M
	JOIN users as U
    ON (U.IdUser = M.IdUserSender AND M.IdUserSender != p_IdUserSender) OR (U.IdUser = M.IdUserAddressee AND M.IdUserAddressee != p_IdUserSender)
    WHERE (M.IdUserSender = p_IdUserSender) OR (M.IdUserAddressee = p_IdUserSender)
    GROUP BY U.IdUser
    ORDER BY M.MessageDate DESC;
    END IF;
    
        IF p_Opc = 'BI'
    THEN

SELECT Us.IdUser as InboxUserID, Us.Names as InboxUserName, Us.FirstSurname as InboxUserFName, Us.SecondSurname as InboxUserSName, 
		Us.Avatar as InboxUserAvatar, Us.Active as InboxUserAct
	FROM users as Us
    WHERE Us.IdUser = p_IdUserAddressee;    
    END IF;
    
            IF p_Opc = 'S'
    THEN
		SELECT * FROM v_usersandschools 
        WHERE (Names LIKE CONCAT('%', p_searchStr, '%') 
        OR FirstSurname LIKE CONCAT('%', p_searchStr, '%') 
        OR SecondSurname LIKE CONCAT('%', p_searchStr, '%') 
        OR SchoolName LIKE CONCAT('%', p_searchStr, '%'))
		AND IdUser!= p_IdUserSender;
    
    END IF;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_pdfFiles`;
DELIMITER $$
CREATE PROCEDURE `sp_pdfFiles`(
	IN p_Opc						CHAR(3),
    IN p_IdPDFFile					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_NameFile					VARCHAR(500),
    IN p_IdUrlFile					VARCHAR(500),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdPDFFile, IdLevel, NameFile, UrlFile, Active
			FROM pdffiles;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdPDFFile, IdLevel, NameFile, UrlFile, Active
			FROM pdffiles
            WHERE IdPDFFile = p_IdPDFFile;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO pdffiles(IdLevel, NameFile, UrlFile)
					VALUES(p_IdLevel, p_NameFile, p_IdUrlFile);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE pdffiles
			SET NameFile = p_NameFile,
				UrlFile = p_UrlFile
			WHERE IdPDFFile = p_IdPDFFile;
    END IF;
	
    IF p_Opc = 'L'
    THEN
		SELECT IdPDFFile, IdLevel, NameFile, UrlFile
			FROM pdffiles
		WHERE IdLevel = p_IdLevel;
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_Schools`;
DELIMITER $$
CREATE PROCEDURE `sp_Schools` (
	IN p_Opc						CHAR(3),
	IN p_IdSchool					BIGINT,
    IN p_IdUser						BIGINT,
    IN p_Email				 		VARCHAR(100),
	IN p_Password		 			VARCHAR(30),
	IN p_Avatar						MEDIUMBLOB,
	IN p_Name		 				VARCHAR(30),
	IN p_Signupdate	 				DATETIME,
	IN p_LastUpdateDate 			DATETIME,
	IN p_Active						BOOL
)
BEGIN
	DECLARE image MEDIUMBLOB;
    
	IF p_Opc = 'IU'
	THEN
    
		SELECT @image:= Avatar FROM users WHERE IdUser = p_IdUser;
        
		INSERT INTO schools(IdSchool, IdUser, Avatar, Name)
				   VALUES(p_IdSchool, p_IdUser, IFNULL(p_Avatar, @image), p_Name)
                   ON DUPLICATE KEY UPDATE 
						Avatar = IFNULL(p_Avatar, Avatar),
                        Name = p_Name,
                        LastUpdateDate = NOW();
	END IF;
		
	IF p_Opc = 'ID'
	THEN
		SELECT IdSchool, IdUser, Avatar, Name, Signupdate, LastUpdateDate, Active
			FROM schools
		WHERE IdUser = p_IdUser;
	END IF;
	
	IF p_Opc = 'D'
	THEN
		DELETE
			FROM schools
		WHERE IdSchool = p_IdSchool;
	END IF;
    
	IF p_Opc = 'L'
    THEN
		SELECT IdUser, IdSchool
			FROM v_usersandschools
		WHERE Email = p_Email AND
        Password = p_Password;
	END IF;
    
	IF p_Opc = 'S'
    THEN
		SELECT IdSchool, IdUser, Avatar, Name, Signupdate, LastUpdateDate, Active
			FROM schools
		WHERE IdSchool = p_IdSchool;
    END IF;

	IF p_Opc = 'X'
	THEN
		SELECT IdSchool, IdUser, Avatar, Name, Signupdate, LastUpdateDate, Active
			FROM schools;
	END IF;
    
    -- Se usa para traerse las ventas de esa escuela
    IF p_Opc = 'SS'
    THEN
		SELECT SumSchoolTotalEarnings(p_IdSchool) as TotalSales, SumSchoolPaypalEarnings(p_IdSchool) as PaypalSales, SumSchoolCardEarnings(p_IdSchool) as CardSales;
    END IF;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_Users`;
DELIMITER $$
CREATE PROCEDURE `sp_Users` (
	IN p_Opc						CHAR(3),
	IN p_IdUser 					BIGINT,
	IN p_Names 						VARCHAR(100),
	IN p_FirstSurname 				VARCHAR(30),
	IN p_SecondSurname 	 			VARCHAR(30),
	IN p_Avatar					 	MEDIUMBLOB,
	IN p_Gender						TINYINT,
	IN p_Email				 		VARCHAR(100),
	IN p_Password		 			VARCHAR(30),
	IN p_Birthdate					DATE,
	IN p_Signupdate	 				DATETIME,
	IN p_LastUpdateDate 			DATETIME,
	IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'I'
	THEN 
		INSERT INTO users(Names, FirstSurname, SecondSurname, Avatar, Gender, Email, Password, Birthdate)
				   VALUES(p_Names, p_FirstSurname, p_SecondSurname, p_Avatar, p_Gender, p_Email, p_Password, p_Birthdate);
	END IF;
	IF p_Opc = 'U'
	THEN
		UPDATE users
			SET Names = p_Names,
				firstSurname = p_FirstSurname,				
                secondSurname = p_SecondSurname,
				Avatar = -- IF(p_Avatar IS NULL, Avatar, p_Avatar),
				IFNULL(p_Avatar, Avatar),
                Gender = p_Gender,
				Email = p_Email,
                Password = p_Password,
				Birthdate = p_Birthdate,
                LastUpdateDate = NOW()
			WHERE IdUser = p_IdUser;
            
	END IF;

	IF p_Opc = 'D'
	THEN
		DELETE
			FROM users
		WHERE IdUser = p_IdUser;
	END IF;

	IF p_Opc = 'X'
	THEN
		SELECT IdUser, Names, FirstSurname, SecondSurname, Avatar, Gender, Email, Password, Birthdate, Signupdate, LastUpdateDate, Active
			FROM users;
	END IF;
    
    IF p_Opc = 'L'
    THEN
		SELECT IdUser
			FROM users
		WHERE Email = p_Email AND
        Password = p_Password;
	END IF;
    
	IF p_Opc = 'ID'
    THEN
		SELECT IdUser, Names, FirstSurname, SecondSurname, Avatar, Gender, Email, Password, Birthdate, Signupdate, LastUpdateDate, Active
			FROM users
		WHERE IdUser = p_IdUser;
	END IF;
    
    IF p_Opc = 'US'
    THEN
		SELECT S.IdUser, S.IdSchool, S.Avatar, S.Name
			FROM users AS U
            INNER JOIN schools AS S
            ON U.IdUser = S.IdUser
		WHERE S.IdUser = p_IdUser;
	END IF;

END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_usersbuylevels`;
DELIMITER $$
CREATE PROCEDURE `sp_usersbuylevels`(
	IN p_Opc						CHAR(3),
    IN p_IdPurchase					BIGINT,
    IN p_IdUser						BIGINT,
	IN p_IdCourse					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_PurchasePrice				DECIMAL(15,2),
    IN p_PaymentMethod				TINYINT,
    IN p_LastVisitDate				DATETIME,
    IN p_Progress					TINYINT(1),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdPurchase, IdUser, IdLevel, PurchasePrice, LastVisitDate, PaymentMethod, Progress, Active
			FROM usersbuylevels;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdPurchase, IdUser, IdLevel, PurchasePrice, LastVisitDate, PaymentMethod, Progress, Active
			FROM usersbuylevels
            WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO usersbuylevels(IdUser, IdLevel, PurchasePrice, PaymentMethod)
					VALUES(p_IdUser, p_IdLevel, p_PurchasePrice, p_PaymentMethod);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE usersbuylevels
			SET LastVisitDate = p_LastVisitDate,
				PurchasePrice = p_PurchasePrice,
                PaymentMethod = p_PaymentMethod,
				Progress = p_Progress
			WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    -- Se usa para el updatear el progreso
    IF p_Opc = 'P'
    THEN
		UPDATE usersbuylevels
			SET Progress = p_Progress
		WHERE IdUser = p_IdUser AND IdLevel = p_IdLevel;
    END IF;
    
    -- Se usa para traerse los niveles comprados de un usuario
    IF p_Opc = 'L'
    THEN
		SELECT UBL.IdLevel, L.IdCourse, L.Title
			 FROM usersbuylevels AS UBL
             INNER JOIN levels AS L
             ON L.IdLevel = UBL.IdLevel
             INNER JOIN courses AS C
             ON C.IdCourse = L.IdCourse
		WHERE C.IdCourse = p_IdCourse
        AND UBL.IdUser = p_IdUser;
    END IF;
    
	-- Se usa para traerse el nivel a visualizar de un usuario
    IF p_Opc = 'V'
    THEN
		SELECT L.IdLevel, L.Title, L.TextLevel, L.TextLinks, L.UrlVideo, UBL.LastVisitDate, UBL.Progress
			 FROM usersbuylevels AS UBL
             INNER JOIN levels AS L
             ON L.IdLevel = UBL.IdLevel
		WHERE UBL.IdLevel = p_IdLevel
        AND UBL.IdUser = p_IdUser;
    END IF;
    
	-- Se usa para actualizar la ultima vez que se visito ese nivel
    IF p_Opc = 'LVD'
    THEN
		UPDATE usersbuylevels
			SET LastVisitDate = CURRENT_TIMESTAMP()
		WHERE IdUser = p_IdUser AND IdLevel = p_IdLevel;
    END IF;

 -- Usado para mostrar los niveles que no tiene al usuario a la hora de querer comprar
    IF p_Opc = 'LB'
    THEN
		SELECT  L.IdLevel, L.title, L.price, L.textlevel
				FROM levels AS L
		WHERE IdCourse = p_IdCourse 
        AND NOT EXISTS (SELECT 1 FROM usersbuylevels as UBL WHERE UBL.IdLevel = L.IdLevel AND UBL.IdUser = p_IdUser);
        
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_usersbuycourses`;
DELIMITER $$
CREATE PROCEDURE `sp_usersbuycourses`(
	IN p_Opc						CHAR(3),
    IN p_IdPurchase					BIGINT,
    IN p_IdUser						BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_PaymentMethod				TINYINT,
    IN p_PurchasePrice				DECIMAL(15,2),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdPurchase, IdUser, IdCourse, PaymentMethod, PurchasePrice, Active
			FROM usersbuycourses;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdPurchase, IdUser, IdCourse, PaymentMethod, PurchasePrice, Active
			FROM usersbuycourses
            WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO usersbuycourses(IdUser, IdCourse, PaymentMethod, PurchasePrice)
					VALUES(p_IdUser, p_IdCourse, p_PaymentMethod, p_PurchasePrice);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE usersbuycourses
			SET PaymentMethod = p_PaymentMethod,
				PurchasePrice = p_PurchasePrice
			WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    IF p_Opc = 'H'
    THEN
		SELECT EXISTS(SELECT 1 FROM usersbuycourses
		WHERE IdUser = p_IdUser
        AND IdCourse = p_IdCourse) AS HasTheCompleteCourse;
			
    END IF;
    
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS `sp_usersregistercourses`;
DELIMITER $$
CREATE PROCEDURE `sp_usersregistercourses`(
	IN p_Opc						CHAR(3),
    IN p_IdRegistration				BIGINT,
    IN p_IdUser						BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_RegistrationDate			DATETIME,
    IN p_FinishDate					DATETIME,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdRegistration, IdUser, IdCourse, RegistrationDate, FinishDate, Active
			FROM usersregistercourses;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdRegistration, IdUser, IdCourse, RegistrationDate, FinishDate, Active
			FROM usersregistercourses
            WHERE IdRegistration = p_IdRegistration;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO usersregistercourses(IdUser, IdCourse)
					VALUES(p_IdUser, p_IdCourse);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE usersregistercourses
			SET RegistrationDate = p_RegistrationDate,
				FinishDate = p_FinishDate
			WHERE IdRegistration = p_IdRegistration;
    END IF;
    
    -- Esto se usa para traerse los cursos registrados de un usuario y su progreso NOTE: CALAR
    IF p_Opc = 'UC'
    THEN
		SELECT URC.IdCourse, C.Title, C.Image, S.Name as SchoolName, StudentProgressPercentage(URC.IdCourse, URC.IdUser) as Progress, URC.RegistrationDate, URC.FinishDate
			FROM usersregistercourses AS URC
            INNER JOIN courses AS C
            ON URC.IdCourse = C.IdCourse
			INNER JOIN schools AS S
            ON C.IdOwner = S.IdSchool
		WHERE URC.IdUser = p_IdUser;
        
    END IF;
    
    -- Validacion para ver si un usuario esta registrado a un curso
    IF p_Opc = 'R'
    THEN
		SELECT IsUserRegisteredInTheCourse(p_IdCourse, p_IdUser) as IsRegistered;
    END IF;
    
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_advancedsearch`;
DELIMITER $$
CREATE PROCEDURE `sp_advancedsearch`(
	IN p_Opc						CHAR(3),
    IN p_StartDate					DATE,
    IN p_EndDate					DATE,
    IN p_IdCategory					BIGINT,
    IN p_SchoolName					VARCHAR(500),
    IN p_Title						VARCHAR(500)
)
BEGIN
	-- Busqueda Avanzada
	IF p_Opc = 'B'
    THEN
		SELECT  SCSR.IdCourse, SCSR.Title, SCSR.Image, SCSR.Name as SchoolName, SCSR.TotalPrice, SCSR.Rating	 
			FROM v_schoolcoursesstudentsrating AS SCSR
            INNER JOIN coursescategories AS CC
            ON SCSR.IdCourse = CC.IdCourse
		WHERE IF(   p_Title IS NULL, 1, SCSR.Title LIKE CONCAT( '%', p_Title, '%') 		)
        AND IF  (   IFNULL(p_IdCategory, -1) = -1, 1, CC.IdCategory = p_IdCategory  	)
        AND IF	( 	IFNULL(p_SchoolName, -1) = -1, 1, SCSR.Name = p_SchoolName       	)
        AND IF	( 	p_StartDate IS NULL, 1, SCSR.CreationDate >= p_StartDate         	)
        AND IF	( 	p_EndDate IS NULL, 1, SCSR.CreationDate <= p_EndDate  				)
		AND SCSR.Active = 1
        GROUP BY SCSR.IdCourse
        ORDER BY SCSR.CreationDate DESC;
        
    END IF;
    
END$$
DELIMITER ;