DELIMITER $$

-- Esta funcion se usa para ver si un usuario ha completado un curso para asi habilitar el que pueda comentar en la presentacion del curso, regresa un booleano
-- Esta funcion se puede dejar de usar y usar la de StudentProgressPercentage 
-- TODO: CHECAR SI FUNCIONA
CREATE FUNCTION HasCompletedTheCourse(
	p_IdCourse BIGINT,
    p_IdUser BIGINT
)
RETURNS BOOLEAN
DETERMINISTIC
BEGIN
	
	DECLARE levelsCompleted INT;
    DECLARE CourseLevels INT;
    DECLARE hasCompletedTheCourse BOOLEAN;
	
	SELECT SUM(Progress)
		INTO levelsCompleted
		FROM usersbuylevels as UBL
		INNER JOIN levels as L
		ON UBL.IdLevel = L.IdLevel
		INNER JOIN courses as C
		ON L.IdCourse = C.IdCourse
		WHERE IdUser = p_IdUser
		AND C.IdCourse = p_IdCourse;
	
    SELECT NumberOfLevels
	 	INTO CourseLevels
		FROM v_coursesnumberoflevels
		WHERE IdCourse = p_IdCourse;
        
	IF(levelsCompleted < CourseLevels)
    THEN
	 SET hasCompletedTheCourse = FALSE; 
    ELSEIF(levelsCompleted = CourseLevels)
    THEN
     SET hasCompletedTheCourse = TRUE;
	END IF;
	
    RETURN (hasCompletedTheCourse);
END$$
DELIMITER ;

-- Se usa para ver si un usuario ya esta registrado para no volverlo a registrar si compra un nivel
DELIMITER $$
CREATE FUNCTION IsUserRegisteredInTheCourse(
	p_IdCourse BIGINT,
    p_IdUser BIGINT
)
RETURNS BOOLEAN
DETERMINISTIC
BEGIN

	DECLARE Registration BIGINT;
    DECLARE IsRegistered BOOLEAN DEFAULT FALSE;
    
	SELECT IdRegistration 
		INTO Registration
		FROM usersregistercourses
	WHERE IdUser = p_IdUser AND IdCourse = p_IdCourse;
    
    IF(Registration IS NOT NULL)
    THEN 	
		SET IsRegistered = TRUE;
    END IF;
    
	RETURN IsRegistered;
    
END$$
DELIMITER ;

-- Se usa para ver el progreso de un usuario en un curso en base del 100% 
DELIMITER $$
CREATE FUNCTION StudentProgressPercentage(
	p_IdCourse BIGINT,
    p_IdUser BIGINT
)
RETURNS INT
DETERMINISTIC
BEGIN
    DECLARE NumberOfLevels INT;
    DECLARE UserLevelsCompleted INT;
    DECLARE Percentage INT;
    
	SELECT COUNT(L.IdCourse)
		INTO NumberOfLevels
		FROM courses as C
        INNER JOIN levels as L
        ON C.IdCourse = L.IdCourse
	WHERE C.IdCourse = p_IdCourse;
    
	SELECT SUM(UBL.Progress) 
		INTO UserLevelsCompleted
		FROM usersbuylevels as UBL
		INNER JOIN levels as L
			ON UBL.IdLevel = L.IdLevel
		INNER JOIN courses as C
			ON L.IdCourse = C.IdCourse
	WHERE UBL.IdUser = p_IdUser
	AND C.IdCourse = p_IdCourse;
        
	SET Percentage = (UserLevelsCompleted * 100)/NumberOfLevels; 
    
    RETURN Percentage;
    
END$$
DELIMITER ;

-- Suma las ventas de un curso
DELIMITER $$
CREATE FUNCTION SumCourseEarnings(
	p_IdCourse BIGINT
)
RETURNS DECIMAL(15,2)
DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    
	SELECT SUM(PurchasePrice) 
		INTO EarningsByCourse
		FROM usersbuycourses
	WHERE IdCourse = p_IdCourse;
    
    SELECT SUM(UBL.PurchasePrice)
		INTO EarningsByLevels
		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
	WHERE C.IdCourse = p_IdCourse;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END$$
DELIMITER ;
    
-- Suma todas las ventas de una escuela 
DELIMITER $$
CREATE FUNCTION SumSchoolTotalEarnings(
	p_IdSchool BIGINT
)
RETURNS DECIMAL(15,2)
DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    
    SELECT SUM(UBC.PurchasePrice)
	INTO EarningsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool;
    
    SELECT SUM(UBL.PurchasePrice)
	INTO EarningsByLevels
		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END$$
DELIMITER ;

-- Suma todas las ventas por paypal de una escuela
DELIMITER $$
CREATE FUNCTION SumSchoolPaypalEarnings(
	p_IdSchool BIGINT
)
RETURNS DECIMAL(15,2)
DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    SELECT SUM(UBC.PurchasePrice)
  	INTO EarningsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBC.PaymentMethod = 1;
    
    SELECT SUM(UBL.PurchasePrice)
 	INTO EarningsByLevels
 		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBL.PaymentMethod = 1;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END$$
DELIMITER ;
    
 -- Suma todas las ventas por tarjeta de una escuela
DELIMITER $$
CREATE FUNCTION SumSchoolCardEarnings(
	p_IdSchool BIGINT
)
RETURNS DECIMAL(15,2)
DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    SELECT SUM(UBC.PurchasePrice)
  	INTO EarningsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBC.PaymentMethod = 2;
    
    SELECT SUM(UBL.PurchasePrice)
 	INTO EarningsByLevels
 		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBL.PaymentMethod = 2;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END$$
DELIMITER ;

 -- Suma todas lo invertido de un usuario en un curso
DELIMITER $$
CREATE FUNCTION SumUserTotalPayments(
	p_IdUser BIGINT,
    p_IdCourse BIGINT
)
RETURNS DECIMAL(15,2)
DETERMINISTIC
BEGIN
    DECLARE PaymentsByCourse DECIMAL(15,2);
    DECLARE PaymentsByLevels DECIMAL(15,2);
    
    SELECT SUM(UBC.PurchasePrice)
	INTO PaymentsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN users AS U
        ON U.IdUser = UBC.IdUser
	WHERE U.IdUser = p_IdUser
    AND UBC.IdCourse = p_IdCourse;
    
    SELECT SUM(UBL.PurchasePrice)
 	INTO PaymentsByLevels
 		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN users AS U
        ON U.IdUser = UBL.IdUser
	WHERE U.IdUser = p_IdUser
	AND C.IdCourse = p_IdCourse;
    
    
    RETURN IFNULL(PaymentsByCourse, 0.00) + IFNULL(PaymentsByLevels, 0.00);
    
END$$
DELIMITER ;
    
 