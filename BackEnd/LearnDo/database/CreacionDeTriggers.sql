-- Este trigger se dispara cuando un usuario compra un curso y lo que hace es insertar todos los niveles de ese curso
DELIMITER $$
CREATE TRIGGER t_InsertBoughtLevels
    AFTER INSERT
    ON usersbuycourses FOR EACH ROW
BEGIN
    INSERT INTO usersbuylevels(IdUser, IdLevel, PurchasePrice, PaymentMethod)
	SELECT NEW.IdUser, IdLevel, 0 as PurchasePrice, NEW.PaymentMethod
		FROM courses as C
        INNER JOIN levels as L
        ON C.IdCourse = L.IdCourse
	WHERE C.IdCourse = NEW.IdCourse;
END$$    
DELIMITER ;

-- Este trigger se dispara cuando se updatea un curso y lo que hace es eliminar todas las categorias del curso porque despues se van a cargar las nuevas
DELIMITER $$
CREATE TRIGGER t_DeleteCourseCategoriesAfterCourseUpdate
    AFTER UPDATE
    ON courses FOR EACH ROW
BEGIN
	IF(NEW.Active = OLD.Active)
    THEN
		DELETE FROM coursescategories 
			WHERE IdCourse = NEW.IdCourse;
	END IF;
END$$    
DELIMITER ;

-- Este trigger se dispara cuando se updatea un nivel y lo que hace es eliminar todas los registros de archivos relacionados a el porque despues se van a cargar los nuevos archivos
DELIMITER $$
CREATE TRIGGER t_DeleteFilesAfterLevelUpdate
    AFTER UPDATE
    ON levels FOR EACH ROW
BEGIN
    DELETE FROM pdffiles 
		WHERE IdLevel = NEW.IdLevel;
        
	DELETE FROM images 
		WHERE IdLevel = NEW.IdLevel;
END$$    
DELIMITER ;


-- Este trigger se dispara cuando se inserta un nuevo nivel lo que hace es registrarle ese nivel a los usuarios que tienen comprado todo el curso TODO: CHECAR COMPORTAMIENTO
DELIMITER $$
CREATE TRIGGER t_AddLevelToCourseStudents
    AFTER INSERT
    ON levels FOR EACH ROW
BEGIN

	IF EXISTS( SELECT 1 FROM usersbuycourses WHERE IdCourse = NEW.IdCourse ) 
	THEN
		INSERT INTO usersbuylevels(IdUser, IdLevel, PurchasePrice, PaymentMethod)
		SELECT IdUser, NEW.IdLevel, 0 as PurchasePrice, UBC.PaymentMethod
			FROM usersbuycourses as UBC
		WHERE IdCourse = NEW.IdCourse;
    END IF;
    
END$$    
DELIMITER ;


-- Este trigger se dispara cuando se updatea el progreso de un nivel, chequea si ya se le puede setear la fecha de termino del curso CALAR
DELIMITER $$
CREATE TRIGGER t_checkProgressToFinishDate
    AFTER UPDATE
    ON usersbuylevels FOR EACH ROW
BEGIN
	
    DECLARE IdCourse BIGINT;
    
    SELECT URC.IdCourse
		INTO IdCourse
		FROM usersregistercourses as URC
        INNER JOIN courses as C
        ON C.IdCourse = URC.IdCourse
        INNER JOIN levels as L
        ON L.IdCourse = C.IdCourse
	WHERE URC.IdUser = NEW.IdUser AND L.IdLevel = NEW.IdLevel;
    
    IF(SELECT HasCompletedTheCourse(IdCourse, NEW.IdUser) 
    AND (SELECT FinishDate FROM usersregistercourses AS URC WHERE IdUser = NEW.IdUser AND URC.IdCourse = IdCourse) IS NULL)
    THEN 
    
		UPDATE usersregistercourses AS URC
			SET FinishDate = CURRENT_TIMESTAMP()
		WHERE URC.IdUser = NEW.IdUser AND URC.IdCourse = IdCourse;
        
    END IF;
    
END$$    
DELIMITER ;