-- MariaDB dump 10.19  Distrib 10.4.20-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: learndo
-- ------------------------------------------------------
-- Server version	10.4.20-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `IdCategory` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCategory`),
  UNIQUE KEY `IdCategory_UNIQUE` (`IdCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaries`
--

DROP TABLE IF EXISTS `commentaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentaries` (
  `IdCommentary` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUser` bigint(20) NOT NULL,
  `IdCourse` bigint(20) NOT NULL,
  `TextCommentary` text NOT NULL,
  `Rating` smallint(6) NOT NULL,
  `DateCommentary` date NOT NULL DEFAULT current_timestamp(),
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCommentary`),
  UNIQUE KEY `IdCommentary_UNIQUE` (`IdCommentary`),
  KEY `FK_COMMENTARY_USER_idx` (`IdUser`),
  KEY `FK_COMMENTARY_COURSE_idx` (`IdCourse`),
  CONSTRAINT `FK_COMMENTARY_COURSE` FOREIGN KEY (`IdCourse`) REFERENCES `courses` (`IdCourse`),
  CONSTRAINT `FK_COMMENTARY_USER` FOREIGN KEY (`IdUser`) REFERENCES `users` (`IdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaries`
--

LOCK TABLES `commentaries` WRITE;
/*!40000 ALTER TABLE `commentaries` DISABLE KEYS */;
/*!40000 ALTER TABLE `commentaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `IdCourse` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdOwner` bigint(20) NOT NULL,
  `Title` varchar(400) NOT NULL,
  `Image` mediumblob NOT NULL,
  `Description` varchar(400) NOT NULL,
  `TotalPrice` decimal(15,2) NOT NULL,
  `CreationDate` date NOT NULL DEFAULT current_timestamp(),
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCourse`),
  UNIQUE KEY `IdCourses_UNIQUE` (`IdCourse`),
  KEY `FK_COURSES_SCHOOL_idx` (`IdOwner`),
  CONSTRAINT `FK_COURSES_SCHOOL` FOREIGN KEY (`IdOwner`) REFERENCES `schools` (`IdSchool`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER t_DeleteCourseCategoriesAfterCourseUpdate
    AFTER UPDATE
    ON courses FOR EACH ROW
BEGIN
	IF(NEW.Active = OLD.Active)
    THEN
		DELETE FROM coursescategories 
			WHERE IdCourse = NEW.IdCourse;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `coursescategories`
--

DROP TABLE IF EXISTS `coursescategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coursescategories` (
  `IdCoursesCategory` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCourse` bigint(20) NOT NULL,
  `IdCategory` bigint(20) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCoursesCategory`),
  UNIQUE KEY `IdCoursesCategory_UNIQUE` (`IdCoursesCategory`),
  KEY `FK_COURSECAT_COURSE_idx` (`IdCourse`),
  KEY `FK_COURSECAT_CATEGORY_idx` (`IdCategory`),
  CONSTRAINT `FK_COURSECAT_CATEGORY` FOREIGN KEY (`IdCategory`) REFERENCES `categories` (`IdCategory`),
  CONSTRAINT `FK_COURSECAT_COURSE` FOREIGN KEY (`IdCourse`) REFERENCES `courses` (`IdCourse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coursescategories`
--

LOCK TABLES `coursescategories` WRITE;
/*!40000 ALTER TABLE `coursescategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `coursescategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formasdepago`
--

DROP TABLE IF EXISTS `formasdepago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formasdepago` (
  `IdFormaDePago` tinyint(4) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) NOT NULL,
  `Activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdFormaDePago`),
  UNIQUE KEY `IdFormaDePago_UNIQUE` (`IdFormaDePago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formasdepago`
--

LOCK TABLES `formasdepago` WRITE;
/*!40000 ALTER TABLE `formasdepago` DISABLE KEYS */;
/*!40000 ALTER TABLE `formasdepago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `IdImages` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdLevel` bigint(20) NOT NULL,
  `Image` mediumblob NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdImages`),
  UNIQUE KEY `IdImages_UNIQUE` (`IdImages`),
  KEY `FK_IMAGES_LEVEL_idx` (`IdLevel`),
  CONSTRAINT `FK_IMAGES_LEVEL` FOREIGN KEY (`IdLevel`) REFERENCES `levels` (`IdLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `levels` (
  `IdLevel` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCourse` bigint(20) NOT NULL,
  `Title` varchar(400) NOT NULL,
  `Price` decimal(15,2) NOT NULL,
  `TextLevel` varchar(500) NOT NULL,
  `TextLinks` varchar(800) NOT NULL,
  `UrlVideo` varchar(500) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdLevel`),
  UNIQUE KEY `IdLevel_UNIQUE` (`IdLevel`),
  KEY `FK_LEVEL_COURSES_idx` (`IdCourse`),
  CONSTRAINT `FK_LEVEL_COURSES` FOREIGN KEY (`IdCourse`) REFERENCES `courses` (`IdCourse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels`
--

LOCK TABLES `levels` WRITE;
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER t_AddLevelToCourseStudents
    AFTER INSERT
    ON levels FOR EACH ROW
BEGIN

	IF EXISTS( SELECT 1 FROM usersbuycourses WHERE IdCourse = NEW.IdCourse ) 
	THEN
		INSERT INTO usersbuylevels(IdUser, IdLevel, PurchasePrice, PaymentMethod)
		SELECT IdUser, NEW.IdLevel, 0 as PurchasePrice, UBC.PaymentMethod
			FROM usersbuycourses as UBC
		WHERE IdCourse = NEW.IdCourse;
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER t_DeleteFilesAfterLevelUpdate
    AFTER UPDATE
    ON levels FOR EACH ROW
BEGIN
    DELETE FROM pdffiles 
		WHERE IdLevel = NEW.IdLevel;
        
	DELETE FROM images 
		WHERE IdLevel = NEW.IdLevel;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `IdLinks` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdLevel` bigint(20) NOT NULL,
  `Link` varchar(300) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdLinks`),
  UNIQUE KEY `IdLinks_UNIQUE` (`IdLinks`),
  KEY `FK_LINK_LEVEL_idx` (`IdLevel`),
  CONSTRAINT `FK_LINK_LEVEL` FOREIGN KEY (`IdLevel`) REFERENCES `levels` (`IdLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `IdMessages` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUserSender` bigint(20) NOT NULL,
  `IdUserAddressee` bigint(20) NOT NULL,
  `TextMessage` text NOT NULL,
  `MessageDate` datetime NOT NULL DEFAULT current_timestamp(),
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdMessages`),
  UNIQUE KEY `IdMessages_UNIQUE` (`IdMessages`),
  KEY `FK_MESSAGES_SENDER_idx` (`IdUserSender`),
  KEY `FK_MESSAGES_ADDRESSEE_idx` (`IdUserAddressee`),
  CONSTRAINT `FK_MESSAGES_ADDRESSEE` FOREIGN KEY (`IdUserAddressee`) REFERENCES `users` (`IdUser`),
  CONSTRAINT `FK_MESSAGES_SENDER` FOREIGN KEY (`IdUserSender`) REFERENCES `users` (`IdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdffiles`
--

DROP TABLE IF EXISTS `pdffiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdffiles` (
  `IdPDFFile` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdLevel` bigint(20) NOT NULL,
  `NameFile` varchar(500) DEFAULT NULL,
  `UrlFile` varchar(500) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdPDFFile`),
  UNIQUE KEY `IdPDFFiles_UNIQUE` (`IdPDFFile`),
  KEY `FK_FILEPDF_LEVEL_idx` (`IdLevel`),
  CONSTRAINT `FK_FILEPDF_LEVEL` FOREIGN KEY (`IdLevel`) REFERENCES `levels` (`IdLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdffiles`
--

LOCK TABLES `pdffiles` WRITE;
/*!40000 ALTER TABLE `pdffiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `pdffiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schools` (
  `IdSchool` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUser` bigint(20) NOT NULL,
  `Avatar` mediumblob NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Signupdate` datetime NOT NULL DEFAULT current_timestamp(),
  `LastUpdateDate` datetime NOT NULL DEFAULT current_timestamp(),
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdSchool`),
  UNIQUE KEY `IdSchool_UNIQUE` (`IdSchool`),
  KEY `FK_SCHOOL_USER_idx` (`IdUser`),
  CONSTRAINT `FK_SCHOOL_USER` FOREIGN KEY (`IdUser`) REFERENCES `users` (`IdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools`
--

LOCK TABLES `schools` WRITE;
/*!40000 ALTER TABLE `schools` DISABLE KEYS */;
/*!40000 ALTER TABLE `schools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `IdUser` bigint(20) NOT NULL AUTO_INCREMENT,
  `Names` varchar(100) NOT NULL,
  `FirstSurname` varchar(30) NOT NULL,
  `SecondSurname` varchar(30) NOT NULL,
  `Avatar` mediumblob NOT NULL,
  `Gender` tinyint(4) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `Birthdate` date NOT NULL,
  `Signupdate` datetime NOT NULL DEFAULT current_timestamp(),
  `LastUpdateDate` datetime NOT NULL DEFAULT current_timestamp(),
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdUser`),
  UNIQUE KEY `IdUser_UNIQUE` (`IdUser`),
  UNIQUE KEY `Email_UNIQUE` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersbuycourses`
--

DROP TABLE IF EXISTS `usersbuycourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersbuycourses` (
  `IdPurchase` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUser` bigint(20) NOT NULL,
  `IdCourse` bigint(20) NOT NULL,
  `PaymentMethod` tinyint(4) NOT NULL,
  `PurchasePrice` decimal(15,2) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdPurchase`),
  UNIQUE KEY `IdPurchase_UNIQUE` (`IdPurchase`),
  KEY `IdUser_idx` (`IdUser`),
  KEY `IdCourse_idx` (`IdCourse`),
  CONSTRAINT `IdCourse` FOREIGN KEY (`IdCourse`) REFERENCES `courses` (`IdCourse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `IdUser` FOREIGN KEY (`IdUser`) REFERENCES `users` (`IdUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersbuycourses`
--

LOCK TABLES `usersbuycourses` WRITE;
/*!40000 ALTER TABLE `usersbuycourses` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersbuycourses` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER t_InsertBoughtLevels
    AFTER INSERT
    ON usersbuycourses FOR EACH ROW
BEGIN
    INSERT INTO usersbuylevels(IdUser, IdLevel, PurchasePrice, PaymentMethod)
	SELECT NEW.IdUser, IdLevel, 0 as PurchasePrice, NEW.PaymentMethod
		FROM courses as C
        INNER JOIN levels as L
        ON C.IdCourse = L.IdCourse
	WHERE C.IdCourse = NEW.IdCourse;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `usersbuylevels`
--

DROP TABLE IF EXISTS `usersbuylevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersbuylevels` (
  `IdPurchase` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUser` bigint(20) NOT NULL,
  `IdLevel` bigint(20) NOT NULL,
  `PaymentMethod` tinyint(4) DEFAULT NULL,
  `PurchasePrice` decimal(15,2) DEFAULT NULL,
  `LastVisitDate` datetime DEFAULT NULL,
  `Progress` tinyint(1) NOT NULL DEFAULT 0,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdPurchase`),
  UNIQUE KEY `IdPurchase_UNIQUE` (`IdPurchase`),
  KEY `FK_PURCHASE_USER_idx` (`IdUser`),
  KEY `FK_PURCHASE_LEVEL_idx` (`IdLevel`),
  CONSTRAINT `FK_PURCHASE_LEVEL` FOREIGN KEY (`IdLevel`) REFERENCES `levels` (`IdLevel`),
  CONSTRAINT `FK_PURCHASE_USER` FOREIGN KEY (`IdUser`) REFERENCES `users` (`IdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersbuylevels`
--

LOCK TABLES `usersbuylevels` WRITE;
/*!40000 ALTER TABLE `usersbuylevels` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersbuylevels` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER t_checkProgressToFinishDate
    AFTER UPDATE
    ON usersbuylevels FOR EACH ROW
BEGIN
	
    DECLARE IdCourse BIGINT;
    
    SELECT URC.IdCourse
		INTO IdCourse
		FROM usersregistercourses as URC
        INNER JOIN courses as C
        ON C.IdCourse = URC.IdCourse
        INNER JOIN levels as L
        ON L.IdCourse = C.IdCourse
	WHERE URC.IdUser = NEW.IdUser AND L.IdLevel = NEW.IdLevel;
    
    IF(SELECT HasCompletedTheCourse(IdCourse, NEW.IdUser) 
    AND (SELECT FinishDate FROM usersregistercourses AS URC WHERE IdUser = NEW.IdUser AND URC.IdCourse = IdCourse) IS NULL)
    THEN 
    
		UPDATE usersregistercourses AS URC
			SET FinishDate = CURRENT_TIMESTAMP()
		WHERE URC.IdUser = NEW.IdUser AND URC.IdCourse = IdCourse;
        
    END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `usersregistercourses`
--

DROP TABLE IF EXISTS `usersregistercourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersregistercourses` (
  `IdRegistration` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUser` bigint(20) NOT NULL,
  `IdCourse` bigint(20) NOT NULL,
  `RegistrationDate` datetime NOT NULL DEFAULT current_timestamp(),
  `FinishDate` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdRegistration`),
  UNIQUE KEY `IdRegistration_UNIQUE` (`IdRegistration`),
  KEY `FK_REGISTER_USER_idx` (`IdUser`),
  KEY `FK_REGISTER_COURSES_idx` (`IdCourse`),
  CONSTRAINT `FK_REGISTER_COURSES` FOREIGN KEY (`IdCourse`) REFERENCES `courses` (`IdCourse`),
  CONSTRAINT `FK_REGISTER_USER` FOREIGN KEY (`IdUser`) REFERENCES `users` (`IdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersregistercourses`
--

LOCK TABLES `usersregistercourses` WRITE;
/*!40000 ALTER TABLE `usersregistercourses` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersregistercourses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_coursesnumberoflevels`
--

DROP TABLE IF EXISTS `v_coursesnumberoflevels`;
/*!50001 DROP VIEW IF EXISTS `v_coursesnumberoflevels`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_coursesnumberoflevels` (
  `IdCourse` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `NumberOfLevels` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_coursesstudentsandsales`
--

DROP TABLE IF EXISTS `v_coursesstudentsandsales`;
/*!50001 DROP VIEW IF EXISTS `v_coursesstudentsandsales`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_coursesstudentsandsales` (
  `IdCourse` tinyint NOT NULL,
  `IdOwner` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `Image` tinyint NOT NULL,
  `Students` tinyint NOT NULL,
  `TotalSales` tinyint NOT NULL,
  `Active` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_schoolcoursesstudentsrating`
--

DROP TABLE IF EXISTS `v_schoolcoursesstudentsrating`;
/*!50001 DROP VIEW IF EXISTS `v_schoolcoursesstudentsrating`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_schoolcoursesstudentsrating` (
  `IdSchool` tinyint NOT NULL,
  `Name` tinyint NOT NULL,
  `IdCourse` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `Image` tinyint NOT NULL,
  `Description` tinyint NOT NULL,
  `TotalPrice` tinyint NOT NULL,
  `CreationDate` tinyint NOT NULL,
  `Students` tinyint NOT NULL,
  `Rating` tinyint NOT NULL,
  `Active` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_studentdetailedboughtcourse`
--

DROP TABLE IF EXISTS `v_studentdetailedboughtcourse`;
/*!50001 DROP VIEW IF EXISTS `v_studentdetailedboughtcourse`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_studentdetailedboughtcourse` (
  `IdCourse` tinyint NOT NULL,
  `IdOwner` tinyint NOT NULL,
  `IdUser` tinyint NOT NULL,
  `Avatar` tinyint NOT NULL,
  `Names` tinyint NOT NULL,
  `FirstSurname` tinyint NOT NULL,
  `SecondSurname` tinyint NOT NULL,
  `Progress` tinyint NOT NULL,
  `PurchasePrice` tinyint NOT NULL,
  `PaymentMethod` tinyint NOT NULL,
  `RegistrationDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_studentdetailedboughtlevels`
--

DROP TABLE IF EXISTS `v_studentdetailedboughtlevels`;
/*!50001 DROP VIEW IF EXISTS `v_studentdetailedboughtlevels`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_studentdetailedboughtlevels` (
  `IdCourse` tinyint NOT NULL,
  `IdOwner` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `IdUser` tinyint NOT NULL,
  `Avatar` tinyint NOT NULL,
  `Names` tinyint NOT NULL,
  `FirstSurname` tinyint NOT NULL,
  `SecondSurname` tinyint NOT NULL,
  `Progress` tinyint NOT NULL,
  `PurchasePrice` tinyint NOT NULL,
  `PaymentMethod` tinyint NOT NULL,
  `RegistrationDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_studentregistereddetailed`
--

DROP TABLE IF EXISTS `v_studentregistereddetailed`;
/*!50001 DROP VIEW IF EXISTS `v_studentregistereddetailed`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_studentregistereddetailed` (
  `IdCourse` tinyint NOT NULL,
  `IdOwner` tinyint NOT NULL,
  `IdUser` tinyint NOT NULL,
  `Avatar` tinyint NOT NULL,
  `Names` tinyint NOT NULL,
  `FirstSurname` tinyint NOT NULL,
  `SecondSurname` tinyint NOT NULL,
  `Progress` tinyint NOT NULL,
  `TotalPayments` tinyint NOT NULL,
  `RegistrationDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_usersandschools`
--

DROP TABLE IF EXISTS `v_usersandschools`;
/*!50001 DROP VIEW IF EXISTS `v_usersandschools`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_usersandschools` (
  `IdUser` tinyint NOT NULL,
  `Names` tinyint NOT NULL,
  `FirstSurname` tinyint NOT NULL,
  `SecondSurname` tinyint NOT NULL,
  `Avatar` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `Email` tinyint NOT NULL,
  `Password` tinyint NOT NULL,
  `Birthdate` tinyint NOT NULL,
  `Signupdate` tinyint NOT NULL,
  `LastUpdateDate` tinyint NOT NULL,
  `Active` tinyint NOT NULL,
  `IdSchool` tinyint NOT NULL,
  `SchoolAvatar` tinyint NOT NULL,
  `SchoolName` tinyint NOT NULL,
  `SchoolSignupdate` tinyint NOT NULL,
  `SchoolLastUpdateDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'learndo'
--
/*!50003 DROP FUNCTION IF EXISTS `HasCompletedTheCourse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `HasCompletedTheCourse`(p_IdCourse BIGINT,
    p_IdUser BIGINT
) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	
	DECLARE levelsCompleted INT;
    DECLARE CourseLevels INT;
    DECLARE hasCompletedTheCourse BOOLEAN;
	
	SELECT SUM(Progress)
		INTO levelsCompleted
		FROM usersbuylevels as UBL
		INNER JOIN levels as L
		ON UBL.IdLevel = L.IdLevel
		INNER JOIN courses as C
		ON L.IdCourse = C.IdCourse
		WHERE IdUser = p_IdUser
		AND C.IdCourse = p_IdCourse;
	
    SELECT NumberOfLevels
	 	INTO CourseLevels
		FROM v_coursesnumberoflevels
		WHERE IdCourse = p_IdCourse;
        
	IF(levelsCompleted < CourseLevels)
    THEN
	 SET hasCompletedTheCourse = FALSE; 
    ELSEIF(levelsCompleted = CourseLevels)
    THEN
     SET hasCompletedTheCourse = TRUE;
	END IF;
	
    RETURN (hasCompletedTheCourse);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `IsUserRegisteredInTheCourse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `IsUserRegisteredInTheCourse`(p_IdCourse BIGINT,
    p_IdUser BIGINT
) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN

	DECLARE Registration BIGINT;
    DECLARE IsRegistered BOOLEAN DEFAULT FALSE;
    
	SELECT IdRegistration 
		INTO Registration
		FROM usersregistercourses
	WHERE IdUser = p_IdUser AND IdCourse = p_IdCourse;
    
    IF(Registration IS NOT NULL)
    THEN 	
		SET IsRegistered = TRUE;
    END IF;
    
	RETURN IsRegistered;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `StudentProgressPercentage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `StudentProgressPercentage`(p_IdCourse BIGINT,
    p_IdUser BIGINT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
    DECLARE NumberOfLevels INT;
    DECLARE UserLevelsCompleted INT;
    DECLARE Percentage INT;
    
	SELECT COUNT(L.IdCourse)
		INTO NumberOfLevels
		FROM courses as C
        INNER JOIN levels as L
        ON C.IdCourse = L.IdCourse
	WHERE C.IdCourse = p_IdCourse;
    
	SELECT SUM(UBL.Progress) 
		INTO UserLevelsCompleted
		FROM usersbuylevels as UBL
		INNER JOIN levels as L
			ON UBL.IdLevel = L.IdLevel
		INNER JOIN courses as C
			ON L.IdCourse = C.IdCourse
	WHERE UBL.IdUser = p_IdUser
	AND C.IdCourse = p_IdCourse;
        
	SET Percentage = (UserLevelsCompleted * 100)/NumberOfLevels; 
    
    RETURN Percentage;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SumCourseEarnings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `SumCourseEarnings`(p_IdCourse BIGINT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    
	SELECT SUM(PurchasePrice) 
		INTO EarningsByCourse
		FROM usersbuycourses
	WHERE IdCourse = p_IdCourse;
    
    SELECT SUM(UBL.PurchasePrice)
		INTO EarningsByLevels
		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
	WHERE C.IdCourse = p_IdCourse;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SumSchoolCardEarnings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `SumSchoolCardEarnings`(p_IdSchool BIGINT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    SELECT SUM(UBC.PurchasePrice)
  	INTO EarningsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBC.PaymentMethod = 2;
    
    SELECT SUM(UBL.PurchasePrice)
 	INTO EarningsByLevels
 		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBL.PaymentMethod = 2;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SumSchoolPaypalEarnings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `SumSchoolPaypalEarnings`(p_IdSchool BIGINT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    SELECT SUM(UBC.PurchasePrice)
  	INTO EarningsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBC.PaymentMethod = 1;
    
    SELECT SUM(UBL.PurchasePrice)
 	INTO EarningsByLevels
 		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool
    AND UBL.PaymentMethod = 1;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SumSchoolTotalEarnings` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `SumSchoolTotalEarnings`(p_IdSchool BIGINT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
    DECLARE EarningsByCourse DECIMAL(15,2);
    DECLARE EarningsByLevels DECIMAL(15,2);
    
    
    SELECT SUM(UBC.PurchasePrice)
	INTO EarningsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool;
    
    SELECT SUM(UBL.PurchasePrice)
	INTO EarningsByLevels
		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN schools AS S
        ON C.IdOwner = S.IdSchool
	WHERE S.IdSchool = p_IdSchool;
    
    RETURN IFNULL(EarningsByCourse, 0.00) + IFNULL(EarningsByLevels, 0.00);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SumUserTotalPayments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  FUNCTION `SumUserTotalPayments`(p_IdUser BIGINT,
    p_IdCourse BIGINT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
    DECLARE PaymentsByCourse DECIMAL(15,2);
    DECLARE PaymentsByLevels DECIMAL(15,2);
    
    SELECT SUM(UBC.PurchasePrice)
	INTO PaymentsByCourse
		FROM usersbuycourses AS UBC
        INNER JOIN courses AS C
        ON UBC.IdCourse = C.IdCourse
        INNER JOIN users AS U
        ON U.IdUser = UBC.IdUser
	WHERE U.IdUser = p_IdUser
    AND UBC.IdCourse = p_IdCourse;
    
    SELECT SUM(UBL.PurchasePrice)
 	INTO PaymentsByLevels
 		FROM usersbuylevels as UBL
        INNER JOIN levels as L
        ON UBL.IdLevel = L.IdLevel 
        INNER JOIN courses as C
        ON C.IdCourse = L.IdCourse
        INNER JOIN users AS U
        ON U.IdUser = UBL.IdUser
	WHERE U.IdUser = p_IdUser
	AND C.IdCourse = p_IdCourse;
    
    
    RETURN IFNULL(PaymentsByCourse, 0.00) + IFNULL(PaymentsByLevels, 0.00);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_advancedsearch` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_advancedsearch`(
	IN p_Opc						CHAR(3),
    IN p_StartDate					DATE,
    IN p_EndDate					DATE,
    IN p_IdCategory					BIGINT,
    IN p_SchoolName					VARCHAR(500),
    IN p_Title						VARCHAR(500)
)
BEGIN
	-- Busqueda Avanzada
	IF p_Opc = 'B'
    THEN
		SELECT  SCSR.IdCourse, SCSR.Title, SCSR.Image, SCSR.Name as SchoolName, SCSR.TotalPrice, SCSR.Rating	 
			FROM v_schoolcoursesstudentsrating AS SCSR
            INNER JOIN coursescategories AS CC
            ON SCSR.IdCourse = CC.IdCourse
		WHERE IF(   p_Title IS NULL, 1, SCSR.Title LIKE CONCAT( '%', p_Title, '%') 		)
        AND IF  (   IFNULL(p_IdCategory, -1) = -1, 1, CC.IdCategory = p_IdCategory  	)
        AND IF	( 	IFNULL(p_SchoolName, -1) = -1, 1, SCSR.Name = p_SchoolName       	)
        AND IF	( 	p_StartDate IS NULL, 1, SCSR.CreationDate >= p_StartDate         	)
        AND IF	( 	p_EndDate IS NULL, 1, SCSR.CreationDate <= p_EndDate  				)
		AND SCSR.Active = 1
        GROUP BY SCSR.IdCourse
        ORDER BY SCSR.CreationDate DESC;
        
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Categories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_Categories`(
	IN p_Opc						CHAR(3),
	IN p_IdCategory					BIGINT,
    IN p_Name						VARCHAR(30),
	IN p_Description				VARCHAR(100),
	IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCategory, Name, Description, Active
			FROM categories;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdCategory, Name, Description, Active
			FROM categories
            WHERE IdCategory = p_IdCategory;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO categories(Name, Description)
					VALUES(p_Name, p_Description);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE categories
			SET Name = p_Name,
				Description = p_Description
			WHERE IdCategory = p_IdCategory;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_commentaries` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_commentaries`(
	IN p_Opc						CHAR(3),
    IN p_IdCommentary				BIGINT,
    IN p_IdUser						BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_TextCommentary 			TEXT,
    IN p_Rating						SMALLINT,
    IN p_DateCommentary				DATE,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCommentary, IdUser, IdCourse, TextCommentary, Rating, DateCommentary, Active
			FROM commentaries;
    END IF;
    
    -- Trae la información de el comentario donde coincide el usuario y el curso (id)
    IF p_Opc = 'ID'
    THEN
		SELECT IdCommentary, IdUser, IdCourse, TextCommentary, Rating, p_DateCommentary, Active
			FROM commentaries
            WHERE IdUser = p_IdUser
            ANd IdCourse = p_IdCourse;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO commentaries(IdUser, IdCourse, TextCommentary, Rating)
					VALUES(p_IdUser, p_IdCourse, p_TextCommentary, p_Rating);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE commentaries
			SET TextCommentary = p_TextCommentary,
				Rating = p_Rating,
                DateCommentary = CURRENT_TIMESTAMP(),
                Active = p_Active
			WHERE IdCommentary = p_IdCommentary;
    END IF;
    
    -- Se usa para los comentarios de un curso
    IF p_Opc = 'CC'
    THEN 
		SELECT IdCommentary, U.IdUser, U.Avatar, U.Names, U.FirstSurname, U.SecondSurname, IdCourse, TextCommentary, Rating, DateCommentary, C.Active
			FROM commentaries as C
            INNER JOIN users as U
            ON C.IdUser = U.IdUser
		WHERE IdCourse = p_IdCourse
        AND C.Active = 1;
    END IF;
    
    -- SE USA PARA CHECAR SI PUEDE COMENTAR
    IF p_Opc = 'UCC'
    THEN
        SELECT HasCompletedTheCourse(p_IdCourse, p_IdUser) as HasCompletedCourse; 
    END IF;
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_courses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_courses`(
	IN p_Opc						CHAR(3),
    IN p_IdCourse					BIGINT,
    IN p_IdOwner					BIGINT,
    IN p_Title						VARCHAR(400),
    IN p_Image						MEDIUMBLOB,
    IN p_Description				VARCHAR(400),
    IN p_TotalPrice					DECIMAL(15,2),
    IN p_CreationDate				DATE,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCourse, IdOwner, Title, Image, Description, TotalPrice, CreationDate, Active
			FROM courses;
    END IF;
    
    IF p_Opc = 'O'
    THEN
		SELECT IdCourse
			FROM courses
		WHERE IdOwner = p_IdOwner
        ORDER BY IdCourse DESC LIMIT 1;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdCourse, IdOwner, Title, Image, Description, TotalPrice, CreationDate, Active
			FROM courses
            WHERE IdCourse = p_IdCourse;
    END IF;
    
    -- Este se usa para mostrar la presentacion del curso
    IF p_Opc = 'SH'
    THEN
		SELECT Name as SchoolName, IdCourse, Title, Image, Description, TotalPrice, Students, Rating, CreationDate
			FROM v_schoolcoursesstudentsrating
		WHERE IdCourse = p_IdCourse
        AND Active = TRUE;
    END IF;

    IF p_Opc = 'I'
    THEN
		INSERT INTO courses(IdOwner, Title, Image, Description, TotalPrice)
					VALUES(p_IdOwner, p_Title, p_Image, p_Description, p_TotalPrice);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE courses
			SET Title = p_Title,
				Image = IFNULL(p_Image, Image),
                Description = p_Description,
                TotalPrice = p_TotalPrice
			WHERE IdCourse = p_IdCourse;
    END IF;
    IF p_Opc = 'CS'
    THEN
		SELECT IdCourse, IdOwner, Title, Image, Students, TotalSales, Active
			FROM v_coursesstudentsandsales
		WHERE IdOwner = p_IdOwner;
    END IF;
    
    -- Esta es para los cursos mas recientes
    IF p_Opc = 'RC'
    THEN
		SELECT C.IdCourse, S.Name as SchoolName, C.Title, C.Image, C.CreationDate
			FROM courses as C
            INNER JOIN schools as S
            ON C.IdOwner = S.IdSchool
		WHERE C.Active = true
		ORDER BY C.CreationDate DESC, C.IdCourse DESC
        LIMIT 10;
    END IF;
    
    -- Este es para traerse los mas vendidos
    IF p_Opc = 'SC'
    THEN
		SELECT CSS.IdCourse, S.Name as SchoolName, CSS.Title, CSS.Image, CSS.TotalSales
			FROM v_coursesstudentsandsales as CSS
			INNER JOIN schools as S
            ON CSS.IdOwner = S.IdSchool
		WHERE CSS.Active = true
		ORDER BY CSS.TotalSales DESC
        LIMIT 10;
    END IF;
    
    -- Este es para traerse los mejores en rating
    
    IF p_Opc = 'R'
    THEN
		SELECT C.IdCourse, S.Name as SchoolName, C.Title, C.Image, SUM(IFNULL(CM.Rating, 0)) as Rating
			FROM courses as C
            INNER JOIN schools as S
            ON C.IdOwner = S.IdSchool
            LEFT JOIN commentaries as CM
            ON C.IdCourse = CM.IdCourse
		WHERE C.Active = true
		GROUP BY  C.IdCourse
		ORDER BY Rating DESC
        LIMIT 10;
    END IF;
    
	-- Este se usa para traerse los detalles de un curso
    IF p_Opc = 'DC'
    THEN
		SELECT CSS.IdCourse, CSS.Title, CSS.Students, SumCourseEarnings(CSS.IdCourse) as TotalSales, CSS.Active
			FROM v_coursesstudentsandsales as CSS
		WHERE CSS.IdCourse = p_IdCourse AND CSS.IdOwner = p_IdOwner;
    END IF;
    
    -- Usado para ver los estudiantes registrados a un curso
    IF p_Opc = 'SRD'
    THEN
		SELECT IdCourse	, IdUser, Avatar, Names, FirstSurname, SecondSurname, Progress, TotalPayments, RegistrationDate 
			FROM v_studentregistereddetailed
		WHERE IdCourse = p_IdCourse AND IdOwner = p_IdOwner;
    END IF;
    
	-- Usado para ver los detalles de los usuarios que compraron por nivel
    IF p_Opc = 'SBL'
    THEN 
		SELECT IdCourse, Title, IdUser, Avatar, Names, FirstSurname, SecondSurname, IF(Progress = 1, 'Terminado', 'Sin Empezar') AS Progress, PurchasePrice, 
			IF (PaymentMethod = 1, 'Paypal', 'Tarjeta Debito/Credito') AS PaymentMethod, RegistrationDate
			FROM v_studentdetailedboughtlevels
		WHERE IdCourse = p_IdCourse AND IdOwner = p_IdOwner;
	END IF;
    
	-- Usado para ver los detalles de los usuarios que compraron por curso
	IF p_Opc = 'SBC'
    THEN 
		SELECT IdCourse, IdUser, Avatar, Names, FirstSurname, SecondSurname, Progress, PurchasePrice, 
		IF (PaymentMethod = 1, 'Paypal', 'Tarjeta Debito/Credito') AS PaymentMethod, RegistrationDate
			FROM v_studentdetailedboughtcourse
		WHERE IdCourse = p_IdCourse AND IdOwner = p_IdOwner;
	END IF;
    
    IF p_Opc = 'D'
    THEN
		UPDATE courses
			SET Active = NOT Active
		WHERE IdCourse = p_IdCourse;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_coursesCategories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_coursesCategories`(
	IN p_Opc						CHAR(3),
    IN p_IdCoursesCategory			BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_IdCategory					BIGINT,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdCoursesCategory, IdCourse, IdCategory, Active
			FROM coursescategories;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdCoursesCategory, IdCourse, IdCategory, Active
			FROM coursescategories
            WHERE IdCoursesCategory = p_IdCoursesCategory;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO coursescategories(IdCourse, IdCategory)
					VALUES(p_IdCourse, p_IdCategory);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE coursescategories
			SET IdCourse = p_IdCourse,
				IdCategory = p_IdCategory
			WHERE IdCoursesCategory = p_IdCoursesCategory;
    END IF;
    
	-- Con este agarramos las categorias de un curso
    IF p_Opc = 'C'
    THEN
		SELECT CC.IdCategory, C.Name
			FROM coursescategories AS CC
            INNER JOIN categories AS C
            ON CC.IdCategory = C.IdCategory
            INNER JOIN courses AS CS
			ON CC.IdCourse = CS.IdCourse
		WHERE CS.IdCourse = p_IdCourse;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_formasDePago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_formasDePago`(
	IN p_Opc						CHAR(3),
    IN p_IdFormaDePago				TINYINT,
    IN p_Nombre						VARCHAR(20),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdFormaDePago, Nombre, Active
			FROM formasdepago;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdFormaDePago, Nombre, Active
			FROM formasdepago
            WHERE IdFormaDePago = p_IdFormaDePago;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO formasdepago(Nombre)
					VALUES(p_Nombre);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE formasdepago
			SET Nombre = p_Nombre
			WHERE IdFormaDePago = p_IdFormaDePago;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_images` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_images`(
	IN p_Opc						CHAR(3),
    IN p_IdImages					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_Image						MEDIUMBLOB,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdImages, IdLevel, Image, Active
			FROM images;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdImages, IdLevel, Image, Active
			FROM images
            WHERE IdImages = p_IdImages;
    END IF;
    
    IF p_Opc = 'I'
    THEN
		INSERT INTO images(IdLevel, Image)
					VALUES(p_IdLevel, p_Image);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE images
			SET Image = p_Image
			WHERE IdImage = p_IdImage;
    END IF;
    
    IF p_Opc = 'L'
    THEN
		SELECT IdImages, IdLevel, Image
			FROM images
		WHERE IdLevel = p_IdLevel;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_levels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_levels`(
	IN p_Opc						CHAR(3),
    IN p_IdLevel					BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_Title						VARCHAR(400),
    IN p_Price						DECIMAL(15,2),
    IN p_TextLevel					VARCHAR(500),
	IN p_TextLinks					VARCHAR(800),
    IN p_UrlVideo					VARCHAR(500),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdLevel, IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo, Active
			FROM levels;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdLevel, IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo, Active
			FROM levels
            WHERE IdLevel = p_IdLevel;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO levels(IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo)
					VALUES(p_IdCourse, p_Title, p_Price, p_TextLevel, p_TextLinks, p_UrlVideo);
    END IF;
    
    IF p_Opc = 'IU'
    THEN
		INSERT INTO levels(IdLevel, IdCourse, Title, Price, TextLevel, TextLinks, UrlVideo)
				   VALUES(p_IdLevel, p_IdCourse, p_Title, p_Price, p_TextLevel, p_TextLinks, p_UrlVideo)
                   ON DUPLICATE KEY UPDATE 
						Title = p_Title,
						Price = p_Price,
						TextLevel = p_TextLevel,
						TextLinks = p_TextLinks,
						UrlVideo = p_UrlVideo;
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE levels
			SET Title = p_Title,
				Price = p_Price,
                TextLevel = p_TextLevel,
                TextLinks = p_TextLinks,
                UrlVideo = p_UrlVideo
			WHERE IdLevel = p_IdLevel;
    END IF;
    
    -- Se usa para traerse el nivel mas barato
    IF p_Opc = 'CH'
    THEN
		SELECT IdLevel, Title, Price 
			FROM levels
		WHERE IdCourse = p_IdCourse
        ORDER BY Price ASC
		LIMIT 1;
        
    END IF;
    
    -- Se usa para traerse los niveles de cada curso
    IF p_Opc = 'L'
    THEN
		SELECT IdLevel, title, price, textlevel
			FROM levels
		WHERE IdCourse = p_IdCourse;
    END IF;
    
     -- Se usa para traerse los niveles de cada curso
    IF p_Opc = 'EL'
    THEN
		SELECT IdLevel, title, price, TextLevel, TextLinks
			FROM levels
		WHERE IdCourse = p_IdCourse;
    END IF;
    
    -- se usa para traerse un nivel recien registrado
    IF p_Opc = 'LC'
    THEN
		SELECT IdLevel
			FROM levels
		WHERE IdCourse = p_IdCourse
        ORDER BY IdLevel DESC LIMIT 1;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_links` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_links`(
	IN p_Opc						CHAR(3),
    IN p_IdLinks					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_Link						VARCHAR(300),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdLinks, IdLevel, Link, Active
			FROM Links;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdLinks, IdLevel, Link, Active
			FROM Links
            WHERE IdLinks = p_IdLinks;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO Links(IdLevel, Link)
					VALUES(p_IdLevel, p_Link);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE Links
			SET Link = p_Link
			WHERE IdLinks = p_IdLinks;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_messages` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_messages`(
	IN p_Opc						CHAR(3),
    IN p_IdMessages					BIGINT,
    IN p_IdUserSender				BIGINT,
    IN p_IdUserAddressee			BIGINT,
    IN p_TextMessage				TEXT,
    IN p_MessageDate				DATETIME,
    IN p_Active						BOOL,
    IN p_searchStr					CHAR(30)
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdMessages, IdUserSender, IdUserAddressee, TextMessage, MessageDate, Active
			FROM messages;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdMessages, IdUserSender, IdUserAddressee, TextMessage, MessageDate, Active
			FROM messages
            WHERE IdMessages = p_IdMessages;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO messages(IdUserSender, IdUserAddressee, TextMessage)
		VALUES (p_IdUserSender, p_IdUserAddressee, p_TextMessage);
                    
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE messages
			SET TextMessage = p_TextMessage,
				MessageDate = p_MessageDate
			WHERE IdMessages = p_IdMessages;
    END IF;
    
    IF p_Opc = 'B'
    THEN

SELECT U.IdUser as SenderID, U.Names as SenderName, U.FirstSurname as SenderFS, U.SecondSurname as SenderSS, U.Avatar as SenderAvatar, U.Active as SenderActive,
		Us.IdUser as AddID, Us.Names as addName, Us.FirstSurname as addFS, Us.SecondSurname as addSS, Us.Avatar as addAvatar, U.Active as addActive,
		M.IdMessages, M.IdUserSender, M.IdUserAddressee, M.TextMessage, M.MessageDate, M.Active as ActiveMss
	FROM users as U
    JOIN users as Us
    LEFT JOIN messages as M
    ON U.IdUser = M.IdUserSender
    AND Us.IdUser = M.IdUserAddressee
    WHERE U.IdUser = p_IdUserSender
	AND Us.IdUser = p_IdUserAddressee 
	AND  M.TextMessage IS NOT NULL
    OR   U.IdUser = p_IdUserAddressee 
	AND Us.IdUser = p_IdUserSender 
    AND  M.TextMessage IS NOT NULL
    ORDER BY M.IdMessages;
    
    END IF;
    
    	IF p_Opc = 'L'
    THEN
	SELECT  U.IdUser as InboxUserID, U.Names as InboxUserName, U.FirstSurname as InboxUserFName, U.SecondSurname as InboxUserSName, U.Avatar as InboxUserAvatar, U.Active as InboxUserAct
	FROM messages as M
	JOIN users as U
    ON (U.IdUser = M.IdUserSender AND M.IdUserSender != p_IdUserSender) OR (U.IdUser = M.IdUserAddressee AND M.IdUserAddressee != p_IdUserSender)
    WHERE (M.IdUserSender = p_IdUserSender) OR (M.IdUserAddressee = p_IdUserSender)
    GROUP BY U.IdUser
    ORDER BY M.MessageDate DESC;
    END IF;
    
        IF p_Opc = 'BI'
    THEN

SELECT Us.IdUser as InboxUserID, Us.Names as InboxUserName, Us.FirstSurname as InboxUserFName, Us.SecondSurname as InboxUserSName, 
		Us.Avatar as InboxUserAvatar, Us.Active as InboxUserAct
	FROM users as Us
    WHERE Us.IdUser = p_IdUserAddressee;    
    END IF;
    
            IF p_Opc = 'S'
    THEN
		SELECT * FROM learndo.v_usersandschools 
        WHERE (Names LIKE CONCAT('%', p_searchStr, '%') 
        OR FirstSurname LIKE CONCAT('%', p_searchStr, '%') 
        OR SecondSurname LIKE CONCAT('%', p_searchStr, '%') 
        OR SchoolName LIKE CONCAT('%', p_searchStr, '%'))
		AND IdUser!= p_IdUserSender;
    
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_pdfFiles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_pdfFiles`(
	IN p_Opc						CHAR(3),
    IN p_IdPDFFile					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_NameFile					VARCHAR(500),
    IN p_IdUrlFile					VARCHAR(500),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdPDFFile, IdLevel, NameFile, UrlFile, Active
			FROM pdffiles;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdPDFFile, IdLevel, NameFile, UrlFile, Active
			FROM pdffiles
            WHERE IdPDFFile = p_IdPDFFile;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO pdffiles(IdLevel, NameFile, UrlFile)
					VALUES(p_IdLevel, p_NameFile, p_IdUrlFile);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE pdffiles
			SET NameFile = p_NameFile,
				UrlFile = p_UrlFile
			WHERE IdPDFFile = p_IdPDFFile;
    END IF;
	
    IF p_Opc = 'L'
    THEN
		SELECT IdPDFFile, IdLevel, NameFile, UrlFile
			FROM pdffiles
		WHERE IdLevel = p_IdLevel;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Schools` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_Schools`(
	IN p_Opc						CHAR(3),
	IN p_IdSchool					BIGINT,
    IN p_IdUser						BIGINT,
    IN p_Email				 		VARCHAR(100),
	IN p_Password		 			VARCHAR(30),
	IN p_Avatar						MEDIUMBLOB,
	IN p_Name		 				VARCHAR(30),
	IN p_Signupdate	 				DATETIME,
	IN p_LastUpdateDate 			DATETIME,
	IN p_Active						BOOL
)
BEGIN
	DECLARE image MEDIUMBLOB;
    
	IF p_Opc = 'IU'
	THEN
    
		SELECT @image:= Avatar FROM users WHERE IdUser = p_IdUser;
        
		INSERT INTO schools(IdSchool, IdUser, Avatar, Name)
				   VALUES(p_IdSchool, p_IdUser, IFNULL(p_Avatar, @image), p_Name)
                   ON DUPLICATE KEY UPDATE 
						Avatar = IFNULL(p_Avatar, Avatar),
                        Name = p_Name,
                        LastUpdateDate = NOW();
	END IF;
		
	IF p_Opc = 'ID'
	THEN
		SELECT IdSchool, IdUser, Avatar, Name, Signupdate, LastUpdateDate, Active
			FROM schools
		WHERE IdUser = p_IdUser;
	END IF;
	
	IF p_Opc = 'D'
	THEN
		DELETE
			FROM schools
		WHERE IdSchool = p_IdSchool;
	END IF;
    
	IF p_Opc = 'L'
    THEN
		SELECT IdUser, IdSchool
			FROM v_usersandschools
		WHERE Email = p_Email AND
        Password = p_Password;
	END IF;
    
	IF p_Opc = 'S'
    THEN
		SELECT IdSchool, IdUser, Avatar, Name, Signupdate, LastUpdateDate, Active
			FROM schools
		WHERE IdSchool = p_IdSchool;
    END IF;

	IF p_Opc = 'X'
	THEN
		SELECT IdSchool, IdUser, Avatar, Name, Signupdate, LastUpdateDate, Active
			FROM schools;
	END IF;
    
    -- Se usa para traerse las ventas de esa escuela
    IF p_Opc = 'SS'
    THEN
		SELECT SumSchoolTotalEarnings(p_IdSchool) as TotalSales, SumSchoolPaypalEarnings(p_IdSchool) as PaypalSales, SumSchoolCardEarnings(p_IdSchool) as CardSales;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_Users`(
	IN p_Opc						CHAR(3),
	IN p_IdUser 					BIGINT,
	IN p_Names 						VARCHAR(100),
	IN p_FirstSurname 				VARCHAR(30),
	IN p_SecondSurname 	 			VARCHAR(30),
	IN p_Avatar					 	MEDIUMBLOB,
	IN p_Gender						TINYINT,
	IN p_Email				 		VARCHAR(100),
	IN p_Password		 			VARCHAR(30),
	IN p_Birthdate					DATE,
	IN p_Signupdate	 				DATETIME,
	IN p_LastUpdateDate 			DATETIME,
	IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'I'
	THEN 
		INSERT INTO users(Names, FirstSurname, SecondSurname, Avatar, Gender, Email, Password, Birthdate)
				   VALUES(p_Names, p_FirstSurname, p_SecondSurname, p_Avatar, p_Gender, p_Email, p_Password, p_Birthdate);
	END IF;
	IF p_Opc = 'U'
	THEN
		UPDATE users
			SET Names = p_Names,
				firstSurname = p_FirstSurname,				
                secondSurname = p_SecondSurname,
				Avatar = -- IF(p_Avatar IS NULL, Avatar, p_Avatar),
				IFNULL(p_Avatar, Avatar),
                Gender = p_Gender,
				Email = p_Email,
                Password = p_Password,
				Birthdate = p_Birthdate,
                LastUpdateDate = NOW()
			WHERE IdUser = p_IdUser;
            
	END IF;

	IF p_Opc = 'D'
	THEN
		DELETE
			FROM users
		WHERE IdUser = p_IdUser;
	END IF;

	IF p_Opc = 'X'
	THEN
		SELECT IdUser, Names, FirstSurname, SecondSurname, Avatar, Gender, Email, Password, Birthdate, Signupdate, LastUpdateDate, Active
			FROM users;
	END IF;
    
    IF p_Opc = 'L'
    THEN
		SELECT IdUser
			FROM users
		WHERE Email = p_Email AND
        Password = p_Password;
	END IF;
    
	IF p_Opc = 'ID'
    THEN
		SELECT IdUser, Names, FirstSurname, SecondSurname, Avatar, Gender, Email, Password, Birthdate, Signupdate, LastUpdateDate, Active
			FROM users
		WHERE IdUser = p_IdUser;
	END IF;
    
    IF p_Opc = 'US'
    THEN
		SELECT S.IdUser, S.IdSchool, S.Avatar, S.Name
			FROM users AS U
            INNER JOIN schools AS S
            ON U.IdUser = S.IdUser
		WHERE S.IdUser = p_IdUser;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_usersbuycourses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_usersbuycourses`(
	IN p_Opc						CHAR(3),
    IN p_IdPurchase					BIGINT,
    IN p_IdUser						BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_PaymentMethod				TINYINT,
    IN p_PurchasePrice				DECIMAL(15,2),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdPurchase, IdUser, IdCourse, PaymentMethod, PurchasePrice, Active
			FROM usersbuycourses;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdPurchase, IdUser, IdCourse, PaymentMethod, PurchasePrice, Active
			FROM usersbuycourses
            WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO usersbuycourses(IdUser, IdCourse, PaymentMethod, PurchasePrice)
					VALUES(p_IdUser, p_IdCourse, p_PaymentMethod, p_PurchasePrice);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE usersbuycourses
			SET PaymentMethod = p_PaymentMethod,
				PurchasePrice = p_PurchasePrice
			WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    IF p_Opc = 'H'
    THEN
		SELECT EXISTS(SELECT 1 FROM usersbuycourses
		WHERE IdUser = p_IdUser
        AND IdCourse = p_IdCourse) AS HasTheCompleteCourse;
			
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_usersbuylevels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_usersbuylevels`(
	IN p_Opc						CHAR(3),
    IN p_IdPurchase					BIGINT,
    IN p_IdUser						BIGINT,
	IN p_IdCourse					BIGINT,
    IN p_IdLevel					BIGINT,
    IN p_PurchasePrice				DECIMAL(15,2),
    IN p_PaymentMethod				TINYINT,
    IN p_LastVisitDate				DATETIME,
    IN p_Progress					TINYINT(1),
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdPurchase, IdUser, IdLevel, PurchasePrice, LastVisitDate, PaymentMethod, Progress, Active
			FROM usersbuylevels;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdPurchase, IdUser, IdLevel, PurchasePrice, LastVisitDate, PaymentMethod, Progress, Active
			FROM usersbuylevels
            WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO usersbuylevels(IdUser, IdLevel, PurchasePrice, PaymentMethod)
					VALUES(p_IdUser, p_IdLevel, p_PurchasePrice, p_PaymentMethod);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE usersbuylevels
			SET LastVisitDate = p_LastVisitDate,
				PurchasePrice = p_PurchasePrice,
                PaymentMethod = p_PaymentMethod,
				Progress = p_Progress
			WHERE IdPurchase = p_IdPurchase;
    END IF;
    
    -- Se usa para el updatear el progreso
    IF p_Opc = 'P'
    THEN
		UPDATE usersbuylevels
			SET Progress = p_Progress
		WHERE IdUser = p_IdUser AND IdLevel = p_IdLevel;
    END IF;
    
    -- Se usa para traerse los niveles comprados de un usuario
    IF p_Opc = 'L'
    THEN
		SELECT UBL.IdLevel, L.IdCourse, L.Title
			 FROM usersbuylevels AS UBL
             INNER JOIN levels AS L
             ON L.IdLevel = UBL.IdLevel
             INNER JOIN courses AS C
             ON C.IdCourse = L.IdCourse
		WHERE C.IdCourse = p_IdCourse
        AND UBL.IdUser = p_IdUser;
    END IF;
    
	-- Se usa para traerse el nivel a visualizar de un usuario
    IF p_Opc = 'V'
    THEN
		SELECT L.IdLevel, L.Title, L.TextLevel, L.TextLinks, L.UrlVideo, UBL.LastVisitDate, UBL.Progress
			 FROM usersbuylevels AS UBL
             INNER JOIN levels AS L
             ON L.IdLevel = UBL.IdLevel
		WHERE UBL.IdLevel = p_IdLevel
        AND UBL.IdUser = p_IdUser;
    END IF;
    
	-- Se usa para actualizar la ultima vez que se visito ese nivel
    IF p_Opc = 'LVD'
    THEN
		UPDATE usersbuylevels
			SET LastVisitDate = CURRENT_TIMESTAMP()
		WHERE IdUser = p_IdUser AND IdLevel = p_IdLevel;
    END IF;

 -- Usado para mostrar los niveles que no tiene al usuario a la hora de querer comprar
    IF p_Opc = 'LB'
    THEN
		SELECT  L.IdLevel, L.title, L.price, L.textlevel
				FROM levels AS L
		WHERE IdCourse = p_IdCourse 
        AND NOT EXISTS (SELECT 1 FROM usersbuylevels as UBL WHERE UBL.IdLevel = L.IdLevel AND UBL.IdUser = p_IdUser);
        
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_usersregistercourses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_usersregistercourses`(
	IN p_Opc						CHAR(3),
    IN p_IdRegistration				BIGINT,
    IN p_IdUser						BIGINT,
    IN p_IdCourse					BIGINT,
    IN p_RegistrationDate			DATETIME,
    IN p_FinishDate					DATETIME,
    IN p_Active						BOOL
)
BEGIN
	IF p_Opc = 'X'
    THEN
		SELECT IdRegistration, IdUser, IdCourse, RegistrationDate, FinishDate, Active
			FROM usersregistercourses;
    END IF;
    
    IF p_Opc = 'ID'
    THEN
		SELECT IdRegistration, IdUser, IdCourse, RegistrationDate, FinishDate, Active
			FROM usersregistercourses
            WHERE IdRegistration = p_IdRegistration;
    END IF;
    
    IF p_Opc = 'I' 
    THEN
		INSERT INTO usersregistercourses(IdUser, IdCourse)
					VALUES(p_IdUser, p_IdCourse);
    END IF;
    
    IF p_Opc = 'U'
    THEN
		UPDATE usersregistercourses
			SET RegistrationDate = p_RegistrationDate,
				FinishDate = p_FinishDate
			WHERE IdRegistration = p_IdRegistration;
    END IF;
    
    -- Esto se usa para traerse los cursos registrados de un usuario y su progreso NOTE: CALAR
    IF p_Opc = 'UC'
    THEN
		SELECT URC.IdCourse, C.Title, C.Image, S.Name as SchoolName, StudentProgressPercentage(URC.IdCourse, URC.IdUser) as Progress, URC.RegistrationDate, URC.FinishDate
			FROM usersregistercourses AS URC
            INNER JOIN courses AS C
            ON URC.IdCourse = C.IdCourse
			INNER JOIN schools AS S
            ON C.IdOwner = S.IdSchool
		WHERE URC.IdUser = p_IdUser;
        
    END IF;
    
    -- Validacion para ver si un usuario esta registrado a un curso
    IF p_Opc = 'R'
    THEN
		SELECT IsUserRegisteredInTheCourse(p_IdCourse, p_IdUser) as IsRegistered;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_coursesnumberoflevels`
--

/*!50001 DROP TABLE IF EXISTS `v_coursesnumberoflevels`*/;
/*!50001 DROP VIEW IF EXISTS `v_coursesnumberoflevels`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_coursesnumberoflevels` AS select `c`.`IdCourse` AS `IdCourse`,`c`.`Title` AS `Title`,count(`l`.`IdLevel`) AS `NumberOfLevels` from (`courses` `c` join `levels` `l` on(`l`.`IdCourse` = `c`.`IdCourse`)) group by `c`.`IdCourse` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_coursesstudentsandsales`
--

/*!50001 DROP TABLE IF EXISTS `v_coursesstudentsandsales`*/;
/*!50001 DROP VIEW IF EXISTS `v_coursesstudentsandsales`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_coursesstudentsandsales` AS select `c`.`IdCourse` AS `IdCourse`,`c`.`IdOwner` AS `IdOwner`,`c`.`Title` AS `Title`,`c`.`Image` AS `Image`,count(distinct `urc`.`IdUser`) AS `Students`,`SumCourseEarnings`(`c`.`IdCourse`) AS `TotalSales`,`c`.`Active` AS `Active` from (((`courses` `c` left join `usersregistercourses` `urc` on(`c`.`IdCourse` = `urc`.`IdCourse`)) join `levels` `l` on(`c`.`IdCourse` = `l`.`IdCourse`)) left join `usersbuylevels` `ubl` on(`l`.`IdLevel` = `ubl`.`IdLevel`)) group by `c`.`IdCourse` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_schoolcoursesstudentsrating`
--

/*!50001 DROP TABLE IF EXISTS `v_schoolcoursesstudentsrating`*/;
/*!50001 DROP VIEW IF EXISTS `v_schoolcoursesstudentsrating`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_schoolcoursesstudentsrating` AS select `s`.`IdSchool` AS `IdSchool`,`s`.`Name` AS `Name`,`c`.`IdCourse` AS `IdCourse`,`c`.`Title` AS `Title`,`c`.`Image` AS `Image`,`c`.`Description` AS `Description`,`c`.`TotalPrice` AS `TotalPrice`,`c`.`CreationDate` AS `CreationDate`,count(distinct `urc`.`IdUser`) AS `Students`,format(avg(`cm`.`Rating`),1) AS `Rating`,`c`.`Active` AS `Active` from (((`courses` `c` join `schools` `s` on(`c`.`IdOwner` = `s`.`IdSchool`)) left join `usersregistercourses` `urc` on(`c`.`IdCourse` = `urc`.`IdCourse`)) left join `commentaries` `cm` on(`c`.`IdCourse` = `cm`.`IdCourse`)) where `cm`.`Active` = 1 group by `c`.`IdCourse`,`urc`.`IdCourse`,`cm`.`IdCourse` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_studentdetailedboughtcourse`
--

/*!50001 DROP TABLE IF EXISTS `v_studentdetailedboughtcourse`*/;
/*!50001 DROP VIEW IF EXISTS `v_studentdetailedboughtcourse`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_studentdetailedboughtcourse` AS select `ubc`.`IdCourse` AS `IdCourse`,`c`.`IdOwner` AS `IdOwner`,`u`.`IdUser` AS `IdUser`,`u`.`Avatar` AS `Avatar`,`u`.`Names` AS `Names`,`u`.`FirstSurname` AS `FirstSurname`,`u`.`SecondSurname` AS `SecondSurname`,`StudentProgressPercentage`(`ubc`.`IdCourse`,`u`.`IdUser`) AS `Progress`,`ubc`.`PurchasePrice` AS `PurchasePrice`,`ubc`.`PaymentMethod` AS `PaymentMethod`,`urc`.`RegistrationDate` AS `RegistrationDate` from (((`usersbuycourses` `ubc` join `users` `u` on(`ubc`.`IdUser` = `u`.`IdUser`)) join `usersregistercourses` `urc` on(`urc`.`IdUser` = `u`.`IdUser` and `urc`.`IdCourse` = `ubc`.`IdCourse`)) join `courses` `c` on(`ubc`.`IdCourse` = `c`.`IdCourse`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_studentdetailedboughtlevels`
--

/*!50001 DROP TABLE IF EXISTS `v_studentdetailedboughtlevels`*/;
/*!50001 DROP VIEW IF EXISTS `v_studentdetailedboughtlevels`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_studentdetailedboughtlevels` AS select `c`.`IdCourse` AS `IdCourse`,`c`.`IdOwner` AS `IdOwner`,`l`.`Title` AS `Title`,`u`.`IdUser` AS `IdUser`,`u`.`Avatar` AS `Avatar`,`u`.`Names` AS `Names`,`u`.`FirstSurname` AS `FirstSurname`,`u`.`SecondSurname` AS `SecondSurname`,`ubl`.`Progress` AS `Progress`,`ubl`.`PurchasePrice` AS `PurchasePrice`,`ubl`.`PaymentMethod` AS `PaymentMethod`,`urc`.`RegistrationDate` AS `RegistrationDate` from ((((`usersbuylevels` `ubl` join `users` `u` on(`ubl`.`IdUser` = `u`.`IdUser`)) join `levels` `l` on(`ubl`.`IdLevel` = `l`.`IdLevel`)) join `courses` `c` on(`l`.`IdCourse` = `c`.`IdCourse`)) join `usersregistercourses` `urc` on(`c`.`IdCourse` = `urc`.`IdCourse` and `u`.`IdUser` = `urc`.`IdUser`)) group by `ubl`.`IdUser`,`ubl`.`IdLevel` order by `ubl`.`IdUser` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_studentregistereddetailed`
--

/*!50001 DROP TABLE IF EXISTS `v_studentregistereddetailed`*/;
/*!50001 DROP VIEW IF EXISTS `v_studentregistereddetailed`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_studentregistereddetailed` AS select `urc`.`IdCourse` AS `IdCourse`,`c`.`IdOwner` AS `IdOwner`,`u`.`IdUser` AS `IdUser`,`u`.`Avatar` AS `Avatar`,`u`.`Names` AS `Names`,`u`.`FirstSurname` AS `FirstSurname`,`u`.`SecondSurname` AS `SecondSurname`,`StudentProgressPercentage`(`urc`.`IdCourse`,`u`.`IdUser`) AS `Progress`,`SumUserTotalPayments`(`u`.`IdUser`,`urc`.`IdCourse`) AS `TotalPayments`,`urc`.`RegistrationDate` AS `RegistrationDate` from ((`usersregistercourses` `urc` join `users` `u` on(`urc`.`IdUser` = `u`.`IdUser`)) join `courses` `c` on(`c`.`IdCourse` = `urc`.`IdCourse`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_usersandschools`
--

/*!50001 DROP TABLE IF EXISTS `v_usersandschools`*/;
/*!50001 DROP VIEW IF EXISTS `v_usersandschools`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `v_usersandschools` AS select `u`.`IdUser` AS `IdUser`,`u`.`Names` AS `Names`,`u`.`FirstSurname` AS `FirstSurname`,`u`.`SecondSurname` AS `SecondSurname`,`u`.`Avatar` AS `Avatar`,`u`.`Gender` AS `Gender`,`u`.`Email` AS `Email`,`u`.`Password` AS `Password`,`u`.`Birthdate` AS `Birthdate`,`u`.`Signupdate` AS `Signupdate`,`u`.`LastUpdateDate` AS `LastUpdateDate`,`u`.`Active` AS `Active`,`s`.`IdSchool` AS `IdSchool`,`s`.`Avatar` AS `SchoolAvatar`,`s`.`Name` AS `SchoolName`,`s`.`Signupdate` AS `SchoolSignupdate`,`s`.`LastUpdateDate` AS `SchoolLastUpdateDate` from (`users` `u` left join `schools` `s` on(`u`.`IdUser` = `s`.`IdUser`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-17 20:18:40
