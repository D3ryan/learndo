CREATE DATABASE LearnDo;
USE LearnDo;

DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users` (
    `IdUser` BIGINT NOT NULL AUTO_INCREMENT,
    `Names` VARCHAR(100) NOT NULL,
    `FirstSurname` VARCHAR(30) NOT NULL,
    `SecondSurname` VARCHAR(30) NOT NULL,
    `Avatar` MEDIUMBLOB NOT NULL,
    `Gender` TINYINT NOT NULL,
    `Email` VARCHAR(100) NOT NULL,
    `Password` VARCHAR(30) NOT NULL,
    `Birthdate` DATE NOT NULL,
    `Signupdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `LastUpdateDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `Active` BOOL NOT NULL DEFAULT 1,
    PRIMARY KEY (`IdUser`),
    UNIQUE KEY `IdUser_UNIQUE` (`IdUser`),
    UNIQUE KEY `Email_UNIQUE` (`Email`)
)  ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Schools`;
CREATE TABLE `Schools` (
  `IdSchool` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUser` BIGINT NOT NULL,
  `Avatar` MEDIUMBLOB NOT NULL,
  `Name` VARCHAR(100) NOT NULL,
  `Signupdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdateDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdSchool`),
  UNIQUE KEY `IdSchool_UNIQUE` (`IdSchool`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Courses`;
CREATE TABLE `Courses` (
  `IdCourse` BIGINT NOT NULL AUTO_INCREMENT,
  `IdOwner` BIGINT NOT NULL,
  `Title` VARCHAR(400) NOT NULL,
  `Image` MEDIUMBLOB NOT NULL,
  `Description` VARCHAR(400) NOT NULL,
  `TotalPrice` DECIMAL(15,2) NOT NULL,
  `CreationDate` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCourse`),
  UNIQUE KEY `IdCourses_UNIQUE` (`IdCourse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Levels`;
CREATE TABLE `Levels` (
  `IdLevel` BIGINT NOT NULL AUTO_INCREMENT,
  `IdCourse` BIGINT NOT NULL,
  `Title` VARCHAR(400) NOT NULL,
  `Price` DECIMAL(15,2) NOT NULL,
  `TextLevel` VARCHAR(500) NOT NULL,
  `TextLinks` VARCHAR(800) NOT NULL,
  `UrlVideo` VARCHAR(500) NOT NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdLevel`),
  UNIQUE KEY `IdLevel_UNIQUE` (`IdLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Messages`;
CREATE TABLE `Messages` (
  `IdMessages` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUserSender` BIGINT NOT NULL,
  `IdUserAddressee` BIGINT NOT NULL,
  `TextMessage` TEXT NOT NULL,
  `MessageDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdMessages`),
  UNIQUE KEY `IdMessages_UNIQUE` (`IdMessages`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Commentaries`;
CREATE TABLE `Commentaries` (
  `IdCommentary` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUser` BIGINT NOT NULL,
  `IdCourse` BIGINT NOT NULL,
  `TextCommentary` TEXT NOT NULL,
  `Rating` SMALLINT NOT NULL,
  `DateCommentary` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCommentary`),
  UNIQUE KEY `IdCommentary_UNIQUE` (`IdCommentary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Categories`;
CREATE TABLE `Categories` (
  `IdCategory` BIGINT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(30) NOT NULL,
  `Description` VARCHAR(100) NOT NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCategory`),
  UNIQUE KEY `IdCategory_UNIQUE` (`IdCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- Checar con api de paypal
DROP TABLE IF EXISTS `FormasDePago`;
CREATE TABLE `FormasDePago` (
  `IdFormaDePago` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(20) NOT NULL,
  `Activo` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdFormaDePago`),
  UNIQUE KEY `IdFormaDePago_UNIQUE` (`IdFormaDePago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `PDFFiles`;
CREATE TABLE `PDFFiles` (
  `IdPDFFile` BIGINT NOT NULL AUTO_INCREMENT,
  `IdLevel` BIGINT NOT NULL,
  `NameFile` VARCHAR(500),
  `UrlFile` VARCHAR(500) NOT NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdPDFFile`),
  UNIQUE KEY `IdPDFFiles_UNIQUE` (`IdPDFFile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ESTO YA NO SE VA A USAR
DROP TABLE IF EXISTS `Links`;
CREATE TABLE `Links` (
  `IdLinks` BIGINT NOT NULL AUTO_INCREMENT,
  `IdLevel` BIGINT NOT NULL,
  `Link` VARCHAR(300) NOT NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdLinks`),
  UNIQUE KEY `IdLinks_UNIQUE` (`IdLinks`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `Images`;
CREATE TABLE `Images` (
  `IdImages` BIGINT NOT NULL AUTO_INCREMENT,
  `IdLevel` BIGINT NOT NULL,
  `Image` MEDIUMBLOB NOT NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdImages`),
  UNIQUE KEY `IdImages_UNIQUE` (`IdImages`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `CoursesCategories`;
CREATE TABLE `CoursesCategories` (
  `IdCoursesCategory` BIGINT NOT NULL AUTO_INCREMENT,
  `IdCourse` BIGINT NOT NULL,
  `IdCategory` BIGINT NOT NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdCoursesCategory`),
  UNIQUE KEY `IdCoursesCategory_UNIQUE` (`IdCoursesCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `UsersBuyLevels`;
CREATE TABLE `UsersBuyLevels` (
  `IdPurchase` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUser` BIGINT NOT NULL,
  `IdLevel` BIGINT NOT NULL,
  `PaymentMethod` TINYINT,
  `PurchasePrice` DECIMAL(15,2),
  `LastVisitDate` DATETIME NULL,
  `Progress` BOOL NOT NULL DEFAULT 0,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdPurchase`),
  UNIQUE KEY `IdPurchase_UNIQUE` (`IdPurchase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `UsersRegisterCourses`;
CREATE TABLE `UsersRegisterCourses` (
  `IdRegistration` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUser` BIGINT NOT NULL,
  `IdCourse` BIGINT NOT NULL,
  `RegistrationDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `FinishDate` DATETIME NULL,
  `Active` BOOL NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdRegistration`),
  UNIQUE KEY `IdRegistration_UNIQUE` (`IdRegistration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `learndo`.`usersbuycourses` (
  `IdPurchase` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUser` BIGINT NOT NULL,
  `IdCourse` BIGINT NOT NULL,
  `PaymentMethod` TINYINT NOT NULL,
  `PurchasePrice` DECIMAL(15,2) NOT NULL,
  `Active` bool NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdPurchase`),
  UNIQUE KEY `IdPurchase_UNIQUE` (`IdPurchase`)
  )ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `learndo`.`schools` 
ADD INDEX `FK_SCHOOL_USER_idx` (`IdUser` ASC);

ALTER TABLE `learndo`.`schools` 
ADD CONSTRAINT `FK_SCHOOL_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`courses` 
ADD INDEX `FK_COURSES_SCHOOL_idx` (`IdOwner` ASC);

ALTER TABLE `learndo`.`courses` 
ADD CONSTRAINT `FK_COURSES_SCHOOL`
  FOREIGN KEY (`IdOwner`)
  REFERENCES `learndo`.`schools` (`IdSchool`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`levels` 
ADD INDEX `FK_LEVEL_COURSES_idx` (`IdCourse` ASC);

ALTER TABLE `learndo`.`levels` 
ADD CONSTRAINT `FK_LEVEL_COURSES`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
  
ALTER TABLE `learndo`.`messages` 
ADD INDEX `FK_MESSAGES_SENDER_idx` (`IdUserSender` ASC),
ADD INDEX `FK_MESSAGES_ADDRESSEE_idx` (`IdUserAddressee` ASC);

ALTER TABLE `learndo`.`messages` 
ADD CONSTRAINT `FK_MESSAGES_SENDER`
  FOREIGN KEY (`IdUserSender`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`messages` 
ADD CONSTRAINT `FK_MESSAGES_ADDRESSEE`
  FOREIGN KEY (`IdUserAddressee`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`commentaries` 
ADD INDEX `FK_COMMENTARY_USER_idx` (`IdUser` ASC),
ADD INDEX `FK_COMMENTARY_COURSE_idx` (`IdCourse` ASC);

ALTER TABLE `learndo`.`commentaries` 
ADD CONSTRAINT `FK_COMMENTARY_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`commentaries` 
ADD CONSTRAINT `FK_COMMENTARY_COURSE`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`pdffiles` 
ADD INDEX `FK_FILEPDF_LEVEL_idx` (`IdLevel` ASC);

ALTER TABLE `learndo`.`pdffiles` 
ADD CONSTRAINT `FK_FILEPDF_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`links` 
ADD INDEX `FK_LINK_LEVEL_idx` (`IdLevel` ASC);

ALTER TABLE `learndo`.`links` 
ADD CONSTRAINT `FK_LINK_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`images` 
ADD INDEX `FK_IMAGES_LEVEL_idx` (`IdLevel` ASC);

ALTER TABLE `learndo`.`images` 
ADD CONSTRAINT `FK_IMAGES_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`coursescategories` 
ADD INDEX `FK_COURSECAT_COURSE_idx` (`IdCourse` ASC),
ADD INDEX `FK_COURSECAT_CATEGORY_idx` (`IdCategory` ASC);

ALTER TABLE `learndo`.`coursescategories` 
ADD CONSTRAINT `FK_COURSECAT_COURSE`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`coursescategories` 
ADD CONSTRAINT `FK_COURSECAT_CATEGORY`
  FOREIGN KEY (`IdCategory`)
  REFERENCES `learndo`.`categories` (`IdCategory`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`usersbuylevels` 
ADD INDEX `FK_PURCHASE_USER_idx` (`IdUser` ASC),
ADD INDEX `FK_PURCHASE_LEVEL_idx` (`IdLevel` ASC);

ALTER TABLE `learndo`.`usersbuylevels` 
ADD CONSTRAINT `FK_PURCHASE_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`usersbuylevels` 
ADD CONSTRAINT `FK_PURCHASE_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  

ALTER TABLE `learndo`.`usersregistercourses` 
ADD INDEX `FK_REGISTER_USER_idx` (`IdUser` ASC),
ADD INDEX `FK_REGISTER_COURSES_idx` (`IdCourse` ASC);

ALTER TABLE `learndo`.`usersregistercourses` 
ADD CONSTRAINT `FK_REGISTER_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`usersregistercourses` 
ADD CONSTRAINT `FK_REGISTER_COURSES`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
ALTER TABLE `learndo`.`usersbuycourses`
 ADD INDEX `IdUser_idx` (`IdUser` ASC),
 ADD INDEX `IdCourse_idx` (`IdCourse` ASC);
 
ALTER TABLE `learndo`.`usersbuycourses` 
ADD CONSTRAINT `IdUser`
    FOREIGN KEY (`IdUser`)
    REFERENCES `learndo`.`users` (`IdUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
ADD CONSTRAINT `IdCourse`
    FOREIGN KEY (`IdCourse`)
    REFERENCES `learndo`.`courses` (`IdCourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

-- ALTER TABLES PARA AGREGAR COMENTARIOS A LAS COLUMNAS
ALTER TABLE `learndo`.`categories` 
CHANGE COLUMN `IdCategory` `IdCategory` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de categorías' ,
CHANGE COLUMN `Name` `Name` VARCHAR(30) NOT NULL COMMENT 'Nombre de la categoría' ,
CHANGE COLUMN `Description` `Description` VARCHAR(100) NOT NULL COMMENT 'Descripción de la categoría' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si la categoría está activa o no' ;

ALTER TABLE `learndo`.`commentaries` 
DROP FOREIGN KEY `FK_COMMENTARY_COURSE`,
DROP FOREIGN KEY `FK_COMMENTARY_USER`;
ALTER TABLE `learndo`.`commentaries` 
CHANGE COLUMN `IdCommentary` `IdCommentary` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de comentarios' ,
CHANGE COLUMN `IdUser` `IdUser` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla usuario' ,
CHANGE COLUMN `IdCourse` `IdCourse` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla curso' ,
CHANGE COLUMN `TextCommentary` `TextCommentary` TEXT NOT NULL COMMENT 'Texto del comentario' ,
CHANGE COLUMN `Rating` `Rating` SMALLINT NOT NULL COMMENT 'La valuación del comentario' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`commentaries` 
ADD CONSTRAINT `FK_COMMENTARY_COURSE`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
ADD CONSTRAINT `FK_COMMENTARY_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`courses` 
DROP FOREIGN KEY `FK_COURSES_SCHOOL`;
ALTER TABLE `learndo`.`courses` 
CHANGE COLUMN `IdCourse` `IdCourse` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de cursos' ,
CHANGE COLUMN `IdOwner` `IdOwner` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla escuelas' ,
CHANGE COLUMN `Title` `Title` VARCHAR(400) NOT NULL COMMENT 'Título del curso' ,
CHANGE COLUMN `Image` `Image` MEDIUMBLOB NOT NULL COMMENT 'Imagen el curso' ,
CHANGE COLUMN `Description` `Description` VARCHAR(400) NOT NULL COMMENT 'Texto que describe de lo que se tratará el curso' ,
CHANGE COLUMN `TotalPrice` `TotalPrice` DECIMAL(15,2) NOT NULL COMMENT 'Precio a pagar por el curso' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`courses` 
ADD CONSTRAINT `FK_COURSES_SCHOOL`
  FOREIGN KEY (`IdOwner`)
  REFERENCES `learndo`.`schools` (`IdSchool`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`coursescategories` 
DROP FOREIGN KEY `FK_COURSECAT_CATEGORY`,
DROP FOREIGN KEY `FK_COURSECAT_COURSE`;
ALTER TABLE `learndo`.`coursescategories` 
CHANGE COLUMN `IdCoursesCategory` `IdCoursesCategory` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla compuesta de categorias y cursos' ,
CHANGE COLUMN `IdCourse` `IdCourse` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla cursos' ,
CHANGE COLUMN `IdCategory` `IdCategory` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla categorías' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`coursescategories` 
ADD CONSTRAINT `FK_COURSECAT_CATEGORY`
  FOREIGN KEY (`IdCategory`)
  REFERENCES `learndo`.`categories` (`IdCategory`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
ADD CONSTRAINT `FK_COURSECAT_COURSE`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`formasdepago` 
CHANGE COLUMN `IdFormaDePago` `IdFormaDePago` TINYINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de formas de pago' ,
CHANGE COLUMN `Nombre` `Nombre` VARCHAR(20) NOT NULL COMMENT 'Nombre de la forma de pago' ,
CHANGE COLUMN `Activo` `Activo` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;

ALTER TABLE `learndo`.`images` 
DROP FOREIGN KEY `FK_IMAGES_LEVEL`;
ALTER TABLE `learndo`.`images` 
CHANGE COLUMN `IdImages` `IdImages` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de imágenes' ,
CHANGE COLUMN `IdLevel` `IdLevel` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla niveles' ,
CHANGE COLUMN `Image` `Image` MEDIUMBLOB NOT NULL COMMENT 'Aquí se guarda la imagen del nivel' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`images` 
ADD CONSTRAINT `FK_IMAGES_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`levels` 
DROP FOREIGN KEY `FK_LEVEL_COURSES`;
ALTER TABLE `learndo`.`levels` 
CHANGE COLUMN `IdLevel` `IdLevel` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de niveles' ,
CHANGE COLUMN `IdCourse` `IdCourse` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla cursos' ,
CHANGE COLUMN `Title` `Title` VARCHAR(400) NOT NULL COMMENT 'Título del nivel' ,
CHANGE COLUMN `Price` `Price` DECIMAL(15,2) NOT NULL COMMENT 'Precio del nivel individual' ,
CHANGE COLUMN `TextLevel` `TextLevel` VARCHAR(500) NOT NULL COMMENT 'Texto asociado al nivel' ,
CHANGE COLUMN `UrlVideo` `UrlVideo` VARCHAR(500) NOT NULL COMMENT 'Dirección URL del video del nivel' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`levels` 
ADD CONSTRAINT `FK_LEVEL_COURSES`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`links` 
DROP FOREIGN KEY `FK_LINK_LEVEL`;
ALTER TABLE `learndo`.`links` 
CHANGE COLUMN `IdLinks` `IdLinks` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de vínculos' ,
CHANGE COLUMN `IdLevel` `IdLevel` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla de niveles' ,
CHANGE COLUMN `Link` `Link` VARCHAR(300) NOT NULL COMMENT 'Texto para los vínculos que se encuentran en los niveles' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`links` 
ADD CONSTRAINT `FK_LINK_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`messages` 
DROP FOREIGN KEY `FK_MESSAGES_ADDRESSEE`,
DROP FOREIGN KEY `FK_MESSAGES_SENDER`;
ALTER TABLE `learndo`.`messages` 
CHANGE COLUMN `IdMessages` `IdMessages` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de mensajes' ,
CHANGE COLUMN `IdUserSender` `IdUserSender` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla de usuarios especificamente del mandador' ,
CHANGE COLUMN `IdUserAddressee` `IdUserAddressee` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla de usuarios especificamente del recibidor' ,
CHANGE COLUMN `TextMessage` `TextMessage` TEXT NOT NULL COMMENT 'El texto de los mensajes' ,
CHANGE COLUMN `MessageDate` `MessageDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'La fecha cuando se envió el mensaje' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`messages` 
ADD CONSTRAINT `FK_MESSAGES_ADDRESSEE`
  FOREIGN KEY (`IdUserAddressee`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
ADD CONSTRAINT `FK_MESSAGES_SENDER`
  FOREIGN KEY (`IdUserSender`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`pdffiles` 
DROP FOREIGN KEY `FK_FILEPDF_LEVEL`;
ALTER TABLE `learndo`.`pdffiles` 
CHANGE COLUMN `IdPDFFile` `IdPDFFile` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de archivos PDF' ,
CHANGE COLUMN `IdLevel` `IdLevel` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla de niveles' ,
CHANGE COLUMN `UrlFile` `UrlFile` VARCHAR(500) NOT NULL COMMENT 'Dirección URL del archivo pdf' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`pdffiles` 
ADD CONSTRAINT `FK_FILEPDF_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`schools` 
DROP FOREIGN KEY `FK_SCHOOL_USER`;
ALTER TABLE `learndo`.`schools` 
CHANGE COLUMN `IdSchool` `IdSchool` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de escuelas' ,
CHANGE COLUMN `IdUser` `IdUser` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla usuario' ,
CHANGE COLUMN `Avatar` `Avatar` MEDIUMBLOB NOT NULL COMMENT 'Imagen o icono que representa a la escuela' ,
CHANGE COLUMN `Name` `Name` VARCHAR(100) NOT NULL COMMENT 'Nombre de la escuela' ,
CHANGE COLUMN `Signupdate` `Signupdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha cuando se registro a la escuela' ,
CHANGE COLUMN `LastUpdateDate` `LastUpdateDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha cuando se actualizo por última vez' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`schools` 
ADD CONSTRAINT `FK_SCHOOL_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`users` 
CHANGE COLUMN `IdUser` `IdUser` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de usuarios' ,
CHANGE COLUMN `Names` `Names` VARCHAR(100) NOT NULL COMMENT 'Nombres del usuario' ,
CHANGE COLUMN `FirstSurname` `FirstSurname` VARCHAR(30) NOT NULL COMMENT 'Primer apellido del usuario' ,
CHANGE COLUMN `SecondSurname` `SecondSurname` VARCHAR(30) NOT NULL COMMENT 'Segundo apellido del usuario' ,
CHANGE COLUMN `Avatar` `Avatar` MEDIUMBLOB NOT NULL COMMENT 'Imagen o icono que representa al usuario' ,
CHANGE COLUMN `Gender` `Gender` TINYINT NOT NULL COMMENT 'Género del usuario' ,
CHANGE COLUMN `Email` `Email` VARCHAR(100) NOT NULL COMMENT 'Correo electrónico del usuario' ,
CHANGE COLUMN `Password` `Password` VARCHAR(30) NOT NULL COMMENT 'Contraseña del usuario' ,
CHANGE COLUMN `Birthdate` `Birthdate` DATE NOT NULL COMMENT 'Fecha de nacimiento del usuario' ,
CHANGE COLUMN `Signupdate` `Signupdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha cuando se registro al usuario' ,
CHANGE COLUMN `LastUpdateDate` `LastUpdateDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha cuando se actualizó al usuario por última vez' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;

ALTER TABLE `learndo`.`usersbuylevels` 
DROP FOREIGN KEY `FK_PURCHASE_LEVEL`,
DROP FOREIGN KEY `FK_PURCHASE_USER`;
ALTER TABLE `learndo`.`usersbuylevels` 
CHANGE COLUMN `IdPurchase` `IdPurchase` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla compuesta de usuarios comprando niveles' ,
CHANGE COLUMN `IdUser` `IdUser` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla usuario' ,
CHANGE COLUMN `IdLevel` `IdLevel` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla niveles' ,
CHANGE COLUMN `LastVisitDate` `LastVisitDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Última fecha de visita' ,
CHANGE COLUMN `Progress` `Progress` TINYINT(1) NULL DEFAULT '0' COMMENT 'Progreso del nivel' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`usersbuylevels` 
ADD CONSTRAINT `FK_PURCHASE_LEVEL`
  FOREIGN KEY (`IdLevel`)
  REFERENCES `learndo`.`levels` (`IdLevel`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
ADD CONSTRAINT `FK_PURCHASE_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `learndo`.`usersregistercourses` 
DROP FOREIGN KEY `FK_REGISTER_COURSES`,
DROP FOREIGN KEY `FK_REGISTER_USER`;
ALTER TABLE `learndo`.`usersregistercourses` 
CHANGE COLUMN `IdRegistration` `IdRegistration` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla de usuarios registrando cursos' ,
CHANGE COLUMN `IdUser` `IdUser` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla usuario' ,
CHANGE COLUMN `IdCourse` `IdCourse` BIGINT NOT NULL COMMENT 'Llave foránea para el identificador de la tabla cursos' ,
CHANGE COLUMN `RegistrationDate` `RegistrationDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de registro' ,
CHANGE COLUMN `FinishDate` `FinishDate` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de completado' ,
CHANGE COLUMN `Active` `Active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Determina si está activo o no' ;
ALTER TABLE `learndo`.`usersregistercourses` 
ADD CONSTRAINT `FK_REGISTER_COURSES`
  FOREIGN KEY (`IdCourse`)
  REFERENCES `learndo`.`courses` (`IdCourse`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
ADD CONSTRAINT `FK_REGISTER_USER`
  FOREIGN KEY (`IdUser`)
  REFERENCES `learndo`.`users` (`IdUser`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;


select distinct
	c.table_name AS 'Table',
    c.column_name AS 'Column',
    c.data_type AS 'Data type',
    c.character_maximum_length AS 'MAX LENGTH',
    r.referenced_table_name AS 'References',
    c.column_key AS 'Key',
    c.is_nullable AS 'Required',
    c.extra AS 'Extra',
    c.column_comment AS 'Description',
    c.privileges AS 'Privileges'
FROM information_schema.tables AS t
INNER JOIN information_schema.columns AS c
	ON t.table_name = c.table_name AND t.table_schema = c.table_schema
LEFT JOIN information_schema.key_column_usage AS r
    ON t.table_name = r.table_name AND (c.column_name = r.column_name)
WHERE t.table_type IN ('BASE TABLE')
AND t.table_schema = 'learndo'
ORDER BY 1, --c.column_name, c.ordinal_position;