<?php

require_once "models/DbConnection.php";

class ChatModel{
    private static $procedureName = "sp_messages";
    protected $IdMessages;
    protected $TextMessage;
    protected $MessageDate;
    protected $IdUserSender;
    protected $nameUserSender;
    protected $firstSurnameSender;
    protected $secondSurnameSender;
    protected $avatarUserSend;
    protected $idUserAdd;
    protected $nameUserAdd;
    protected $firstSurnameAdd;
    protected $secondSurnameAdd;
    protected $avatarUserAdd;

    public function getIdMessages(){
        return $this->IdMessages;
    }

    public function setIdMessages($IdMessages){
        $this->IdMessages = $IdMessages;
    }
    

    public function getTextMessage(){
        return $this->TextMessage;
    }

    public function setTextMessage($TextMessage){
        $this->TextMessage = $TextMessage;
    }


    public function getMessageDate(){
        return $this->MessageDate;
    }

    public function setMessageDate($MessageDate){
        $this->MessageDate = $MessageDate;
    }


    public function getIdUserSender(){
        return $this->IdUserSender;
    }

    public function setIdUserSender($IdUserSender){
        $this->IdUserSender = $IdUserSender;
    }


    public function getNameUserSender(){
        return $this->nameUserSender;
    }

    public function setNameUserSender($nameUserSender){
        $this->nameUserSender = $nameUserSender;
    }


    public function getFirstSurnameSender(){
        return $this->firstSurnameSender;
    }

    public function setFirstSurnameSender($firstSurnameSender){
        $this->firstSurnameSender = $firstSurnameSender;
    }


    public function getSecondSurnameSender(){
        return $this->secondSurnameSender;
    }

    public function setSecondSurnameSender($secondSurnameSender){
        $this->secondSurnameSender = $secondSurnameSender;
    }

    
    public function getAvatarUserSend(){
        return $this->avatarUserSend;
    }

    public function setAvatarUserSend($avatarUserSend){
        $this->avatarUserSend = $avatarUserSend;
    }


    public function getIdUserAdd(){
        return $this->idUserAdd;
    }

    public function setIdUserAdd($idUserAdd){
        $this->idUserAdd = $idUserAdd;
    }


    public function getNameUserAdd(){
        return $this->nameUserAdd;
    }

    public function setNameUserAdd($nameUserAdd){
        $this->nameUserAdd = $nameUserAdd;
    }


    
    public function getFirstSurnameAdd(){
        return $this->firstSurnameAdd;
    }

    public function setFirstSurnameAdd($firstSurnameAdd){
        $this->firstSurnameAdd = $firstSurnameAdd;
    }


    public function getSecondSurnameAdd(){
        return $this->secondSurnameSender;
    }

    public function setSecondSurnameAdd($secondSurnameAdd){
        $this->secondSurnameAdd = $secondSurnameAdd;
    }


    public function getAvatarUserAdd(){
        return $this->avatarUserAdd;
    }

    public function setAvatarUserAdd($avatarUserAdd){
        $this->avatarUserAdd = $avatarUserAdd;
    }

    public function getUsersAndSchools($InputStr){
        $option = "S";
        $IdUserSender = UserSession::getCurrentUserId();
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , :idUser, NULL, NULL, NULL, NULL, :searchStr)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $IdUserSender, PDO::PARAM_INT);
        $statement->bindParam(":searchStr", $InputStr, PDO::PARAM_STR);

        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            
            foreach ($result as $user => $value){
                $result[$user]["Avatar"] = base64_encode( $result[$user]["Avatar"]);
                if(isset( $result[$user]["SchoolAvatar"]))
                $result[$user]["SchoolAvatar"]  = base64_encode( $result[$user]["SchoolAvatar"]);
            }


        }catch (Exception $e){
            echo $e;
        }

        return $result;
    
    }

    public function sendMessagesInsert($idUserAdd, $TextMessage){
        $option = "I";
        $IdUserSender = UserSession::getCurrentUserId();
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , :idUser, :idUserIn, :Message, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $IdUserSender, PDO::PARAM_INT);
        $statement->bindParam(":idUserIn", $idUserAdd, PDO::PARAM_INT);
        $statement->bindParam(":Message", $TextMessage, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    
    }

    public function getConversationsById($idUserAdd){
        $option = "B";
        $IdUserSender = UserSession::getCurrentUserId();
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , :idUser, :idUserIn, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $IdUserSender, PDO::PARAM_INT);
        $statement->bindParam(":idUserIn", $idUserAdd, PDO::PARAM_INT);

        try {
            $statement->execute();
            $result = $statement->fetchAll();
            


        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }


    public static function getUserByID($IdUserSender){
        $option = "BI";
        
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , NULL, :idUserIn, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUserIn", $IdUserSender, PDO::PARAM_INT);


        try {
            $statement->execute();
            $result = $statement->fetchAll();
            


        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function inbox(){
        $option = "L";
        $IdUserSender = UserSession::getCurrentUserId();
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , :idUser, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $IdUserSender, PDO::PARAM_INT);

        try {
            $statement->execute();
            $result = $statement->fetchAll();
            
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

}