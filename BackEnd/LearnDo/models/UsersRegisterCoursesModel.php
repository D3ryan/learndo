<?php

require_once "models/DbConnection.php";

class UsersRegisterCoursesModel {
    private static $procedureName = "sp_usersregistercourses";
    private $IdRegistration;
    private $IdUser;
    private $IdCourse;
    private $RegistrationDate;
    private $FinishDate;

    public function InsertUserRegisterCourse() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, :IdCourse, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $this->IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdCourse", $this->IdCourse, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function IsUserRegistered($IdUser, $IdCourse) {
        $option = "R";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, :IdCourse, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;
        try {
            $statement->execute();
            $result = $statement->fetch();
        } catch(Exception $e){
            echo $e;
        } 

        return $result;
    }

    public static function getCoursesByUserRegisteredId($IdUser){
        $option = "UC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $IdUser, PDO::PARAM_INT);

        $result = null;
        try {
            $statement->execute();
            $result = $statement->fetchAll();
        } catch(Exception $e){
            echo $e;
        } 

        return $result;
    }

    public function getIdRegistration()
    {
        return $this->IdRegistration;
    }

    public function setIdRegistration($IdRegistration)
    {
        $this->IdRegistration = $IdRegistration;
    }

    public function getRegistrationDate()
    {
        return $this->RegistrationDate;
    }

    public function setRegistrationDate($RegistrationDate)
    {
        $this->RegistrationDate = $RegistrationDate;
    }

    public function getFinishDate()
    {
        return $this->FinishDate;
    }

    public function setFinishDate($FinishDate)
    {
        $this->FinishDate = $FinishDate;
    }

    public function getIdUser()
    {
        return $this->IdUser;
    }

    public function setIdUser($IdUser)
    {
        $this->IdUser = $IdUser;
    }

    public function getIdCourse()
    {
        return $this->IdCourse;
    }

    public function setIdCourse($IdCourse)
    {
        $this->IdCourse = $IdCourse;
    }
}