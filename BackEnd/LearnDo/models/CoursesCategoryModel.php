<?php

require_once "models/DbConnection.php";

class CoursesCategoryModel {
    private static $procedureName = "sp_coursesCategories";
    private $idCourseCategory;
    private $idCourse;
    private $idCategory;

    public function getIdCourse(){
        return $this->idCourse;
    }

    public function setIdCourse($idCourse){
        $this->idCourse = $idCourse;
    }

    public function getIdCourseCategory(){
        return $this->idCourseCategory;
    }

    public function setIdCourseCategory($idCourseCategory){
        $this->idCourseCategory = $idCourseCategory;
    }
    
    public function getIdCategory(){
        return $this->idCategory;
    }

    public function setIdCategory($idCategory){
        $this->idCategory = $idCategory;
    }
    
    public function InsertCourseCategory() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, :idCategory, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $this->idCourse, PDO::PARAM_INT);
        $statement->bindParam(":idCategory", $this->idCategory, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getCategoriesByIdCourse($IdCourse){
        $option = "C";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetchAll();

        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }
}