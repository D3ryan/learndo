<?php 

    require_once "models/DbConnection.php";

    class ImagesModel{
        private static $procedureName = "sp_images";
        private $idImages;
        private $idLevel;
        private $image;

        public function InsertImage() {
                $option = "I";
                $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idLevel, :image, NULL)");

                $statement->bindParam(":option", $option, PDO::PARAM_STR);
                $statement->bindParam(":idLevel", $this->idLevel, PDO::PARAM_INT);
                $statement->bindParam(":image", $this->image, PDO::PARAM_LOB);

                $result = true;
                try {
                $statement->execute();
                } catch(Exception $e){
                echo $e;
                $result =  false;
                } 

                return $result;
        }

        public static function getImagesByIdLevel($idLevel){
                $option = "L";
                $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idLevel, NULL, NULL)");

                $statement->bindParam(":option", $option, PDO::PARAM_STR);
                $statement->bindParam(":idLevel", $idLevel, PDO::PARAM_INT);

                $result = null;
                try {
                        $statement->execute();
                        $result = $statement->fetchAll();
                } catch(Exception $e){
                        echo $e;
                } 

                return $result;
        }

        public function getIdImages()
        {
                return $this->idImages;
        }

        public function setIdImages($idImages)
        {
                $this->idImages = $idImages;
        }

        public function getIdLevel()
        {
                return $this->idLevel;
        }

        public function setIdLevel($idLevel)
        {
                $this->idLevel = $idLevel;
        }

        public function getImage()
        {
                return $this->image;
        }

        public function setImage($image)
        {
                $this->image = $image;
        }
    }