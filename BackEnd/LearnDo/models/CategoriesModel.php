<?php

require_once "../models/DbConnection.php";

class CategoryModel implements JsonSerializable {
    private static $procedureName = "sp_Categories";
    private $idCategory;
    private $name;
    private $description;
    private $active;

    public function getIdCategory(){
        return $this->idCategory;
    }

    public function setIdCategory($idCategory){
        $this->idCategory = $idCategory;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function getActive(){
        return $this->active;
    }

    public function setActive($active){
        $this->active = $active;
    }

    public function jsonSerialize(){
        return array(
            'IdCategory' => $this->idCategory,
            'Name' => $this->name,
            'Description' => $this->description,
            'Active' => $this->active
        );
    }

    public static function getAllCategories(){
        $option = "X";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);

        $result = null;
        try {

            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        }catch (Exception $e){
            echo $e;
            $result = false;
        }

        return $result;
    }
    
    public static function InsertCategory($category){
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :name, :description, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":name", $category["name"], PDO::PARAM_STR);
        $statement->bindParam(":description", $category["description"], PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();

        }catch (Exception $e){
            echo $e;
            $result = false;
        }

        return $result;
    }
}