<?php
    require_once "models/DbConnection.php";

class LevelModel {
    private static $procedureName = "sp_levels";
    private $idlevel;
    private $idCourse;
    private $title;
    private $price;
    private $textlevel;
    private $textLinks;
    private $urlVideo;

    public function InsertLevel() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, :title, :price, :textlevel, :textLinks, :urlVideo, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $this->idCourse, PDO::PARAM_INT);
        $statement->bindParam(":title", $this->title, PDO::PARAM_STR);
        $statement->bindParam(":price", $this->price, PDO::PARAM_STR);
        $statement->bindParam(":textlevel", $this->textlevel, PDO::PARAM_STR);
        $statement->bindParam(":textLinks", $this->textLinks, PDO::PARAM_STR);
        $statement->bindParam(":urlVideo", $this->urlVideo, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public function UpdateInsertLevels() {
        $option = "IU";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idLevel, :idCourse, :title, :price, :textlevel, :textLinks, :urlVideo, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idLevel", $this->idlevel, PDO::PARAM_INT);
        $statement->bindParam(":idCourse", $this->idCourse, PDO::PARAM_INT);
        $statement->bindParam(":title", $this->title, PDO::PARAM_STR);
        $statement->bindParam(":price", $this->price, PDO::PARAM_STR);
        $statement->bindParam(":textlevel", $this->textlevel, PDO::PARAM_STR);
        $statement->bindParam(":textLinks", $this->textLinks, PDO::PARAM_STR);
        $statement->bindParam(":urlVideo", $this->urlVideo, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getLevelById($idlevel){
        $option = "ID";

        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idLevel, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idLevel", $idlevel, PDO::PARAM_INT);

        $level = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $level = new LevelModel();
                $level->setIdlevel($result["IdLevel"]);
                $level->setIdCourse($result["IdCourse"]);
                $level->setTitle($result["Title"]);
                $level->setPrice($result["Price"]);
                $level->setTextlevel($result["TextLevel"]);
                $level->settextLinks($result["TextLinks"]);
                $level->setUrlVideo($result["UrlVideo"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $level;
    }

    public static function getIdlevelByIdCourse($idCourse){
        $option = "LC";

        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);

        $level = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $level = new LevelModel();
                $level->setIdlevel($result["IdLevel"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $level;
    }

    public static function getLevelsByIdCourse($idCourse){
        $option = "L";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getLevelsByIdCourseForEdition($idCourse){
        $option = "EL";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getCheapestLevelByIdCourse($idCourse){
        $option = "CH";

        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);

        $level = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $level = new LevelModel();
                $level->setPrice($result["Price"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $level;
    }

    public function getIdlevel()
    {
        return $this->idlevel;
    }

    public function setIdlevel($idlevel)
    {
        $this->idlevel = $idlevel;
    }

    public function getIdCourse()
    {
        return $this->idCourse;
    }

    public function setIdCourse($idCourse)
    {
        $this->idCourse = $idCourse;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getTextlevel()
    {
        return $this->textlevel;
    }

    public function setTextlevel($textlevel)
    {
        $this->textlevel = $textlevel;
    }

    public function getUrlVideo()
    {
        return $this->urlVideo;
    }

    public function setUrlVideo($urlVideo)
    {
        $this->urlVideo = $urlVideo;
    }

    public function gettextLinks()
    {
        return $this->textLinks;
    }

    public function settextLinks($textLinks)
    {
        $this->textLinks = $textLinks;
    }
}
    