<?php

require_once "models/DbConnection.php";

class UsersBuyCoursesModel {
    private static $procedureName = "sp_usersbuycourses";
    private $IdPurchase;
    private $IdUser;
    private $IdCourse;
    private $PaymentMethod;
    private $PurchasePrice;

    public function InsertUsersBuyCourses() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, :IdCourse, :PaymentMethod, :PurchasePrice, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $this->IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdCourse", $this->IdCourse, PDO::PARAM_INT);
        $statement->bindParam(":PaymentMethod", $this->PaymentMethod, PDO::PARAM_INT);
        $statement->bindParam(":PurchasePrice", $this->PurchasePrice, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getUserHasTheCourse($IdUser, $IdCourse) {
        $option = "H";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, :IdCourse, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;
        try {

            $statement->execute();
            $result = $statement->fetch();

        } catch(Exception $e){
            echo $e;
        } 

        return $result;
    }

    public function getIdPurchase()
    {
        return $this->IdPurchase;
    }

    public function setIdPurchase($IdPurchase)
    {
        $this->IdPurchase = $IdPurchase;
    }

    public function getIdUser()
    {
        return $this->IdUser;
    }

    public function setIdUser($IdUser)
    {
        $this->IdUser = $IdUser;
    }

    public function getIdCourse()
    {
        return $this->IdCourse;
    }

    public function setIdCourse($IdCourse)
    {
        $this->IdCourse = $IdCourse;
    }

    public function getPaymentMethod()
    {
        return $this->PaymentMethod;
    }

    public function setPaymentMethod($PaymentMethod)
    {
        $this->PaymentMethod = $PaymentMethod;
    }

    public function getPurchasePrice()
    {
        return $this->PurchasePrice;
    }

    public function setPurchasePrice($PurchasePrice)
    {
        $this->PurchasePrice = $PurchasePrice;
    }
}