<?php

require_once "models/DbConnection.php";
require_once "models/UserModel.php";

class SchoolModel extends UserModel {
    private static $procedureName = "sp_Schools";
    private $idSchool;
    private $nameSchool;
    private $avatarSchool;

    public function getIdSchool(){
        return $this->idSchool;
    }

    public function setIdSchool($idSchool){
        $this->idSchool = $idSchool;
    }

    public function getAvatarSchool(){
        return $this->avatarSchool;
    }

    public function setAvatarSchool($avatarSchool){
        $this->avatarSchool = $avatarSchool;
    }

    public function getNameSchool(){
        return $this->nameSchool;
    }

    public function setNameSchool($nameSchool){
        $this->nameSchool = $nameSchool;
    }

    public function InsertUpdateSchool() {
        $option = "IU";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idSchool, :idUser, NULL, NULL, :avatarSchool, :nameSchool, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idSchool", $this->idSchool, PDO::PARAM_INT);
        $statement->bindParam(":idUser", $this->id, PDO::PARAM_INT);
        $statement->bindParam(":avatarSchool", $this->avatarSchool, PDO::PARAM_LOB);
        $statement->bindParam(":nameSchool", $this->nameSchool, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getSchoolByUserId($idUser) {
        $option = "ID";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idUser, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $idUser, PDO::PARAM_INT);

        $school = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $school = new SchoolModel();
                $school->setIdSchool($result["IdSchool"]);
                $school->setId($result["IdUser"]);
                $school->setAvatarSchool($result["Avatar"]);
                $school->setNameSchool($result["Name"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $school;
    }

    
    public static function getSchoolById($idSchool) {
        $option = "S";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idSchool, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idSchool", $idSchool, PDO::PARAM_INT);

        $school = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $school = new SchoolModel();
                $school->setIdSchool($result["IdSchool"]);
                $school->setId($result["IdUser"]);
                $school->setAvatarSchool($result["Avatar"]);
                $school->setNameSchool($result["Name"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $school;
    }


    public static function getUserIdByEmailAndPassword($email, $password){
        $option = "L";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, NULL, :email, :password, NULL, NULL, NULL, NULL, NULL)");
        
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":email", $email, PDO::PARAM_STR);
        $statement->bindParam(":password", $password, PDO::PARAM_STR);

        $user = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
    
            if($result){
            $user = new SchoolModel();
            $user->setId($result["IdUser"]);
            $user->setIdSchool($result["IdSchool"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $user;

    }

    public static function getSchoolSales($idOwner){
        $option = "SS";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idSchool, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
        
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idSchool", $idOwner, PDO::PARAM_INT);

        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
    
            if(!$result){
                $result = null;
            }

        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

}