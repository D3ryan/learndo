<?php

require_once "models/DbConnection.php";

class CommentariesModel {
    private static $procedureName = "sp_commentaries";
    private $IdCommentary;
    private $IdUser;
    private $IdCourse;
    private $textCommentary;
    private $rating;

    // public function InsertCourseCategory() {
    //     $option = "I";
    //     $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idCourse, :idCategory, NULL)");

    //     $statement->bindParam(":option", $option, PDO::PARAM_STR);
    //     $statement->bindParam(":idCourse", $this->idCourse, PDO::PARAM_INT);
    //     $statement->bindParam(":idCategory", $this->idCategory, PDO::PARAM_INT);

    //     $result = true;
    //     try {
    //         $statement->execute();
    //     } catch(Exception $e){
    //         echo $e;
    //         $result =  false;
    //     } 

    //     return $result;
    // }

    public static function updateCommenteById($IdComment, $rating, $textCommentary, $active){
        $option = "U";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idComment, NULL, :idCourse, :TextComment, :Rating, NULL, :Active)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idComment", $IdComment, PDO::PARAM_INT);
        $statement->bindParam(":idCourse", $IdCourse, PDO::PARAM_INT);
        $statement->bindParam(":TextComment", $textCommentary, PDO::PARAM_STR);
        $statement->bindParam(":Rating", $rating, PDO::PARAM_INT);
        $statement->bindParam(":Active", $active, PDO::PARAM_BOOL);

        
        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getCommentByIdUserCourse($IdCourse){
        $option = "ID";
        $userComment = UserSession::getCurrentUserId();
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idUser, :idCourse, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $userComment, PDO::PARAM_INT);
        $statement->bindParam(":idCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetchAll();

        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function insertComment($IdCourse, $rating, $textCommentary){
        $option = "I";
        $userComment = UserSession::getCurrentUserId();
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idUser, :idCourse, :TextComment, :Rating, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $userComment, PDO::PARAM_INT);
        $statement->bindParam(":idCourse", $IdCourse, PDO::PARAM_INT);
        $statement->bindParam(":TextComment", $textCommentary, PDO::PARAM_STR);
        $statement->bindParam(":Rating", $rating, PDO::PARAM_INT);

        
        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getCommentariesByIdCourse($IdCourse){
        $option = "CC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, NULL, :idCourse, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetchAll();

        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function HasUserCompletedTheCourse($idUser, $idCourse){
        $option = "UCC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idUser, :idCourse, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);


        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetch();

        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public function getIdCommentary()
    {
        return $this->IdCommentary;
    }

    public function setIdCommentary($IdCommentary)
    {
        $this->IdCommentary = $IdCommentary;

        return $this;
    }

    
    public function getIdUser()
    {
        return $this->IdUser;
    }

    public function setIdUser($IdUser)
    {
        $this->IdUser = $IdUser;

        return $this;
    }


    public function getIdCourse()
    {
        return $this->IdCourse;
    }

    public function setIdCourse($IdCourse)
    {
        $this->IdCourse = $IdCourse;

        return $this;
    }

    public function getTextCommentary()
    {
        return $this->textCommentary;
    }

    public function setTextCommentary($textCommentary)
    {
        $this->textCommentary = $textCommentary;

        return $this;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }
}