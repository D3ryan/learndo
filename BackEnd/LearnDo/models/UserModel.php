<?php

require_once "models/DbConnection.php";

class UserModel {
    private static $procedureName = "sp_Users";
    protected $id;
    protected $names;
    protected $firstSurname;
    protected $secondSurname;
    protected $avatar;
    protected $gender;
    protected $email;
    protected $password;
    protected $birthdate;
    // private $lastUpdateDate;
    

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getNames(){
        return $this->names;
    }

    public function setNames($names){
        $this->names = $names;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getFirstSurname(){
        return $this->firstSurname;
    }

    public function setFirstSurname($firstSurname){
        $this->firstSurname = $firstSurname;
    }

    public function getSecondSurname(){
        return $this->secondSurname;
    }

    public function setSecondSurname($secondSurname){
        $this->secondSurname = $secondSurname;
    }

    public function getGender(){
        return $this->gender;
    }

    public function setGender($gender){
        $this->gender = $gender;
    }

    public function getAvatar(){
        return $this->avatar;
    }

    public function setAvatar($avatar){
        $this->avatar = $avatar;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getBirthdate(){
        return $this->birthdate;
    }

    public function setBirthdate($birthdate){
        $this->birthdate = $birthdate;
    }

    public function InsertUser() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , :names, :firstSurname, :secondSurname, :avatar, :gender, :email, :password, :birthDate, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":names", $this->names, PDO::PARAM_STR);
        $statement->bindParam(":firstSurname", $this->firstSurname, PDO::PARAM_STR);
        $statement->bindParam(":secondSurname", $this->secondSurname, PDO::PARAM_STR);
        $statement->bindParam(":avatar", $this->avatar, PDO::PARAM_LOB);
        $statement->bindParam(":gender", $this->gender, PDO::PARAM_INT);
        $statement->bindParam(":email", $this->email, PDO::PARAM_STR);
        $statement->bindParam(":password", $this->password, PDO::PARAM_STR);
        $statement->bindParam(":birthDate", $this->birthdate, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getUserIdByEmailAndPassword($email, $password){
        $option = "L";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL , NULL, NULL, NULL, NULL, NULL, :email, :password, NULL, NULL, NULL, NULL)");
        
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":email", $email, PDO::PARAM_STR);
        $statement->bindParam(":password", $password, PDO::PARAM_STR);

        $user = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
    
            if($result){
            $user = new UserModel();
            $user->setId($result["IdUser"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $user;

    }

    public static function getUserById($id){
        $option = "ID";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :id , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);

        $user = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
        
            $user = new UserModel();

            $user->setId($result["IdUser"]);
            $user->setNames($result["Names"]);
            $user->setFirstSurname($result["FirstSurname"]);
            $user->setSecondSurname($result["SecondSurname"]);
            $user->setAvatar($result["Avatar"]);
            $user->setGender($result["Gender"]);
            $user->setEmail($result["Email"]);
            $user->setPassword($result["Password"]);
            $user->setBirthdate($result["Birthdate"]);
            // $user->setLastUpdateDate($result["LastUpdateDate"]);


        }catch (Exception $e){
            echo $e;
        }

        return $user;
    }


    public function UpdateUser() {
        $option = "U";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :id , :names, :firstSurname, :secondSurname, :avatar, :gender, :email, :password, :birthDate, NULL, NULL, NULL)");

        $statement->bindParam(":id", $this->id, PDO::PARAM_INT);
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":names", $this->names, PDO::PARAM_STR);
        $statement->bindParam(":firstSurname", $this->firstSurname, PDO::PARAM_STR);
        $statement->bindParam(":secondSurname", $this->secondSurname, PDO::PARAM_STR);
        $statement->bindParam(":avatar", $this->avatar, PDO::PARAM_LOB);
        $statement->bindParam(":gender", $this->gender, PDO::PARAM_INT);
        $statement->bindParam(":email", $this->email, PDO::PARAM_STR);
        $statement->bindParam(":password", $this->password, PDO::PARAM_STR);
        $statement->bindParam(":birthDate", $this->birthdate, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

}