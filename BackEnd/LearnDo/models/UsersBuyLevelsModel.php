<?php

require_once "models/DbConnection.php";

class UsersBuyLevelsModel {
    private static $procedureName = "sp_usersbuylevels";
    private $IdPurchase;
    private $IdUser;
    private $IdLevel;
    private $LastVisitDate;
    private $PaymentMethod;
    private $PurchasePrice;
    private $Progress;

    public function InsertUsersBuyLevels() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser,  NULL, :IdLevel, :PurchasePrice, :PaymentMethod, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $this->IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdLevel", $this->IdLevel, PDO::PARAM_INT);
        $statement->bindParam(":PaymentMethod", $this->PaymentMethod, PDO::PARAM_INT);
        $statement->bindParam(":PurchasePrice", $this->PurchasePrice, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getUserBoughtLevels($IdCourse, $IdUser) {
        $option = "L";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, :IdCourse, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;
        try {
            $statement->execute();
            $result = $statement->fetchAll();
        } catch(Exception $e){
            echo $e;
        } 
        return $result;
    }

    public static function getUserNotBoughtLevels($IdUser, $IdCourse) {
        $option = "LB";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, :IdCourse, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdCourse", $IdCourse, PDO::PARAM_INT);

        $result = null;
        try {
            $statement->execute();
            $result = $statement->fetchAll();
        } catch(Exception $e){
            echo $e;
        } 
        return $result;
    }

    public static function getVisualizedBoughtLevel($IdLevel, $IdUser) {
        $option = "V";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, NULL, :IdLevel, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdLevel", $IdLevel, PDO::PARAM_INT);
        $statement->bindParam(":IdUser",  $IdUser, PDO::PARAM_INT);

        $result = null;
        try {
            $statement->execute();
            $result = $statement->fetch();
            if($result == false) $result = null;

        } catch(Exception $e){
            echo $e;
        } 
        return $result;
    }

    public function UpdateProgress(){
        $option = "P";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser,  NULL, :IdLevel, NULL, NULL, NULL, :Progress, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $this->IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdLevel", $this->IdLevel, PDO::PARAM_INT);
        $statement->bindParam(":Progress", $this->Progress, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public function UpdateLastVisitDate(){
        $option = "LVD";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdUser, NULL, :IdLevel, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdUser", $this->IdUser, PDO::PARAM_INT);
        $statement->bindParam(":IdLevel", $this->IdLevel, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }


    public function getIdPurchase()
    {
        return $this->IdPurchase;
    }

    public function setIdPurchase($IdPurchase)
    {
        $this->IdPurchase = $IdPurchase;
    }

    public function getIdUser()
    {
        return $this->IdUser;
    }

    public function setIdUser($IdUser)
    {
        $this->IdUser = $IdUser;
    }

    public function getPaymentMethod()
    {
        return $this->PaymentMethod;
    }

    public function setPaymentMethod($PaymentMethod)
    {
        $this->PaymentMethod = $PaymentMethod;
    }

    public function getPurchasePrice()
    {
        return $this->PurchasePrice;
    }

    public function setPurchasePrice($PurchasePrice)
    {
        $this->PurchasePrice = $PurchasePrice;
    }

    public function getIdLevel()
    {
        return $this->IdLevel;
    }

    public function setIdLevel($IdLevel)
    {
        $this->IdLevel = $IdLevel;
    }

    public function getLastVisitDate()
    {
        return $this->LastVisitDate;
    }

    public function setLastVisitDate($LastVisitDate)
    {
        $this->LastVisitDate = $LastVisitDate;
    }

    public function getProgress()
    {
        return $this->Progress;
    }

    public function setProgress($Progress)
    {
        $this->Progress = $Progress;
    }
}