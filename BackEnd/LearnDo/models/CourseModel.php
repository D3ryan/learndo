<?php

require_once "models/DbConnection.php";

class CourseModel {
    private static $procedureName = "sp_courses";
    private static $procedureSearch = "sp_advancedsearch";
    private $idCourse;
    private $idOwner;
    private $schoolName;
    private $title;
    private $image;
    private $description;
    private $totalprice;
    private $creationDate;
    private $students;
    private $rating;

    public function InsertCourse() {
        $option = "I";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idOwner, :title, :image, :description, :totalprice, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idOwner", $this->idOwner, PDO::PARAM_INT);
        $statement->bindParam(":title", $this->title, PDO::PARAM_STR);
        $statement->bindParam(":image", $this->image, PDO::PARAM_LOB);
        $statement->bindParam(":description", $this->description, PDO::PARAM_STR);
        $statement->bindParam(":totalprice", $this->totalprice, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public function UpdateCourse() {
        $option = "U";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idCourse, NULL, :title, :image, :description, :totalprice, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $this->idCourse, PDO::PARAM_INT);
        $statement->bindParam(":title", $this->title, PDO::PARAM_STR);
        $statement->bindParam(":image", $this->image, PDO::PARAM_LOB);
        $statement->bindParam(":description", $this->description, PDO::PARAM_STR);
        $statement->bindParam(":totalprice", $this->totalprice, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result =  false;
        } 

        return $result;
    }

    public static function getCourseById($IdCourse){
        $option = "SH";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :IdCourse, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdCourse", $IdCourse, PDO::PARAM_INT);

        $course = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $course = new CourseModel();
                $course->setIdCourse($result["IdCourse"]);
                $course->setImage($result["Image"]);
                $course->setTitle($result["Title"]);
                $course->setDescription($result["Description"]);
                $course->setTotalprice($result["TotalPrice"]);
                $course->setCreationDate($result["CreationDate"]);
                $course->setSchoolName($result["SchoolName"]);
                $course->setStudents($result["Students"]);
                $course->setRating($result["Rating"]);  
            }

        }catch (Exception $e){
            echo $e;
        }

        return $course;
    }

    public static function getLastIdCourseByIdOwner($idOwner){
        $option = "O";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idOwner, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idOwner", $idOwner, PDO::PARAM_INT);

        $course = null;

        try {
            $statement->execute();
            $result = $statement->fetch();
            
            if($result){
                $course = new CourseModel();
                $course->setIdCourse($result["IdCourse"]);
            }

        }catch (Exception $e){
            echo $e;
        }

        return $course;
    }

    public static function getCoursesBySchoolId($IdSchool){
        $option = "CS";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :IdSchool, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":IdSchool", $IdSchool, PDO::PARAM_INT);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getRecentCourses(){
        $option = "RC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getBestSellerCourses(){
        $option = "SC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }
    
    public static function getBestRatedCourses(){
        $option = "R";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getCourseDetailByIdCourse($idCourse, $idOwner){
        $option = "DC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idCourse, :idOwner, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);
        $statement->bindParam(":idOwner", $idOwner, PDO::PARAM_INT);

        $result = true;

        try {
            $statement->execute();
            $result = $statement->fetch();
            if(!$result) $result = null;
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getStudentsRegisteredByIdCourse($idCourse, $idOwner){
        $option = "SRD";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idCourse, :idOwner, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);
        $statement->bindParam(":idOwner", $idOwner, PDO::PARAM_INT);

        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getStudentsBoughtLevelsByIdCourse($idCourse, $idOwner){
        $option = "SBL";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idCourse, :idOwner, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);
        $statement->bindParam(":idOwner", $idOwner, PDO::PARAM_INT);


        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
            if(!$result) $result = null;
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getStudentsBoughtCourseByIdCourse($idCourse, $idOwner){
        $option = "SBC";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idCourse, :idOwner, NULL, NULL, NULL, NULL, NULL, NULL)");
        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);
        $statement->bindParam(":idOwner", $idOwner, PDO::PARAM_INT);


        $result = null;

        try {
            $statement->execute();
            $result = $statement->fetchAll();
            if(!$result) $result = null;
        }catch (Exception $e){
            echo $e;
        }

        return $result;
    }

    public static function getSearchedCourses($startDate, $endDate, $idCategory, $schoolName, $title) {
        $option = "B";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureSearch."(:option, :startDate, :endDate, :idCategory, :schoolName, :title)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":startDate", $startDate, PDO::PARAM_STR);
        $statement->bindParam(":endDate", $endDate, PDO::PARAM_STR);
        $statement->bindParam(":idCategory", $idCategory, PDO::PARAM_INT);
        $statement->bindParam(":schoolName", $schoolName, PDO::PARAM_STR);
        $statement->bindParam(":title", $title, PDO::PARAM_STR);

        $result = null;
        try {
            $statement->execute();
            $result = $statement->fetchAll();
        } catch(Exception $e){
            echo $e;
        } 
        return $result;
    }

    public static function setActivateDeactivate($idCourse) {
        $option = "D";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, :idCourse, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");

        $statement->bindParam(":option", $option, PDO::PARAM_STR);
        $statement->bindParam(":idCourse", $idCourse, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
        } catch(Exception $e){
            echo $e;
            $result = false;
        } 
        return $result;
    }


    public function getIdCourse(){
        return $this->idCourse;
    }

    public function setIdCourse($idCourse){
        $this->idCourse = $idCourse;
    }

    public function getIdOwner(){
        return $this->idOwner;
    }

    public function setIdOwner($idOwner){
        $this->idOwner = $idOwner;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getImage(){
        return $this->image;
    }

    public function setImage($image){
        $this->image = $image;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function getTotalprice(){
        return $this->totalprice;
    }

    public function setTotalprice($totalprice){
        $this->totalprice = $totalprice;
    }

    public function getSchoolName()
    {
        return $this->schoolName;
    }

    public function setSchoolName($schoolName)
    {
        $this->schoolName = $schoolName;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }
    public function getStudents()
    {
        return $this->students;
    }
    public function setStudents($students)
    {
        $this->students = $students;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
    }
}