<?php 

    require_once "models/DbConnection.php";

    class PDFFilesModel {
        private static $procedureName = "sp_pdfFiles";
        private $idPDFFile;
        private $idLevel;
        private $nameFile;
        private $urlFile;

        public function InsertPDFFile() {
            $option = "I";
            $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idLevel, :nameFile, :urlFile, NULL)");
    
            $statement->bindParam(":option", $option, PDO::PARAM_STR);
            $statement->bindParam(":idLevel", $this->idLevel, PDO::PARAM_INT);
            $statement->bindParam(":nameFile", $this->nameFile, PDO::PARAM_STR);
            $statement->bindParam(":urlFile", $this->urlFile, PDO::PARAM_STR);
    
            $result = true;
            try {
                $statement->execute();
            } catch(Exception $e){
                echo $e;
                $result =  false;
            } 
    
            return $result;
        }

        public static function getPDFFilesByIdLevel($idLevel){
            $option = "L";
            $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:option, NULL, :idLevel, NULL, NULL, NULL)");

            $statement->bindParam(":option", $option, PDO::PARAM_STR);
            $statement->bindParam(":idLevel", $idLevel, PDO::PARAM_INT);

            $result = null;
            try {
                $statement->execute();
                $result = $statement->fetchAll();
            } catch(Exception $e){
                echo $e;
            } 

            return $result;
        }

        public function getIdPDFFile()
        {
                return $this->idPDFFile;
        }

        public function setIdPDFFile($idPDFFile)
        {
                $this->idPDFFile = $idPDFFile;
        }

        public function getIdLevel()
        {
                return $this->idLevel;
        }

        public function setIdLevel($idLevel)
        {
                $this->idLevel = $idLevel;
        }

        public function getUrlFile()
        {
                return $this->urlFile;
        }

        public function setUrlFile($urlFile)
        {
                $this->urlFile = $urlFile;
        }

        public function getNameFile()
        {
            return $this->nameFile;
        }

        public function setNameFile($nameFile)
        {
            $this->nameFile = $nameFile;
        }
    }