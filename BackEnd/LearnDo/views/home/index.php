<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/index.css"?>">
<title>Inicio - LearnDo!</title>
<main>
    <div class="container-fluid Presentacion">
        <div class="row align-items-center presentacion">
            <div class="col-sm-6 Presentacion__letra">
                <h3 class="Presentacion__letra-h3-learn"> Learn Do!</h3>
                <h3 class="Presentacion__letra-h3-Click">Conocimiento en un click</h3>
                <p class="Presentacion__letra-h2-Aprende">Aprende con los cursos de los mejores profesores,
                    los mejores cursos.</p>
                <p class="Presentacion__letra-h2-ilimitado">Ten el conocimiento ilimitado a tus manos.</p>
                <a href="<?php echo Template::Route(UsersController::ROUTE, UsersController::LOGIN)?>"><button class="btn Presentacion__btn">¡Inicia ya!</button></a>
            </div>
            <div class="col-sm-5 Presentacion__image">
                <img class="Presentacion__image-Atlantic img-fluid" src="<?php echo Template::ROOT_PATH . "views/img/homepageImg2.png"?>">
            </div>
        </div>
    </div>
    <div class="container categorias text-center my-5 p-3">
        <div class=" categorias__letra">
            <h3 class="categorias__letra-h3">Comienza ahora</h3>
            <p class="categorias__letra-p">Estudia en todas las áreas que puedes imaginar</p>
        </div>
        <div class="row container-btn justify-content-center">
            <?php foreach($categories as $category): ?>
                <div class="col-sm-3 categorias__btn">
                    <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SEARCH_COURSE) ?>/title-/startdate-/endDate-/category-<?php echo $category["IdCategory"] ?>/school-"><button class="btn w-100 categorias__btn-item">
                        <?php echo $category["Name"] ?></button></a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="container text-center my-5 frases">
        <div class="row">
            <div class="col-sm-4 frases__p">
                <p class="frases__p-p">La mejor experiencia educativa digital con nuevas
                    formas de aprendizaje</p>
            </div>
            <div class="col-sm-4  frases__p">
                <p class="frases__p-p">Nuevo contenido cada semana, cursos para crear y adquirir nuevas
                    habilidades</p>
            </div>
            <div class="col-sm-4  frases__p">
                <p class="frases__p-p">Diplomas de certificación en formato digital</p>
            </div>
        </div>
    </div>
    <div class="container-fluid Video">
        <div class="Video__p">
            <h3 class="Video__p-h3">Ultimos agregados</h3>
            <p class="Video__p-p">Cursos más recientes de todas las categorías</p>
        </div>
        <div class="Video__container">
            <div class="owl-carousel owl-theme">
                <?php foreach($recentCourses as $course): ?>
              
                <div class="item video__item">
                    <div class="video__item-img">
                        <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::INDEX) . "/" . $course["IdCourse"]; ?>"> <img class="video__item-img-card"
                                src="data:;base64,<?php echo base64_encode($course["Image"])?>"></a>
                    </div>
                    <div class="video__item-descripcion">
                        <h6 class="video__item-descripcion-titulo"><?php echo $course["Title"] ?></h6>
                        <h6 class="video__item-descripcion-NombreA">Por <?php echo $course["SchoolName"] ?></h6>
                    </div>
                </div>

                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="container-fluid VideoP">
        <div class="Video__p">
            <h3 class="Video__p-h3">Mejor calificados</h3>
            <p class="Video__p-p">Los cursos con las mayores valoraciones positivas</p>
        </div>
        <div class="Video__container">
            <div class="owl-carousel owl-theme">
                <?php foreach($bestRatedCourses as $course): ?>
                    <div class="item video__item">
                        <div class="video__item-img">
                            <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::INDEX) . "/" . $course["IdCourse"]; ?>"> <img class="video__item-img-card"
                                    src="data:;base64,<?php echo base64_encode($course["Image"])?>"> </a>
                        </div>
                        <div class="video__item-descripcion">
                            <h6 class="video__item-descripcion-titulo"><?php echo $course["Title"] ?></h6>
                            <h6 class="video__item-descripcion-NombreA">Por <?php echo $course["SchoolName"] ?></h6>
                        </div>
                    </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="container-fluid Video">
        <div class="Video__p">
            <h3 class="Video__p-h3">Con más ventas</h3>
            <p class="Video__p-p">Cursos favoritos de nuestros usuarios</p>
        </div>
        <div class="Video__container">
            <div class="owl-carousel owl-theme">
            <?php foreach($bestSellerCourses as $course): ?>
                <div class="item video__item">
                    <div class="video__item-img">
                        <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::INDEX) . "/" . $course["IdCourse"]; ?>"> <img class="video__item-img-card"
                                src="data:;base64,<?php echo base64_encode($course["Image"])?>"> </a>
                    </div>
                    <div class="video__item-descripcion">
                        <h6 class="video__item-descripcion-titulo"><?php echo $course["Title"] ?></h6>
                        <h6 class="video__item-descripcion-NombreA">Por <?php echo $course["SchoolName"] ?></h6>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</main>
<?php
function Scripts(){
    include "views/home/scripts/home.scripts.php";
}
?>