<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/Chat.css"?>">

<title>LearnDo - Chat</title>

<div id="divScreen" class="container-fluid chat">
    <div class="row chat__row">
        <!-- buzon -->
        <div class="col-2 col-sm-4 chat__row-colM d-flex flex-column">
            <div class="chat__row-colM-mensaje">
                <h3 class="chat__row-colM-mensaje-h3 d-none d-sm-block ">Mensajes</h3>
                
                <img src="<?php echo Template::ROOT_PATH . "views/img/mensaje_M.png"?>" class="chat__row-colM-mensaje-img d-block d-sm-none" >
            </div>
            <div class="form-outline chat__row-colM-busqueda d-none d-sm-block ">
                <button id="BackSearch" name="BackSearch" class="btn btn-outline-light shadow-none " hidden> 
                    <img src="<?php echo Template::ROOT_PATH . "views/img/flechaBack.png"?>">
                </button>
                <input type="text" id="SearchChat" name="SearchChat" class="form-control shadow-none chat__row-colM-busqueda-input"
                    placeholder="Buscar">
            </div>

            <section id="tablaUsers">
                <!-- TABLA -->
            </section>
            <!-- ENTRADA USUARIOS  -->
            <?php if(empty($userIn)) :?>
                <h5 class="chat__row-colM-entrada-users-h5 mt-5 mx-4 " > 
                    <!-- Buzon comentario sin ninguna conversación -->
                    Aun no has tenido ninguna conversación, ¡busca a alguien para empezar!
                </h5>
                <?php else :?>
            <div id="buzonDiv" class="chat__row-colM-e flex-grow-1">
                <?php foreach($userIn as $user) :?>
                    <div class="chat__row-colM-entrada">
                        <div class="chat__row-colM-entrada-users">
                        <a class='vivian_a' href="<?php echo Template::Route(MessageController::ROUTE, MessageController::CLICK) . "/" . $user["InboxUserID"]; ?>">
                                <div class="row chat__row-colM-entrada-users-row ">
                                    <div class="col-sm-3 chat__row-colM-entrada-users-col-img">
                                        <img class="chat__row-colM-entrada-users-img" src="data:;base64,<?php echo base64_encode($user["InboxUserAvatar"]); ?>">
                                    </div>
                                    <div class="col-sm-9 chat__row-colM-entrada-users-col-name d-none d-sm-block ">
                                        <h5 class="chat__row-colM-entrada-users-h5"><?php echo $user["InboxUserName"] ."  ".$user["InboxUserFName"] ."  ".$user["InboxUserSName"]; ?></h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif ?>
        </div>
        <!-- buzon -->
        
        <!-- conversaciones -->
        <div class="col-10 col-sm-8 chat__row-colC d-flex flex-column">
            <div class="row chat__row-colC-chat"> 
            <?php if (isset($userC)): ?>
                                    <!-- AL DAR CLICK -->
                <?php if(empty($userC)) :?>
                    <!-- No deberia de entrar nunca -->
                    <span>Creo que aun no has empezado ninguna conversacion</span>
                
                <?php else :?>
                                            
                    <div class="col-sm-1 chat__row-colC-User3">
                        <img src="data:;base64,<?php echo base64_encode($userC[0]["InboxUserAvatar"]); ?>" class="chat__row-colC-User-img  d-none d-sm-block ">
                    </div>
                    <div class="col-sm-8 chat__row-colC-User9">
                        <h3 class="chat__row-colC-User-name"><?php echo $userC[0]["InboxUserName"] ."  ".$userC[0]["InboxUserFName"] ."  ".$userC[0]["InboxUserSName"]; ?></h3>
                    </div>
                
                <?php endif ?>
            </div>
            <!-- CONVERSACION -->
            <div class="chat__row-colC-all d-flex flex-column flex-grow-1">
                <div class=" chat__row-colC-conversacion flex-grow-1">

                <?php foreach($messages as $messagesWC) :?>
                    <?php if(($userC[0]["InboxUserID"]) != ($messagesWC["AddID"])) :?>
                        <div class="chat__row-colC-conversacion-UserB" >
                            <div class="row chat__row-colC-conversacion-UserB-row">
                                <div class="col-1 col-sm-1 chat__row-colC-conversacion-UserB-colI me-3 d-none d-sm-block">
                                    <img class="chat__row-colC-conversacion-UserB-img" src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                                </div>
                                <div class="col-1 col-sm-1 chat__row-colC-conversacion-UserB-colI me-3  d-block d-sm-none">
                                    <img class="chat__row-colC-conversacion-UserB-img2" src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                            </div>
                                <div class="col-10 col-sm-7 chat__row-colC-conversacion-UserB-colC">
                                    <h6 class="chat__row-colC-conversacion-UserB-h6 px-2"> <?php echo $messagesWC["TextMessage"]?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    <?php else :?>
                    <div class="chat__row-colC-conversacion-UserA" >
                        <div class="row justify-content-end chat__row-colC-conversacion-UserA-row">
                            <div class="col-10 col-sm-7 chat__row-colC-conversacion-UserA-colC">
                                <h6 class="chat__row-colC-conversacion-UserA-h6 px-2"> 
                                <?php echo $messagesWC["TextMessage"]?>
                                </h6>
                            </div>
                            <div class="col-1 col-sm-1 chat__row-colC-conversacion-UserA-colI mx-2">
                                <img class="chat__row-colC-conversacion-UserA-img d-none d-sm-block" src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                                <img class="chat__row-colC-conversacion-UserA-img2 d-block d-sm-none"  src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                <?php endforeach; ?>
                <!-- CONVERSACION -->
            <?php else: ?>    
                                <!-- AL ENTRAR -->
                <?php if(empty($userIn)) :?>
                    <div class="col-sm-8 chat__row-colC-User9">
                        <!-- Nombre de usuario, no hay conversacciones asi que no debe de aparecer nada -->
                        <h3 class="chat__row-colC-User-name">  .  </h3>
                    </div>
                
                <?php else :?>
                                            
                    <div class="col-sm-1 chat__row-colC-User3">
                        <img src="data:;base64,<?php echo base64_encode($userIn[0]["InboxUserAvatar"]); ?>" class="chat__row-colC-User-img  d-none d-sm-block ">
                    </div>
                    <div class="col-sm-8 chat__row-colC-User9">
                    <h3 class="chat__row-colC-User-name"><?php echo $userIn[0]["InboxUserName"] ."  ".$userIn[0]["InboxUserFName"] ."  ".$userIn[0]["InboxUserSName"]; ?></h3>
                    </div>         
                <?php endif ?>
            </div>
            <div class="chat__row-colC-all d-flex flex-column flex-grow-1">
                <div class=" chat__row-colC-conversacion flex-grow-1">
            <?php if($messages == null):?>
                <!-- Espacio conversación blanco porque no hay nada -->
                <!-- <span>I need fondo blanco aaaaaaaaaas</span> -->
            <?php else: ?>
                <?php foreach($messages as $messagesWC) :?>
                    <?php if(($userIn[0]["InboxUserID"]) != ($messagesWC["AddID"])) :?>
                        <div class="chat__row-colC-conversacion-UserB" >
                            <div class="row chat__row-colC-conversacion-UserB-row">
                                <div class="col-1 col-sm-1 chat__row-colC-conversacion-UserB-colI me-3 d-none d-sm-block">
                                    <img class="chat__row-colC-conversacion-UserB-img" src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                                </div>
                                <div class="col-1 col-sm-1 chat__row-colC-conversacion-UserB-colI me-3  d-block d-sm-none">
                                    <img class="chat__row-colC-conversacion-UserB-img2" src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                            </div>
                                <div class="col-10 col-sm-7 chat__row-colC-conversacion-UserB-colC">
                                    <h6 class="chat__row-colC-conversacion-UserB-h6 px-2"> <?php echo $messagesWC["TextMessage"]?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    <?php else :?>
                    <div class="chat__row-colC-conversacion-UserA" >
                        <div class="row justify-content-end chat__row-colC-conversacion-UserA-row">
                            <div class="col-10 col-sm-7 chat__row-colC-conversacion-UserA-colC">
                                <h6 class="chat__row-colC-conversacion-UserA-h6 px-2"> 
                                <?php echo $messagesWC["TextMessage"]?>
                                </h6>
                            </div>
                            <div class="col-1 col-sm-1 chat__row-colC-conversacion-UserA-colI mx-2">
                                <img class="chat__row-colC-conversacion-UserA-img d-none d-sm-block" src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                                <img class="chat__row-colC-conversacion-UserA-img2 d-block d-sm-none"  src="data:;base64,<?php echo base64_encode($messagesWC["SenderAvatar"]); ?>">
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                <?php endforeach; ?>
            <?php endif?>
            <?php endif ?>

                </div>

                <?php if (isset($userC)): ?> 
                    <!-- AL DAR CLICK -->
                    <div class="chat__row-colC-write">
                    <?php if(empty($userIn)) :?>
                    <form id="SendMessage" method="post" enctype="multipart/form-data" action="<?php echo Template::Route(MessageController::ROUTE, MessageController::SEND). "/" . $userC[0]["InboxUserID"]; ?>"?>   
                        <div class="input-group my-auto message">
                            <input type="text" name="messageInput" class="form-control shadow-none space" placeholder="Aa"  aria-describedby="btn_send">
                            <button class="btn shadow-none " type="submit" id="btn_send">
                                <img class="Chat__send" src="<?php echo Template::ROOT_PATH . "views/img/enviarR.png"?>">
                            </button>    
                    <?php else :?> 
                    <form id="SendMessage" method="post" enctype="multipart/form-data" action="<?php echo Template::Route(MessageController::ROUTE, MessageController::SEND). "/" . $userC[0]["InboxUserID"]; ?>"?>
                    <div class="input-group my-auto message">
                            <input type="text" name="messageInput" class="form-control shadow-none space" placeholder="Aa"  aria-describedby="btn_send">
                            <button class="btn shadow-none " type="submit" id="btn_send">
                                <img class="Chat__send" src="<?php echo Template::ROOT_PATH . "views/img/enviarR.png"?>">
                            </button>
                    <?php endif ?>
                    </div>                                
                </form>
            </div>
                    <?php else: ?> 
                    <!-- AL ENTRAR -->
                        <div class="chat__row-colC-write">
                        <?php if(empty($userIn)) :?>
                    <form id="SendMessage" method="post" enctype="multipart/form-data" action="#">   
                        <div class="input-group my-auto message">
                            <input type="text" name="messageInput" class="form-control shadow-none space" placeholder="Aa"  aria-describedby="btn_send">
                            <button class="btn shadow-none " type="submit" id="btn_send" disabled>
                                <img class="Chat__send" src="<?php echo Template::ROOT_PATH . "views/img/enviarR.png"?>">
                            </button>    
                    <?php else :?> 

                    <form id="SendMessage" method="post" enctype="multipart/form-data" action="<?php echo Template::Route(MessageController::ROUTE, MessageController::SEND). "/" . $userIn[0]["InboxUserID"]; ?>"?>
                        <div class="input-group my-auto message">
                            <input type="text" name="messageInput" class="form-control shadow-none space" placeholder="Aa"  aria-describedby="btn_send">
                            <button class="btn shadow-none " type="submit" id="btn_send">
                                <img class="Chat__send" src="<?php echo Template::ROOT_PATH . "views/img/enviarR.png"?>">
                            </button>
                        </div>                                
                        <?php endif ?>
                    </form>
                </div>
                    <?php endif ?>
            </div>
        </div>
        <!-- conversaciones  -->
    </div>
</div>

<?php
function Scripts(){
    include "views/chat/scripts/chat.scripts.php";
}
?>
