<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo Template::ROOT_PATH . "views/img/libro.ico"?>" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/navbar.css"?> ">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/generals.css"?>">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/footer.css"?>">
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?php echo Template::ROOT_PATH ?>">
                    <img class="navbar__logo" src="<?php echo Template::ROOT_PATH . "views/img/Logo.png"?>">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <form class="d-flex mx-3 flex-grow-1" method="POST" action="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SEARCH_COURSE) ?> ">
                        <input class="form-control shadow-none me-2" name="title" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn shadow-none btn-outline-danger" type="submit">Search</button>
                    </form>
                    <ul class="navbar-nav mb-2 mb-lg-0">
                        <?php if(isset($userId)) : ?>
                            
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo Template::Route(MessageController::ROUTE, MessageController::BUZON) ?>">
                                <img class="navbar__iconsPages" src="<?php echo Template::ROOT_PATH . "views/img/mensajeicon.png"?>">
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::MY_COURSES) ?>">
                                <img class="navbar__iconsPages" src="<?php echo Template::ROOT_PATH . "views/img/youtube.png" ?>">
                            </a>
                        </li>
                        
                        <?php if(isset($userSchoolId)) :?>
                            <li class="nav-item">
                            <a class="nav-link active" href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SHOW_ALL_COURSES_FABRIC) ?>">
                                <img class="navbar__iconsPages" src="<?php echo Template::ROOT_PATH . "views/img/cursosFabrica.png"?>">
                            </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::CREATE)?>">
                                    <img class="navbar__iconsPages" src="<?php echo Template::ROOT_PATH . "views/img/crear.png" ?>">
                                </a>
                            </li>
                        <?php endif ?>


                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo Template::Route(UsersController::ROUTE, UsersController::PROFILE); ?>">
                                <img class="navbar__userImg" src="data:;base64,<?php echo base64_encode($user->getAvatar()); ?>">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo Template::Route(UsersController::ROUTE, UsersController::LOGOUT); ?>">
                                <i class="df_icon-door fas fa-door-open"></i>
                            </a>
                        </li>

                        <?php else :?>
                            <li class="nav-item">
                                <a class="btn shadow-none btn-outline-primary" href="<?php echo Template::Route(UsersController::ROUTE, UsersController::LOGIN); ?>">
                                    Iniciar Sesión
                                </a>
                            </li>
                        <?php endif ?>
                        
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <?php $this->DeterminePage();?>
    <footer>
        <section class="footer-container px-2 py-4 px-lg-5">
            <div class="df_title-row row justify-content-center  w-100">
                <div class="col-12 col-lg-5 text-center mb-3">
                    <img src="<?php echo Template::ROOT_PATH . "views/img/Logo.png"?>" alt="Logo de la empresa">
                </div>
                <div class="df_motto col-12 col-lg-5 text-center mb-3">
                    <p class="m-0">Transformando el aprendizaje de los estudiantes y formando una comunidad creativa e
                        innovadora</p>
                </div>
            </div>
            <div class="df_category-container">
                <p class="df_categoryTitle text-center mt-2">Categorías</p>
                <div class="df_categories row mb-4  w-100">
                    <?php foreach($categories as $category): ?>
                        <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SEARCH_COURSE) ?>/title-/startdate-/endDate-/category-<?php echo $category["IdCategory"] ?>/school-" 
                        class="df_link col text-center"><?php echo $category["Name"] ?></a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="credit-container">
                <p class="df_company text-center m-0"><i class="far fa-copyright"></i> Videk 2021, Inc.</p>
            </div>
        </section>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ea1f7a49e6.js" crossorigin="anonymous"></script>
    <?php $this->CallScripts();?>
</body>
</html>