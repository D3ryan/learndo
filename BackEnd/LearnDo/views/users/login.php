<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/login.css"?>">
<title>LearnDo - Login</title>
<main class="df_main-container container-fluid min-vh-100">
    <div class="row mb-lg-3 flex-column align-items-center p-3 p-xxl-5">
        <section class="row mb-4 mb-lg-4 mb-xxl-5 col-12 col-md-6">
            <div class="df_logo-container text-center">
                <img src="<?php echo Template::ROOT_PATH . "views/img/Logo.png" ?>" alt="">
            </div>
        </section>
        <section class="df_login-container row p-3 mb-4 col-12 col-md-6 col-lg-8 col-xl-6 col-xxl-6">
            <div class="col">
                <div class="row mb-4">
                    <div class="col d-none d-lg-flex">
                        <img class="df_circle img-fluid" src="<?php echo Template::ROOT_PATH . "views/img/RojoC.png"?>"
                            alt="">
                    </div>
                    <div class="df_welcome-container col-lg-10 p-0">
                        <h1 class="df_title text-center mb-3">¡Bienvenido de vuelta!</h1>
                        <p class="df_text text-center">Te esperabamos, ¿listo para aprender?</p>
                    </div>
                    <div class="col d-none d-lg-flex justify-content-end">
                        <img class="df_circle img-fluid" src="<?php echo Template::ROOT_PATH . "views/img/RojoC.png"?>"
                            alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex flex-column justify-content-center ps-4">
                        <div class="df_input-container">

                            <form id="LoginForm" action="" method="post">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control df_input" name="email" id="email-login"
                                        placeholder="name@example.com">
                                    <label for="email-login">Email</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control df_input" name="password"
                                        id="password-login" placeholder="password">
                                    <label for="password-login">Contraseña</label>

                                </div>

                                <div class="mensaje">
                                    <?php if(isset($getS)): ?>
                                        <h6 class="error">¡Oh no! Al parecer los datos ingresados son incorrectos. Intentalo otra vez</h6>                                        
                                        <?php endif; ?>
                                </div>

                                <div class="df_button-container">
                                    <div class="df_btn-buy row justify-content-center">
                                        <button type="submit" id="btn-send"
                                            class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-lg-4">Entrar</button>
                                    </div>
                                </div>
                            </form>
                            <div class="df_signup-container">
                                <p class="df_text text-center">¿Aún no tienes una cuenta?</p>
                                <a href="<?php echo Template::Route(UsersController::ROUTE, UsersController::INDEX); ?>"
                                    class="df_link text-center d-block">¡Registrate!</a>
                            </div>
                        </div>
                    </div>
                    <div class="col d-none d-lg-flex justify-content-center">
                        <img class="img-fluid" src="<?php echo Template::ROOT_PATH . "views/img/Login_img.jpg"?>"
                            alt="">
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php
function Scripts(){
    include "views/users/scripts/login.scripts.php";
}
?>