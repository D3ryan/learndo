<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/signup.css"?>">
<title>Registro - LearnDo!</title>

<main class="df_main-container container-fluid min-vh-100">
    <div class="row flex-column align-items-center mb-5 p-3">
        <section class="row mb-3 mb-lg-4 mb-xxl-5 col-12 col-md-6">
            <div class="df_logo-container text-center">
                <img src="<?php echo Template::ROOT_PATH . "views/img/Logo.png"?>" alt="logo">
            </div>
        </section>
        <section class="df_signup-container p-3 my-3 col-12 col-md-8 col-lg-5 col-xxl-3 row">
            <div class="df_circle-container text-center">
                <img class="df_circle-signup" src="<?php echo Template::ROOT_PATH . "views/img/RojoC.png"?>" alt="">
            </div>
            <div class="df_title-container mt-2">
                <div class="df_welcome-container col-lg-10 p-0 mx-auto">
                    <h1 class="df_title text-center mb-3">Unete a nuestra comunidad</h1>
                    <p class="df_text text-center">Tu camino al aprendizaje en un click</p>
                </div>
            </div>
            <form id="SignupForm" action="<?php echo Template::ROOT_PATH . "users/signup"?>" method="post" enctype="multipart/form-data">
                <div class="df_user-img my-4">
                    <div class="df_img-container" id="df_img-container">
                        <img src="https://definicionde.es/wp-content/uploads/2019/04/definicion-de-persona-min.jpg"
                            class="df_img-signup" id="df_img" alt="imagen de usuario" srcset="">
                        <div class="df_overlay d-none d-lg-flex justify-content-center align-items-center" id="df_overlay">
                            <label for="formFile" class="form-label df_btn df_file-label p-2">Subir Foto</label>
                            <input class="form-control d-none" type="file" id="formFile" name="avatar">
                        </div>
                    </div>
                    <div class="df_file-button d-lg-none mt-4 row justify-content-center">
                        <label for="formFile" class="form-label df_btn df_file-label text-center col-5 col-md-3 p-2">Subir Foto</label>
                    </div>
                </div>
                <div class="df_input-container">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control df_input" id="inputName" name="names" placeholder="name@example.com">
                        <label for="inputName">Nombre(s)</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input type="text" class="form-control df_input" id="inputFirstSurname" name="firstsurname" placeholder="name@example.com">
                        <label for="inputFirstSurname">Apellido Paterno</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input type="text" class="form-control df_input" id="inputSecondSurname" name="secondsurname" placeholder="name@example.com">
                        <label for="inputSecondSurname">Apellido Materno</label>
                    </div>

                    <div class="form-floating mb-3">
                        <select class="form-select df_select" id="inputGender" name="gender" aria-label="floating-label">
                            <option value="1" selected>Masculino</option>
                            <option value="2">Femenino</option>
                            <option value="3">Otro</option>
                        </select>
                        <label for="inputGender">Género</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input type="date" class="form-control df_input" id="inputBirthdate" name="birthdate" placeholder="name@example.com">
                        <label for="inputBirthdate">Fecha de Nacimiento</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input type="text" class="form-control df_input" id="inputEmail" name="email" placeholder="name@example.com">
                        <label for="inputEmail">Email</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input type="password" class="form-control df_input" id="inputPassword" name="password" placeholder="name@example.com">
                        <label for="inputPassword">Contraseña</label>
                    </div>
                </div> 
                <div class="df_button-container">
                    <div class="df_btn-buy row justify-content-center">
                        <button type="submit" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-4 col-lg-5">Crear Cuenta</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</main>
<?php
function Scripts(){
    include "views/users/scripts/signup.scripts.php";
}
?>