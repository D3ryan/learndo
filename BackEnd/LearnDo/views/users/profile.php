<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/profile.css"?>">
<title>Perfil - LearnDo!</title>
<main class="df_main-container container-fluid min-vh-100">
    <div class="row justify-content-center p-3">
        <div class="df_tabs-container col-12 col-md-8 col-lg-5 col-xxl-3 mb-5 mt-3 p-0">
            <nav class="p-0">
                <div class="df_nav-tabs nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="df_nav-link nav-link active col-6" id="df_student-tab" data-bs-toggle="tab" data-bs-target="#nav-student"
                        type="button" role="tab" aria-controls="nav-student" aria-selected="true">Estudiante</button>
                    <button class="df_nav-link nav-link col-6" id="df_school-tab" data-bs-toggle="tab" data-bs-target="#nav-school"
                        type="button" role="tab" aria-controls="nav-school" aria-selected="false">Escuela</button>
                </div>
            </nav>
            <div class="tab-content p-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-student" role="tabpanel" aria-labelledby="df_student-tab">
                    <div class="df_profile-container p-4">
                        <h1 class="df_title text-center">Perfil de estudiante</h1>
                        <form id="ProfileForm" method="post" enctype="multipart/form-data">
                            <div class="df_user-img mb-4">
                                <div class="df_img-container" id="df_img-container">
                                    <img src="data:;base64,<?php echo base64_encode($user->getAvatar()); ?>"
                                        class="df_img-profile" id="df_img" alt="imagen de usuario" srcset="">
                                    <div class="df_overlay d-none d-lg-flex justify-content-center align-items-center">
                                        <label for="formFile" class="form-label df_btn df_file-label p-2">Subir Foto</label>
                                        <input class="form-control d-none" type="file" id="formFile" name="avatar">
                                    </div>
                                </div>
                                <div class="df_file-button d-lg-none mt-4 row justify-content-center">
                                    <label for="formFile" class="form-label df_btn df_file-label text-center col-5 col-md-3 p-2">Subir Foto</label>
                                </div>
                            </div>
                            <div class="df_input-container">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control df_input" id="inputName" name="names" placeholder="name@example.com" value="<?php echo $user->getNames();?>">
                                    <label for="inputName">Nombre(s)</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control df_input" id="inputFirstSurname" name="firstsurname" placeholder="name@example.com" value="<?php echo $user->getFirstSurname()?>">
                                    <label for="inputFirstSurname">Apellido Paterno</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control df_input" id="inputSecondSurname" name="secondsurname" placeholder="name@example.com" value="<?php echo $user->getSecondSurname();?>">
                                    <label for="inputSecondSurname">Apellido Materno</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <select class="form-select df_select" id="inputGender" name="gender" aria-label="floating-label">
                                        <?php   switch ($user->getGender()) :
                                                case 1: ?>
                                            <option value="1" selected>Masculino</option>
                                            <option value="2">Femenino</option>
                                            <option value="3">Otro</option>
                                        <?php   break;
                                                case 2: ?>
                                            <option value="1">Masculino</option>
                                            <option value="2" selected>Femenino</option>
                                            <option value="3">Otro</option>
                                        <?php   break;
                                                case 3: ?>
                                            <option value="1">Masculino</option>
                                            <option value="2">Femenino</option>
                                            <option value="3" selected>Otro</option>
                                        <?php   break;
                                                endswitch; ?>
                                    </select>
                                    <label for="inputGender">Género</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="date" class="form-control df_input" id="inputBirthdate" name="birthdate" placeholder="name@example.com" value="<?php echo $user->getBirthdate();?>">
                                    <label for="inputBirthdate">Fecha de Nacimiento</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control df_input" id="inputEmail" name="email" placeholder="name@example.com" value="<?php echo $user->getEmail();?>">
                                    <label for="inputEmail">Email</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control df_input" id="inputPassword" name="password" placeholder="name@example.com" value="<?php echo $user->getPassword();?>">
                                    <label for="inputPassword">Contraseña</label>
                                </div>
                            </div>
                            <div class="df_button-container">
                                <div class="df_btn-buy row justify-content-center">
                                    <button type="submit" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-lg-5">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-school" role="tabpanel" aria-labelledby="df_school-tab">
                    <div class="df_profile-container p-4">
                        <h1 class="df_title text-center">Perfil de escuela</h1>
                        <form id="SchoolForm" action="<?php echo Template::Route(UsersController::ROUTE, UsersController::SCHOOL)?>" method="post" enctype="multipart/form-data">
                            <?php if (isset($school)): ?>

                                <div class="df_user-img mb-4">
                                    <div class="df_img-container" id="df_img-container-school">
                                            <img src="data:;base64,<?php echo base64_encode($school->getAvatarSchool()); ?>"
                                                class="df_img-profile" id="df_school-img" alt="imagen de escuela" srcset="">
                                            <div class="df_overlay d-none d-lg-flex justify-content-center align-items-center">
                                                <label for="formFileSchool" class="form-label df_btn df_file-label p-2">Subir Foto</label>
                                                <input class="form-control d-none" type="file" id="formFileSchool" name="avatar">
                                            </div>
                                    </div>
                                    <div class="df_file-button d-lg-none mt-4 row justify-content-center">
                                        <label for="formFileSchool" class="form-label df_btn df_file-label text-center col-5 col-md-3 p-2">Subir Foto</label>
                                    </div>
                                </div>
                                <div class="df_input-container">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control df_input" id="schoolName" name="schoolname" placeholder="SchoolName" value="<?php echo $school->getNameSchool(); ?>">
                                        <label for="schoolName">Nombre de la escuela</label>
                                    </div>
                                <div class="df_button-container">
                                    <div class="df_btn-buy row justify-content-center">
                                        <button type="submit" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-lg-5">Guardar</button>
                                    </div>
                                </div>

                            <?php else: ?>
                                
                                <div class="df_user-img mb-4">
                                    <div class="df_img-container" id="df_img-container-school">
                                            <img src="data:;base64,<?php echo base64_encode($user->getAvatar());?>"
                                                class="df_img-profile" id="df_school-img" alt="imagen de escuela" srcset="">
                                            <div class="df_overlay d-none d-lg-flex justify-content-center align-items-center">
                                                <label for="formFileSchool" class="form-label df_btn df_file-label p-2">Subir Foto</label>
                                                <input class="form-control d-none" type="file" id="formFileSchool" name="avatar">
                                            </div>
                                    </div>
                                    <div class="df_file-button d-lg-none mt-4 row justify-content-center">
                                        <label for="formFileSchool" class="form-label df_btn df_file-label text-center col-5 col-md-3 p-2">Subir Foto</label>
                                    </div>
                                </div>
                                <div class="df_input-container">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control df_input" id="schoolName" name="schoolname" placeholder="SchoolName">
                                        <label for="schoolName">Nombre de la escuela</label>
                                    </div>
                                <div class="df_button-container">
                                    <div class="df_btn-buy row justify-content-center">
                                        <button type="submit" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-lg-5">Guardar</button>
                                    </div>
                                </div>

                            <?php endif ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
function Scripts(){
    include "views/users/scripts/profile.scripts.php";
}
?>