<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/createcourses.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/createlevels.css"?>">
<title>Create Courses Level - LearnDo!</title>
<main>
    <div class="container-fluid create min-vh-100 p-5">
        <form id="createLevelForm" method="POST" enctype="multipart/form-data">
            <h3 class="create__h3 text-center">¡Crea un curso de forma rapida y facil!</h3>
            <div class="create__folder">
                <div class="row justify-content-center create__folder-row">
                    <div class="col-sm-1 create__folder-row-colC">
                        <a href="#">
                            <div class="create__folder-row-colC-lightBlue text-center"> Curso </div>
                        </a>
                        <a href="#">
                            <div class="create__folder-row-colC-white text-center"> Niveles </div>
                        </a>
                    </div>
                    <div class="col-sm-11 create__folder-row-colInfo" id="levelcontainer">
                        <div class=" create__folder-row-colInfo-level-h5D">
                            <h5 class="create__folder-row-colInfo-level-h5D-h5 text-center">Creacion de Niveles</h5>
                        </div>
                        <div class="Level" id="Level">
                            <div class="create__folder-row-colInfo-level-h6D">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control df_level-title df_input" name="leveltitle[0]"
                                        id="df_level-title" placeholder="titulo del nivel">
                                    <label for="df_level-title">Titulo del Curso</label>
                                </div>
                            </div>
                            <div class="create__folder-row-colInfo-level-DA">
                                <div class="row create__folder-row-colInfo-level-DA-row">
                                    <div class="col-sm-7 create__folder-row-colInfo-level-DA-row-colD mb-3">
                                        <div class="form-floating mb-4 h-100">
                                            <textarea class="form-control df_textarea-levels df_textarea"
                                                placeholder="Aqui va la descripcion" id="df_textarea-levels"
                                                name="leveldescription[0]"></textarea>
                                            <label for="df_textarea-levels">Descripción del nivel</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 create__folder-row-colInfo-level-DA-row-colA">
                                        <div class="create__folder-row-colInfo-level-DA-row-colAL">
                                            <div class="form-floating mb-4 h-100">
                                                <textarea class="form-control df_textarea df_textarea-links"
                                                    placeholder="Aqui va la descripcion" id="df_textarea-links"
                                                    name="levellinks[0]"></textarea>
                                                <label for="df_textarea-links">Links del nivel</label>
                                            </div>
                                        </div>
                                        <div class="create__folder-row-colInfo-level-DA-row-colAA">
                                            <h6 class="create__folder-row-colInfo-level-DA-row-colAA-h6">Imagenes:
                                            </h6>
                                            <label for="archivos" class="label__archivos"></label>
                                            <input type="file" id="archivos" name="multimediafiles[0][]" class="shadow-none archivos form-control form-control-sm 
                                            create__folder-row-colInfo-level-DA-row-colAA-file" multiple>
                                        </div>
                                        <div class="create__folder-row-colInfo-level-DA-row-colAA">
                                            <h6 class="create__folder-row-colInfo-level-DA-row-colAA-h6">Archivos PDF:
                                            </h6>
                                            <label for="pdf" class="label__archivos"></label>
                                            <input type="file" id="pdf" name="pdffiles[0][]" class="shadow-none pdf form-control form-control-sm 
                                            create__folder-row-colInfo-level-DA-row-colAA-file" multiple>
                                        </div>
                                        <div class="create__folder-row-colInfo-level-DA-row-colAV">
                                            <h6 class="create__folder-row-colInfo-level-DA-row-colAV-h6">Video:</h6>
                                            <input type="file" id="video" name="videolevel[0]" class="video shadow-none form-control form-control-sm 
                                            create__folder-row-colInfo-level-DA-row-colAV-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="create__folder-row-colInfo-level-CA">
                                <div class="create__folder-row-colInfo-level-CAC df_price-container" id="df_price-container">
                                    <label for="price"
                                        class="create__folder-row-colInfo-colCampos-row-colDts-costo-h6">Costo
                                        total: $</label>
                                    <input type="number" class="form-control form-control-sm df_input
                                    create__folder-row-colInfo-colCampos-row-colDts-costo-btn price" id="price"
                                        name="price[0]">
                                    <span
                                        class="ms-2 create__folder-row-colInfo-colCampos-row-colDts-costo-h6">MXN</span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-evenly mt-5 create__folder-row-colInfo-level-CAA">
                <button id="btn-eraselevel" class="btn d-none shadow-none create__folder-row-colInfo-level-CAA-btn">
                    <img src="<?php echo Template::ROOT_PATH . "views/img/eliminar.png" ?>" class="create__folder-row-colInfo-level-CAA-btn-img">
                    <h6 class="create__folder-row-colInfo-level-CAA-h6">Eliminar nivel</h6>
                </button>

                <button id="btn-newlevel" class="btn shadow-none create__folder-row-colInfo-level-CAA-btn">
                    <img src="<?php echo Template::ROOT_PATH . "views/img/agregar.png"?>" class="create__folder-row-colInfo-level-CAA-btn-img">
                    <h6 class="create__folder-row-colInfo-level-CAA-h6">Agrega otro nivel</h6>
                </button>

            </div>
            <div class="df_btn-buy row justify-content-end me-lg-3 me-xxl-5">
                <button type="submit" class="df_btn btn my-4 col-12 col-md-3 col-lg-2 col-xxl-1">
                    Subir Curso
                </button>
            </div>
        </form>
    </div>
</main>
<?php 
function Scripts(){
    include "views/levels/scripts/createlevels.scripts.php";
}
?>
