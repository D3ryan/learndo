<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/CoursePresentation.css"?>">
<title>LearnDo - Curso</title>

<main class="df_main-container container-fluid min-vh-100">
    <div class="row justify-content-md-center justify-content-lg-start justify-content-xl-center p-4">
        <section class="col-12 col-md-10 col-lg-8 col-xl-5 p-0">
            <div class="df_course-container mb-3 p-4">
                <div class="row">
                    <h1 class="df_title"><?php echo $course->getTitle(); ?></h1>
                </div>
                <div class="row">
                    <p class="df_caption col-lg-8">Por <?php echo $course->getSchoolName(); ?></p>
                    <p class="df_caption col-lg-4"><?php echo $course->getCreationDate(); ?></p>
                </div>
                <div class="row justify-content-center mb-4">
                    <?php foreach($categories as $category) :?>
                        <div class="col row g-1">
                            <div class="col">
                                <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SEARCH_COURSE) ?>/title-/startdate-/endDate-/category-<?php echo $category["IdCategory"] ?>/school-"
                                 class="df_course-category df_link d-block text-center p-1"><?php echo $category["Name"] ?></a>
                            </div>
                        </div>
                    <?php endforeach?>
                </div>
                <div class="row mb-4">
                    <img class="df_img p-0 img-fluid" src="data:;base64,<?php echo base64_encode($course->getImage()); ?>" alt="">
                </div>
                <div class="df_text row">
                    <p><?php echo $course->getDescription(); ?></p>
                </div>
                <div class="df_price-container row">
                    <div class="df_price-details">
                        <p class="df_total-price">Precio: $<?php echo $course->getTotalprice(); ?> MXN</p>
                        <p class="df_cheaper-price">o desde: $<?php echo $cheapestLevel->getPrice(); ?> MXN</p>
                    </div>
                    <div class="df_course-details">
                        <p class="df_suscribed-users"><?php echo $course->getStudents();?> Alumnos inscritos</p>
                        <p class="df_general-rating text-center text-lg-start d-lg-inline-block p-2 px-lg-3">Valoracion: <?php echo $course->getRating();?>/10</p>
                    </div>             
                    <div class="df_button-container">
                        <div class="df_btn-buy row justify-content-center">
                            <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SHOPPING_CART) . "/" . $course->getIdCourse(); ?>" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-xxl-2">Comprar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="df_accordion-container">
                    <h2 class="bg-light df_subject m-0 p-3">Temario</h2>
                    <div class="df_accordion accordion accordion-flush" id="accordionFlushExample">
                        <?php foreach($levels as $key => $value) :?>
                            <div class="df_accordion-item accordion-item">
                                <h2 class="accordion-header" id="flush-heading<?php echo $key ?>">
                                    <button class="df_accordion-button accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#flush-collapse<?php echo $key ?>" aria-expanded="false"
                                        aria-controls="flush-collapse<?php echo $key ?>">
                                        Nivel <?php echo $key + 1 . " : " . $levels[$key]["title"]?>
                                    </button>
                                </h2>
                                <div id="flush-collapse<?php echo $key ?>" class="accordion-collapse collapse"
                                    aria-labelledby="flush-heading<?php echo $key ?>" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body"><?php echo $levels[$key]["textlevel"]?></div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="row p-3 justify-content-center">
                <p class="df_subtitle text-center text-md-start">Comentarios de Estudiantes</p>
                <?php if(isset($userCanComment) && intval($userCanComment[0])) :?>
                    <div class="df_commentary-card row p-3 mb-3">
                        <div class="df_img-container col-lg-2 mb-3 pe-lg-4 p-0 m-lg-0">
                            <img class="df_user-img" src="data:;base64,<?php echo base64_encode($user->getAvatar()); ?>" alt="">
                        </div>
                        <div class="df_commentary-section col-lg-10 p-0">
                            <form id="CoursePresentationForm" method="post" action="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::COMMENT_COURSE). "/" . $course->getIdCourse(); ?>"?>
                                <div class="df_commentary-details mb-2">
                                    <p class="df_text"><?php echo $user->getNames() . " " . $user->getFirstSurname() . " " . $user->getSecondSurname()?> </p>
                                    <div class="form-floating mb-3">
                                    <?php if (!(empty($userComment))): ?>
                                        <input type="number" class="form-control df_input" name="rating" id="rating-login" placeholder="name@example.com" value="<?php echo $userComment[0]["Rating"];?>">
                                        <label for="rating-login">Valoracion</label>
                                        <?php else :?>
                                            <input type="number" class="form-control df_input" name="rating" id="rating-login" placeholder="name@example.com">
                                        <label for="rating-login">Valoracion</label>
                                        <?php endif?>
                                    </div>
                                </div>
                                <div class="form-floating mb-2">
                                <?php if (!(empty($userComment))): ?>
                                    <textarea class="form-control df_textarea" placeholder="Aqui va tu comentario" id="df_textarea" name="commentary" ><?php echo $userComment[0]["TextCommentary"];?></textarea>
                                    <label for="df_textarea">Comentario</label>

                                    <?php else :?>
                                    <textarea class="form-control df_textarea" placeholder="Aqui va tu comentario" id="df_textarea" name="commentary" ></textarea>
                                    <label for="df_textarea">Comentario</label>

                                    <?php endif?>
                                </div>
                                <div class="df_button-container">
                                    <div class="df_btn-buy row justify-content-evenly p-3">
                                        <button type="submit" name="btn_comentar" id="btn-comment" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-12 col-md-3 col-lg-4">Comentar</button>
                                        <button type="submit" name="btn_delete" id="btn-erase" class="df_btn df_btn-erase btn py-2 my-4 px-lg-3 py-lg-2 col-12 col-md-3 col-lg-4">Eliminar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php endif?>
                <?php foreach($commentaries as $commentary) :?>
                    <div class="df_commentary-card row p-3 mb-3">
                        <div class="df_img-container col-lg-2 mb-3 pe-lg-4 p-0 m-lg-0">
                            <img class="df_user-img" src="data:;base64,<?php echo base64_encode($commentary["Avatar"]) ?>" alt="">
                        </div>
                        <div class="df_commentary-section col-lg-10 p-0">
                            <p class="df_text"><?php echo $commentary["Names"] . " " . $commentary["FirstSurname"] . " " . $commentary["SecondSurname"]?></p>
                            <div class="df_commentary-details mb-2">
                                <div class="mb-3">
                                    <span class="df_text df_rating p-1">Valoracion: <?php echo $commentary["Rating"]?>/10</span>
                                </div>
                                <div class="df_caption">
                                    <span class="df_date"><?php echo $commentary["DateCommentary"]?></span>
                                </div>
                            </div>
                            <p class="df_commentary">" <?php echo $commentary["TextCommentary"] ?> "
                            </p>
                        </div>
                    </div>
                <?php endforeach ?>
        </section>
        <section class="col-12 col-lg-4 col-xl-3 d-none d-lg-block ps-5">
            <div class="df_price-container-sticky row p-3">
                <div class="df_price-details">
                    <p class="df_total-price-sticky">Precio: $<?php echo $course->getTotalprice(); ?> MXN</p>
                    <p class="df_cheaper-price">o desde: $<?php echo $cheapestLevel->getPrice(); ?> MXN</p>
                </div>
                <div class="df_course-details">
                    <p class="df_suscribed-users"><?php echo $course->getStudents();?> Alumnos inscritos</p>
                    <p class="df_general-rating text-center text-lg-start d-lg-inline-block p-2 px-lg-3">Valoracion: <?php echo $course->getRating();?>/10</p>
                </div>             
                <div class="df_button-container">
                    <div class="df_btn-buy row justify-content-center">
                        <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::SHOPPING_CART) . "/" . $course->getIdCourse(); ?>" 
                        class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-lg-5">Comprar</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php
function Scripts(){
    include "views/course/scripts/index.scripts.php";
}
?>   



