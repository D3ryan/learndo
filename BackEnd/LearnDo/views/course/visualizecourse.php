<!DOCTYPE html>
<html lang="en">

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="../IMG/libro.ico" type="image/x-icon">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweet-modal@1.3.2/dist/min/jquery.sweet-modal.min.css">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/visualizecourse.css"?>">
<title>LearnDo - Curso</title>

<main class="df_main-container container-fluid min-vh-100">
    <div class="row min-vh-100">
        <section class="df_visualize-course col-lg-9 col-xxl-10">
            <div class="df_video-container row">
                <video class="df_video p-0" controls id="df_video">
                    <source src="<?php echo $visualizedLevel["UrlVideo"]?>" type="video/mp4">
                    <source src="<?php echo $visualizedLevel["UrlVideo"]?>" type="video/x-msvideo">
                    <source src="<?php echo $visualizedLevel["UrlVideo"]?>" type="video/quicktime">
                </video>
            </div>
            <div class="row df_course-content d-block d-lg-none mb-3">
                <div class="p-3">
                    <button type="button" class="df_content-title d-block text-start w-100 btn p-0 m-0"
                    data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false"
                    aria-controls="collapseExample">Contenido del curso</button>
                    <div class="df_wrapper mt-2 collapse" style="height: 3000px;" id="collapseExample">
                    <?php foreach($levels as $key => $value) :?>
                        <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::VISUALIZE_COURSE) . "/" . $levels[$key]["IdCourse"] . "/" . $levels[$key]["IdLevel"] ?>" 
                            class="df_link-level d-block p-3" repeatable>
                            <p class="df_level m-0">Nivel : <?php echo $levels[$key]["Title"]?></p>
                        </a>
                    <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center px-4 pt-4">
                <input type="text" value="<?php echo $visualizedLevel["IdLevel"]?>" id="IdVisualizedLevel" hidden> 
                <div class="col-lg-10 df_titleDesc-container p-4 mb-4">
                    <h1 class="df_title text-center"><?php echo $visualizedLevel["Title"]?></h1>
                    <div class="df_description-container df_text mb-2">
                        <p class="text-center"><?php echo $visualizedLevel["TextLevel"]?></p>
                        <p class="df_caption text-center m-0">Ultima Visita: <?php echo $visualizedLevel["LastVisitDate"]?></p>
                        <?php if(boolval($visualizedLevel["Progress"])) :?>
                            <p id="status" class="df_caption text-center">Estatus: Terminado</p>
                        <?php else :?>
                            <p id="status" class="df_caption text-center">Estatus: Sin Empezar</p>
                        <?php endif ?>

                    </div>
                </div>
                <div class="df_link-container col-lg-10 p-4 mb-4">
                    <h2 class="text-center">Enlaces externos</h2>
                    <div class="df_link-wrapper text-center">
                    <?php foreach($levelLinks as $link) :?>
                        <a href="http://<?php echo $link ?>" class="df_link"><?php echo $link ?></a>
                    <?php endforeach ?>
                    </div>
                </div>
                <div class="df_file-container col-lg-10 p-4 mb-4">
                    <h2 class="text-center">Archivos</h2>
                    <?php foreach($pdfFiles as $pdf) :?>
                        <a href="<?php echo $pdf["UrlFile"] ?>" class="df_link df_file d-block mb-2" download>
                        <i class="fas fa-file-pdf df_icon"></i> <?php echo $pdf["NameFile"]?></a>
                    <?php endforeach ?>
                </div>
                <div class="df_file-container col-lg-10 p-4 mb-4">
                    <h2 class="text-center">Imágenes</h2>
                    <?php foreach($images as $image) :?>
                        <img class="df_img-course " src="data:;base64,<?php echo base64_encode($image["Image"]); ?>">
                    <?php endforeach ?>
                </div>
            </div>
            <div class="df_btn-buy d-flex justify-content-center my-5">
                <button type="button" class="df_btn btn px-lg-3 py-lg-2" id="finishlevel">Terminar Nivel</button>
            </div>
        </section>
        <section class="df_course-content col-lg-3 col-xxl-2 d-none d-lg-block p-0">
            <p class="df_content-title p-3 m-0">Contenido del curso</p>
                <?php foreach($levels as $key => $value) :?>
                    <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::VISUALIZE_COURSE) . "/" . $levels[$key]["IdCourse"] . "/" . $levels[$key]["IdLevel"] ?>" 
                        class="df_link-level d-block p-3" repeatable>
                        <p class="df_level m-0">Nivel : <?php echo $levels[$key]["Title"]?></p>
                    </a>
                <?php endforeach ?>
                <?php if ($finish["HasCompletedCourse"] == 1):?>
                    <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::DOWNLOAD) . "/" . $value["IdCourse"] ?>" 
                    class="df_link-levelBtn p-3 d-flex justify-content-center" >
                    <button class="df_btn btn px-lg-3 py-lg-2"> Diploma </button>
                    </a>
                    <?php endif ?>
        </section>
    </div>
</main>
<?php function Scripts(){
    include "views/course/scripts/visualizecourse.scripts.php";
}
?>