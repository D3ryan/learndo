<!DOCTYPE html>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/searchcourses.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/mycourses.css"?>">
<title>My Courses - LearnDo!</title>

<main>
    <div class="container-fluid resultados min-vh-100">
        <h2 class="resultados__h3"> Mis Cursos </h2>
        <div class="resultados__cursos">
            <h6 class="resultados__cursos-h4">Es hora de seguir aprendiendo, selecciona un curso y terminalo o ¡empieza uno nuevo!</h6>
        </div>
        <div class="row resultados__cursos-row">
            <?php foreach($courses as $course) :?>
                <div class="col-sm-3 resultados__cursos-row-col">
                    <div class="card  resultados__cursos-row-col-card">
                        <div class="resultados__cursos-row-col-card-img-opacity">
                            <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::VISUALIZE_COURSE) . "/" . $course["IdCourse"] ?>">    
                                <img class="resultados__cursos-row-col-card-img" src="data:;base64,<?php echo base64_encode($course["Image"]); ?>">
                                <div class="middle"> 
                                    <div class="text">Ver Curso</div>
                                </div>
                            </a>
                        </div>
                        <div class="progress">
                            <div class="progress-bar resultados__cursos-row-col-card-progress"
                                role="progressbar" style="width: <?php echo $course["Progress"]; ?>%" aria-valuenow="75" aria-valuemin="0"
                                aria-valuemax="100"></div>
                        </div>
                        <div class="card-body resultados__cursos-row-col-card-body">
                            <h6 class="resultados__cursos-row-col-card-body-Titulo"><?php echo $course["Title"]; ?></h6>
                            <h6 class="resultados__cursos-row-col-card-body-NameA">Por <?php echo $course["SchoolName"]; ?></h6>
                            <h6 class="resultados__cursos-row-col-card-body-NameA">Registrado el: <?php echo $course["RegistrationDate"]; ?></h6>
                            <?php if(isset($course["FinishDate"])) : ?>
                                <h6 class="resultados__cursos-row-col-card-body-NameA">Terminado el: <?php echo $course["FinishDate"]; ?></h6>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</main>
