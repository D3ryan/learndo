<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/tokenize2.min.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/createcourses.css"?>">
<title>Create Courses - LearnDo!</title>
<main>
    <div class="container-fluid create p-5 min-vh-100">
        <form id="editCourseForm" method="POST" enctype="multipart/form-data">
            <h3 class="create__h3 text-center">¡Crea un curso de forma rapida y facil!</h3>
            <div class="create__folder">
                <div class="row create__folder-row justify-content-center">
                    <div class="col-sm-1 create__folder-row-colC">
                        <a href="#">
                            <div class="create__folder-row-colC-white text-center"> Curso </div>
                        </a>
                        <div class="create__folder-row-colC-lightBlue text-center">
                            <button type="submit" class="btn p-0 shadow-none" form="editCourseForm">Niveles</button>
                        </div>
                    </div>
                    <div class="col-sm-11 create__folder-row-colInfo">
                        <div class="create__folder-row-colInfo-h5D">
                            <h5 class="create__folder-row-colInfo-h5D-h5 text-center">Datos generales del curso</h5>
                        </div>
                        <div class="row create__folder-row-colInfo-colCampos-row">
                            <div class="col-sm-5 create__folder-row-colInfo-colCampos-row-colIC">
                                <div class="px-3">
                                    <div class="df_course-img">
                                        <div class="df_img-container" id="df_img-container">
                                            <img src="data:;base64,<?php echo base64_encode($course->getImage()); ?>" class="df_img-course" id="df_img"
                                                alt="" srcset="">
                                            <div class="df_overlay d-none d-lg-flex justify-content-center align-items-center"
                                                id="df_overlay">
                                                <label for="formFile"
                                                    class="form-label df_btn df_file-label p-2">Subir Foto</label>
                                                <input class="form-control d-none" type="file" id="formFile"
                                                    name="image">
                                            </div>
                                        </div>
                                        <div class="df_file-button d-lg-none mt-4 row justify-content-center">
                                            <label for="formFile"
                                                class="form-label df_btn df_file-label text-center col-5 col-md-3 p-2">Subir
                                                Foto</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="create__folder-row-colInfo-colCampos-row-colIC-ctg p-4">
                                    <label
                                        class="create__folder-row-colInfo-colCampos-row-colIC-ctg-h6 mb-2 d-block">Categorías:</label>
                                    <div id="df_category-container" class="df_category-container mb-3">
                                        <select class="form-select" id="df_create-courses" name="category[]" multiple>
                                            <?php foreach($courseCategories as $category): ?>
                                                <option value="<?php echo $category["IdCategory"] ?>" selected><?php echo $category["Name"] ?></option>
                                            <?php endforeach; ?>
                                            <?php foreach($categories as $category): ?>
                                                <option value="<?php echo $category["IdCategory"] ?>"><?php echo $category["Name"] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <!-- <div class="df_btn-buy row justify-content-end me-1">
                                        <button type="button"
                                            class="df_btn btn my-4 col-12 col-md-9 col-lg-6 col-xxl-3"
                                            data-bs-toggle="modal" data-bs-target="#categoryModal">
                                            Crear categoría
                                        </button>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-sm-7 create__folder-row-colInfo-colCampos-row-colDts">
                                <div class="create__folder-row-colInfo-colCampos-row-colDts-titulo">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control df_input" name="title"
                                            id="df_course-title" placeholder="titulo del curso" value="<?php echo $course->getTitle(); ?>">
                                        <label for="df_course-title">Titulo del Curso</label>
                                    </div>
                                </div>
                                <div class="create__folder-row-colInfo-colCampos-row-colDts-Description">
                                    <div class="form-floating mb-4">
                                        <textarea class="form-control df_textarea"
                                            placeholder="Aqui va la descripcion" id="df_textarea"
                                            name="description"><?php echo $course->getDescription(); ?></textarea>
                                        <label for="df_textarea">Descripción del curso completo</label>
                                    </div>
                                </div>
                                <div id="df_price-container"
                                    class="create__folder-row-colInfo-colCampos-row-colDts-costo">
                                    <label for="totalprice"
                                        class="create__folder-row-colInfo-colCampos-row-colDts-costo-h6">Costo
                                        total: $</label>
                                    <input type="number" class="form-control form-control-sm df_input
                                    create__folder-row-colInfo-colCampos-row-colDts-costo-btn" id="totalprice"
                                        name="totalprice" value="<?php echo $course->getTotalPrice(); ?>">
                                    <span
                                        class="ms-2 create__folder-row-colInfo-colCampos-row-colDts-costo-h6">MXN</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</main>
<!-- <section class="modal-container">
    <div class="modal fade" id="categoryModal" tabindex="-1" aria-labelledby="categoryModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="categoryModalLabel">Crear tu categoria</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo Template::Route(CoursesController::ROUTE,  CoursesController::CREATE_CATEGORY) ?>" method="POST" id="categoryForm">
                        <div class="mb-3">
                            <label for="category-title" class="form-label text-dark fw-bold">Titulo de la
                                categoria:</label>
                            <input type="text" class="form-control" id="category-title" name="category-title"
                                placeholder="Titulo">
                        </div>
                        <div class="mb-3">
                            <label for="category-description" class="form-label text-dark fw-bold">Descripcion de la
                                categoria:</label>
                            <input type="text" class="form-control" id="category-description"
                                name="category-description" placeholder="Descripcion">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="submit" form="categoryForm" class="btn btn-primary">Crear</button>
                </div>
            </div>
        </div>
    </div>
</section> -->
<?php
function Scripts(){
    include "views/course/scripts/create.scripts.php";
}
?>   