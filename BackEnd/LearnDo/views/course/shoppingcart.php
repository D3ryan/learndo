<!DOCTYPE html>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../IMG/libro.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweet-modal@1.3.2/dist/min/jquery.sweet-modal.min.css">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/error.css"?>">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/card.css"?>">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/shoppingcart.css"?>">
    <title>LearnDo - Carrito</title>
<main class="df_main-container container-fluid border min-vh-100">
    <section class="df_title-container p-3 px-lg-5 pt-lg-4">
        <i class="fas fa-shopping-cart df_icon me-2"></i>
        <h1 class="df_title d-inline-block m-0">Carrito</h1>
    </section>
    <div class="df_shoppingCart-container sticky-top row p-3 px-lg-5">
        <section class="df_course-container col-12 col-md-4 p-3 mb-5 mb-md-0">
            <img src="data:;base64,<?php echo base64_encode($course->getImage());?>"
                class="df_img mb-4" alt="Imagen del curso">
            <div class="df_courseDetails mb-3">
                <p class="df_title m-0"><?php echo $course->getTitle(); ?></p>
                <span class="df_caption df_creator">Por <?php echo $course->getSchoolName(); ?></span>
            </div>
            <p class="df_caption">"<?php echo $course->getDescription(); ?>"</p>
        </section>
        <section class="df_shoppingCart col-12 col-md-8 ps-md-3 p-0">
            <form id="ShoppingForm" method="post">
                <div class="df_shopping-wrapper" id="df_shopping-wrapper">
                    <div class="df_coursePrice-container p-2">
                        <div class="df_course-row d-flex justify-content-between">
                        <?php if(!boolval($hasTheCourse[0])) :?>
                            <?php if(!boolval($userIsRegistered[0])) :?>
                                <div class="df_input-course ms-2">
                                        <input class="form-check-input validationgroup df_checkbox me-2" name="course" type="checkbox" levelprice="<?php echo $course->getTotalprice(); ?>" value="<?php echo $course->getIdCourse(); ?>" id="df_completeCourse">
                                        <input type="text" id="courseprice" name="courseprice" value="<?php echo $course->getTotalprice(); ?>" hidden>

                                    <label class="form-check-label df_text df_checkbox-label d-inline df_overflow-wrap" for="df_completeCourse">
                                        Curso Completo
                                    </label>
                                </div>
                                <span class="df_price df_text df_price flex-shrink-0 mx-2">$<?php echo $course->getTotalprice(); ?> MXN</span>
                                
                            <?php else : ?>
                                <input class="d-none" name="course" type="checkbox" value="<?php echo $course->getIdCourse(); ?>" id="df_completeCourse">
                                <input type="text" id="courseprice" name="courseprice" value="<?php echo $course->getTotalprice(); ?>" hidden>
                                <span class="df_price df_text df_price flex-shrink-0 mx-2">
                                    ¿Qué nuevo nivel deseas comprar?
                                </span>
                            <?php endif ?>

                        <?php else : ?>
                                <span class="df_price df_text df_price flex-shrink-0 mx-2">
                                    ¡Creo que ya has comprado todo el curso!, en cuanto el creador agregue nuevos niveles, te serán añadidos.
                                </span>
                        <?php endif ?>
                        </div>
                    </div>
                    <div class="df_levels-container">
                        <div class="df_levels-wrapper ps-lg-4">
                            <?php if(!boolval($hasTheCourse[0])) : ?>
                                <?php foreach($levels as $key => $value) :?>
                                    <div class="df_level p-2 d-flex justify-content-between">
                                        <div class="df_input-container ms-2">
                                            <input class="form-check-input validationgroup df_checkbox me-2" name="levels[]" type="checkbox" levelprice="<?php echo $levels[$key]["price"] ?>" value="<?php echo $levels[$key]["IdLevel"] ?>" id="df_levelCheckbox<?php echo $key?>">
                                            <input type="text" name="levelprice[]" value="<?php echo $levels[$key]["price"] ?>" hidden>
                                            <label class="form-check-label df_checkbox-label df_text d-inline df_overflow-wrap" for="df_levelCheckbox">
                                                Nivel: <?php echo $levels[$key]["title"]?>
                                            </label>
                                        </div>
                                        <span class="df_price df_text df_price flex-shrink-0 mx-2">$<?php echo $levels[$key]["price"] ?> MXN</span>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif ?>

                        </div>
                        <div class="df_totalPrice-wrapper">
                            <div class=" df_totalPrice-container p-2 d-flex justify-content-between">
                                <div class="df_input-container ms-2">
                                    <span class="form-check-label df_text">
                                        Total a Pagar
                                    </span>
                                </div>
                                <span class="df_price df_text df_price me-2" id="df_totalPrice">$0 MXN</span>
                                <input id="totalprice" name="totalprice" type="text" value="0" hidden>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="d-flex justify-content-center mt-5 mb-3">
                    <div id="paypal-button-container" class="paypal-button-container w-75"></div>
                </div>
                <div class="df_btn-buy d-flex justify-content-center">
                    <button type="button" class="df_btn df_btn-payment btn px-lg-3 py-lg-2 w-50" id="cardbutton">Pagar con tarjeta debito/credito</button>
                </div>
                <div class="cardpayment-container mt-5" id="cardpayment-container">
                    <div class="card-wrapper"></div>
                    <div class="form-container active">
                        <form id="cardForm" method="post">
                            <div class="d-flex flex-column align-items-center mt-3">
                                <input type="text" class="df_input my-3 form-control w-25" placeholder="Numero de la tarjeta" name="number">
                                <input type="text" class="df_input my-3 form-control w-25" placeholder="Nombre del titular" name="name"/>
                                <input type="text" class="df_input my-3 form-control w-25" placeholder="Expiración" name="expiry"/>
                                <input type="text" class="df_input my-3 form-control w-25" placeholder="CVV" name="cvc"/>
                                <button type="button" id="cardpayment-button" class="df_btn btn px-lg-3 py-lg-2 my-4">Pagar</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
        </section>
    </div>
</main>
<?php function Scripts(){
    include "views/course/scripts/shoppingcart.scripts.php";
}
?>