<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/searchcourses.css"?>">
    <title>Search Courses - LearnDo!</title>

<main>
    <div class="container-fluid resultados min-vh-100" style="font-family: Nunito;">
        <h3 class="resultados__h3"> Busqueda avanzada
        </h3>
        <div class="resultados__forms-filtar">
            <button class="btn shadow-none resultados__forms-filtrar-btn1" id="btnShow">
                <h6 class="resultados__forms-filtrar-btn-h5">Filtrar</h6>
                <img class=" resultados__forms-filtrar-btn-img" src="<?php echo Template::ROOT_PATH . "views/img/calendario (1).png" ?>">
            </button>
        </div>
        <form id="advancedSearch" method="POST">
            <div class="resultados__forms-filtrar-fecha" id="hide">
                <label for="fechaRe">Fecha de inicio: </label>
                <div class="div d-inline-block">
                    <input type="date" id="startDate" name="startDate" class="form-control df_input-busqueda d-inline-block shadow-none">
                </div>

                <label for="fechaRe">Fecha final: </label>
                <div class="div d-inline-block">
                    <input type="date" id="finalDate" name="finalDate" class="form-control df_input-busqueda d-inline-block shadow-none">
                </div>

                <label for="fechaRe">Categorias: </label>
                <div class="div d-inline-block">
                    <select id="category" name="category" class="form-control df_input-busqueda d-inline-block shadow-none" style="width: 150px;">
                        <option value="" selected>Elige una categoría</option>
                        <?php foreach($categories as $category) : ?>
                            <option value="<?php echo $category["IdCategory"] ?>"><?php echo $category["Name"] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                
                <label for="fechaRe">Propietario: </label>
                <div class="div d-inline-block">
                    <input type="text" id="schoolname" name="schoolname" class="form-control df_input-busqueda d-inline-block shadow-none">
                </div>

                <label for="fechaRe">Titulo: </label>
                <div class="div d-inline-block">
                    <input type="text" id="title" name="title" class="form-control df_input-busqueda d-inline-block shadow-none">
                </div>

                <input type="submit" id="FiltroSubmit" value="Buscar" class="btn btn-outline-danger shadow-none">
            </div>
        </form>
        <div class="row resultados__cursos-row">
            <?php foreach($courses as $course) : ?>
                <div class="col-sm-3 resultados__cursos-row-col">
                    <div class="card  resultados__cursos-row-col-card">
                        <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::INDEX) . "/" . $course["IdCourse"] ?>"><img class="resultados__cursos-row-col-card-img" src="data:;base64,<?php echo base64_encode($course["Image"]); ?>"></a>
                        <div class="card-body resultados__cursos-row-col-card-body">
                            <h6 class="resultados__cursos-row-col-card-body-Titulo"><?php echo $course["Title"] ?></h6>
                            <h6 class="resultados__cursos-row-col-card-body-NameA">Por <?php echo $course["SchoolName"]?></h6>
                            <div class="row center_fanther">
                                <div class="col-sm-6 resultados__cursos-row-col-row-col">
                                    <h6 class="resultados__cursos-row-col-card-body-Precio ps-2"> $<?php echo $course["TotalPrice"] ?> MXN</h6>
                                </div>
                                <div class="col-sm-6 resultados__cursos-row-col-row-col2">
                                    <img class="resultados__cursos-row-col-card-body-imgStart" src="<?php echo Template::ROOT_PATH . "views/img/Star.png"?>">
                                    <h6 style="display: inline;"><?php echo $course["Rating"] ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</main>
<?php function Scripts(){
    include "views/course/scripts/searchcourses.scripts.php";
}
?>