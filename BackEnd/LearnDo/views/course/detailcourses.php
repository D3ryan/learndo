<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/detailcourses.css"?>">
    <title>Detail Courses</title>

    <main>
        <div class="container-fluid details min-vh-100">
            <div class="details__divDetails">
                <h3 class="details__divDetails-h3 d-inline-block"><?php echo $course["Title"] ?></h3>
                <?php if(boolval($course["Active"])) : ?>
                    <h3 class="details__divDetails-h3 d-inline-block ms-2">(Activo)</h3>
                <?php else :?>  
                    <h3 class="details__divDetails-h3 d-inline-block ms-2">(Inactivo)</h3>
                <?php endif ?>  
            </div>
            <div class="details__divEstudiante">
                <div class=" row details__divEstudiante-row">
                    <div class="col-sm-1 details__divEstudiante-row-col">
                        <img src="<?php echo Template::ROOT_PATH . "views/img/personas-llenas-de-grupo.png"?>" class="details__divEstudisnte-img">
                    </div>
                    <div class="col-sm-2 details__divEstudiante-row-col-mum">
                        <h6 class="details__divEstudiante-h6"><?php echo $course["Students"] ?> alumnos</h6>
                    </div>
                </div>
            </div>
            
            <div class="details__divLista">
                <div class="details__divLista-h">
                    <h class="details__divLista-h6">Estudiantes que inscritos al curso:</h>
                </div>
                <table class="table details__divLista-table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Estudiantes</th>
                            <th scope="col">Progreso</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Inscripcion</th>
                        </tr>
                    </thead>
                    <tbody class="details__divLista-table-tbody ">
                        <?php if(isset($registeredUsers)) : ?>
                            <?php foreach($registeredUsers as $user) : ?>
                                <tr>
                                    <th scope="row" class="scope__r">
                                        <img src="data:;base64, <?php echo base64_encode($user["Avatar"]) ?>" class="details__divLista-table-tbody-img">
                                    </th>
                                    <td><?php echo $user["Names"] . " " . $user["FirstSurname"] . " " . $user["SecondSurname"]?></td>
                                    <td><?php echo $user["Progress"] ?>%</td>
                                    <td>$<?php echo $user["TotalPayments"]?> MXN</td>
                                    <td><?php echo $user["RegistrationDate"]?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                        </tbody>
                </table>
            </div>

            <div class="details__divLista">
                <div class="details__divLista-h">
                    <h class="details__divLista-h6">Estudiantes que compraron el curso completo :</h>
                </div>
                <table class="table details__divLista-table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Estudiantes</th>
                            <th scope="col">Progreso</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Pago</th>
                            <th scope="col">Inscripcion</th>
                        </tr>
                    </thead>
                    <tbody class="details__divLista-table-tbody ">
                        <?php if(isset($usersbycourses)) : ?>
                            <?php foreach($usersbycourses as $user) : ?>
                                <tr>
                                    <th scope="row" class="scope__r">
                                        <img src="data:;base64, <?php echo base64_encode($user["Avatar"]) ?>" class="details__divLista-table-tbody-img">
                                    </th>
                                    <td><?php echo $user["Names"] . " " . $user["FirstSurname"] . " " . $user["SecondSurname"]?></td>
                                    <td><?php echo $user["Progress"]?>%</td>
                                    <td>$<?php echo $user["PurchasePrice"]?> MXN</td>
                                    <td><?php echo $user["PaymentMethod"] ?></td>
                                    <td><?php echo $user["RegistrationDate"]?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>

            <div class="details__divLista">
                <div class="details__divLista-h">
                    <h class="details__divLista-h6">Estudiantes que compraron niveles del curso:</h>
                </div>
                <table class="table details__divLista-table">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Estudiantes</th>
                            <th scope="col">Nivel</th>
                            <th scope="col">Progreso</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Pago</th>
                            <th scope="col">Inscripcion</th>
                        </tr>
                    </thead>
                    <tbody class="details__divLista-table-tbody ">
                        <?php if(isset($usersbylevels)) : ?>
                            <?php foreach($usersbylevels as $user) : ?>
                                <tr>
                                    <th scope="row" class="scope__r">
                                        <img src="data:;base64, <?php echo base64_encode($user["Avatar"]) ?>" class="details__divLista-table-tbody-img">
                                    </th>
                                    <td><?php echo $user["Names"] . " " . $user["FirstSurname"] . " " . $user["SecondSurname"]?></td>
                                    <td><?php echo $user["Title"]?></td>
                                    <td><?php echo $user["Progress"] ?></td>
                                    <td>$<?php echo $user["PurchasePrice"]?> MXN</td>
                                    <td><?php echo $user["PaymentMethod"] ?></td>
                                    <td><?php echo $user["RegistrationDate"]?></td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
            <div class="row resultados__total d-flex justify-content-center align-items-center mt-5">
                <h6 class="col-sm-4 resultados__total-h5 text-center">Total de ventas: <span class="money">$<?php echo $course["TotalSales"] ?> MXN</span></h6>
            </div>
            <div class="df_button-container">
                <div class="df_btn-buy row justify-content-evenly">
                    <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::EDIT_COURSE)?>" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 col-6 col-md-3 col-xxl-2">Editar Curso</a>
                    <form class="col-6 col-md-3 col-xxl-2 p-0" action="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::DEACTIVATE_COURSE) ?>" method="post">
                        <button type="submit" class="df_btn btn py-2 my-4 px-lg-3 py-lg-2 w-100">Desactivar/Activar curso</a>
                    </form>
                </div>
            </div>
        </div>
    </main>