<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script src="https://www.paypal.com/sdk/js?client-id=AQ4oa7jz9s6oBEZYbc31ikw59Y-trJhihGWml4dRuBlnRiQpr8kbVcEBenBZ7icHYtS1Cv6V0kslPyA5&currency=MXN&disable-funding=credit,card"></script>  
<script src="https://cdn.jsdelivr.net/npm/sweet-modal@1.3.2/dist/min/jquery.sweet-modal.min.js"></script>
<script src="<?php echo Template::ROOT_PATH . "views/js/paypal.js"?>"></script>
<script src="<?php echo Template::ROOT_PATH . "views/js/validations.js"?>"></script>
<script src="<?php echo Template::ROOT_PATH . "views/js/jquery.card.js"?>"></script>
<script src="<?php echo Template::ROOT_PATH . "views/js/shoppingcart.js"?>"></script>
