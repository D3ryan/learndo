    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
    <script src="<?php echo Template::ROOT_PATH . "views/js/tokenize2.min.js"?>"></script>
    <script src="<?php echo Template::ROOT_PATH . "views/js/createcourses.js"?>"></script>
    <script src="<?php echo Template::ROOT_PATH . "views/js/general.js"?>"></script>
    <script src="<?php echo Template::ROOT_PATH . "views/js/validations.js"?>"></script>