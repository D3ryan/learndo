<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo Template::ROOT_PATH . "views/css/searchcourses.css"?>">
<title>My Fabric Courses - LearnDo!</title>
<main>
    <div class="container-fluid resultados min-vh-100">
        <h2 class="resultados__h3"> <span class="fabric">Fabrica de aprendizaje:</span> Mis Cursos </h2>
        <div class="resultados__cursos">
            <h6 class="resultados__cursos-h4">Es muy facil saber como la comunidad ha reaccionado a su contenido,
                ¡Seleccione un curso y vea los detalles!</h6>
        </div>
        <div class="row resultados__cursos-row">
            
            <?php foreach($courses as $course) :?>
                <div class="col-sm-3 resultados__cursos-row-col">
                    <div class="card  resultados__cursos-row-col-card">
                        <div class="resultados__cursos-row-col-card-img-opacity">
                            <a href="<?php echo Template::Route(CoursesController::ROUTE, CoursesController::DETAIL_COURSE) . "/" . $course["IdCourse"] ?>"><img class="resultados__cursos-row-col-card-img" src="data:;base64,<?php echo base64_encode($course["Image"]); ?>"></a>
                        </div>
                        <div class="card-body resultados__cursos-row-col-card-body">
                            <h6 class="resultados__cursos-row-col-card-body-Titulo"><?php echo $course["Title"] ?></h6>
                            <div class="row resultados__cursos-row-col-card-body-row">
                                <div class="col-sm-6 class resultados__cursos-row-col-card-body-row-colV ">
                                    <h6 class="resultados__cursos-row-col-card-body-ventas"> Ventas: <span
                                            class="money"> $<?php echo $course["TotalSales"]; ?> MXN</span> </h6>
                                </div>
                                <div class="col-sm-6 resultados__cursos-row-col-card-body-row-colA">
                                    <img src="<?php echo Template::ROOT_PATH . "views/img/personas-llenas-de-grupo.png"?>"
                                        class=" resultados__cursos-row-col-card-body-colA-img">
                                    <span class=" resultados__cursos-row-col-card-body-row-colA-A"><?php echo $course["Students"] ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class=" row resultados__total d-flex justify-content-evenly align-items-center">
                <h6 class="col-sm-4 resultados__total-h5">Ventas por PayPal: <span class="money">$<?php echo $sales["PaypalSales"] ?> MXN</span></h6>
                <h6 class="col-sm-4 resultados__total-h5">Ventas por Tarjeta Débito/Crédito: <span class="money">$<?php echo $sales["CardSales"] ?> MXN </span></h6>
                <h6 class="col-sm-4 resultados__total-h5">Total de ventas: <span class="money">$<?php echo $sales["TotalSales"] ?> MXN</span></h6>
            </div>
        </div>
    </div>
</main>
<?php
    function Scripts(){
        include "views/course/scripts/coursesfabric.scripts.php";
    }
?>