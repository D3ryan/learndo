var levelnumber = 0;
var levelnumberStop = 0

$(document).ready(function () {

    levelnumber = levelnumberStop = parseInt($("#numberoflevels").val()) - 1;

    $("#btn-newlevel").click(function (e) { 
        e.preventDefault();
        newLevelContainer();
    });

    $("#btn-eraselevel").click(function (e){
        e.preventDefault();
        eraseLevelContainer();
    });

    // Debuggeo de input arrays
    // $("#btn-publish").click(function (e) { 
    //     e.preventDefault();
    //     $("input[name='level-title[]']").each(function () { 
    //         console.log($(this).val());
    //     });
    // });

});

function newLevelContainer() {
    levelnumber += 1;

    var levelcontainerHTML = `<div class="Level" id="Level">
    <input type="text" name="IdLevel[]" hidden>
    <div class="create__folder-row-colInfo-level-h6D">
        <div class="form-floating mb-3">
            <input type="text" class="form-control df_level-title df_input" name="leveltitle[${levelnumber}]"
                id="df_level-title" placeholder="titulo del nivel">
            <label for="df_level-title">Titulo del Curso</label>
        </div>
    </div>
    <div class="create__folder-row-colInfo-level-DA">
        <div class="row create__folder-row-colInfo-level-DA-row">
            <div class="col-sm-7 create__folder-row-colInfo-level-DA-row-colD mb-3">
                <div class="form-floating mb-4 h-100">
                    <textarea class="form-control df_textarea-levels df_textarea"
                        placeholder="Aqui va la descripcion" id="df_textarea-levels"
                        name="leveldescription[${levelnumber}]"></textarea>
                    <label for="df_textarea-levels">Descripción del nivel</label>
                </div>
            </div>
            <div class="col-sm-5 create__folder-row-colInfo-level-DA-row-colA">
                <div class="create__folder-row-colInfo-level-DA-row-colAL">
                    <div class="form-floating mb-4 h-100">
                        <textarea class="form-control df_textarea df_textarea-links"
                            placeholder="Aqui va la descripcion" id="df_textarea-links"
                            name="levellinks[${levelnumber}]"></textarea>
                        <label for="df_textarea-links">Links del nivel</label>
                    </div>
                </div>
                <div class="create__folder-row-colInfo-level-DA-row-colAA">
                    <h6 class="create__folder-row-colInfo-level-DA-row-colAA-h6">Imagenes:
                    </h6>
                    <label for="archivos" class="label__archivos"></label>
                    <input type="file" id="archivos" name="multimediafiles[${levelnumber}][]" class="shadow-none archivos form-control form-control-sm 
                    create__folder-row-colInfo-level-DA-row-colAA-file" multiple>
                </div>
                <div class="create__folder-row-colInfo-level-DA-row-colAA">
                    <h6 class="create__folder-row-colInfo-level-DA-row-colAA-h6">Archivos PDF:
                    </h6>
                    <label for="pdf" class="label__archivos"></label>
                    <input type="file" id="pdf" name="pdffiles[${levelnumber}][]" class="shadow-none pdf form-control form-control-sm 
                    create__folder-row-colInfo-level-DA-row-colAA-file" multiple>
                </div>
                <div class="create__folder-row-colInfo-level-DA-row-colAV">
                    <h6 class="create__folder-row-colInfo-level-DA-row-colAV-h6">Video:</h6>
                    <input type="file" id="video" name="videolevel[${levelnumber}]" class="video shadow-none form-control form-control-sm 
                    create__folder-row-colInfo-level-DA-row-colAV-file">
                </div>
            </div>
        </div>
    </div>
    <div class="create__folder-row-colInfo-level-CA">
        <div class="create__folder-row-colInfo-level-CAC df_price-container" id="df_price-container">
            <label for="price"
                class="create__folder-row-colInfo-colCampos-row-colDts-costo-h6">Costo
                total: $</label>
            <input type="number" class="form-control form-control-sm df_input
            create__folder-row-colInfo-colCampos-row-colDts-costo-btn price" id="price"
                name="price[${levelnumber}]">
            <span
                class="ms-2 create__folder-row-colInfo-colCampos-row-colDts-costo-h6">MXN</span>
        </div>
    </div>
</div>`

    $(levelcontainerHTML).appendTo("#levelcontainer").hide().fadeIn("slow");

   setErrorListeners();

    if(levelnumber > levelnumberStop)
        $("#btn-eraselevel").removeClass("d-none");
}

function eraseLevelContainer() {
    levelnumber -= 1;
    if(levelnumber > levelnumberStop )
        $("#levelcontainer").children().last().fadeOut("slow", function () { $("#levelcontainer").children().last().remove();  });
    else if(levelnumber == levelnumberStop){
        $("#levelcontainer").children().last().fadeOut("slow", function () { $("#levelcontainer").children().last().remove();  });
        $("#btn-eraselevel").addClass("d-none");
    }
    else{
        levelnumber = levelnumberStop;
        $("#btn-eraselevel").addClass("d-none");
    }
}


