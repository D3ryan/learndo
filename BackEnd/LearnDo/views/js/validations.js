jQuery.validator.methods.email = function( value, element ) {
    return this.optional(element) || /^([a-zA-ZÁ-ÿ0-9_]+(?:[.-]?[a-zA-ZÁ-ÿ0-9]+)*@[a-zA-ZÁ-ÿ0-9]+(?:[.-]?[a-zA-ZÁ-ÿ0-9]+)*\.[a-zA-ZÁ-ÿ]{2,7})$/.test(value);
}

jQuery.validator.addMethod("alphanumericspaces", function (value, element) { 
    return this.optional(element) || /^[A-Za-zÁ-ÿ0-9]+(\s+[A-Za-zÁ-ÿ0-9]+)*$/.test(value);
}, "Schoolname just accepts alphanumeric and spaces")



jQuery.validator.addMethod("names", function(value, element) {
    return this.optional(element) || /^[A-zÁ-ÿ]+([-\s]{1}[A-zÁ-ÿ]+)*$/i.test(value);
}, "Letters only please");



jQuery.validator.addMethod("surnames", function(value, element) {
    return this.optional(element) || /^[A-zÁ-ÿ]+$/i.test(value);
}, "Letters only please");

jQuery.validator.addMethod("complexpassword", function(value, element) {
    return this.optional(element) || /^(?=.*[a-zá-ÿ])(?=.*[A-ZÁ-Ý])(?=.*\d)(?=.*[@$"#/=':;,._\-+¡!%*¿?&{}\[\]])[A-zÁ-ÿ\d@$"#/=':;,._\-+¡!%*¿?&{}\[\]]{8,}$/.test(value);
}, "Bad password format");


function setErrorListeners(){
    $("input.df_level-title").each(function () {
        $(this).rules('add', {
            required: true, 
            messages: {
                required: "Por favor introduzca el titulo del nivel"
            }
        });
    });

    $("textarea.df_textarea-levels").each(function () {
        $(this).rules('add', {
            required: true, 
            messages: {
                required: "Por favor introduzca la descripcion del nivel"
            }
        });
    });

    // $("textarea.df_textarea-links").each(function () {
    //     $(this).rules('add', {
    //         required: true, 
    //         messages: {
    //             required: "Por favor introduzca links del nivel"
    //         }
    //     });
    // });


    $("input.archivos").each(function () {
        $(this).rules('add', {
            // required: true, 
            extension: "jpg|png|gif|jpeg",
            messages: {
                // required: "Por favor introduzca algun archivo, jpg, png, gif y jpeg",
                extension: "Por favor introduzca solo archivos, jpg, png, gif y jpeg"
            }
        });
    });

    $("input.pdf").each(function () {
        $(this).rules('add', {
            // required: true, 
            extension: "pdf",
            messages: {
                // required: "Por favor introduzca algun archivo pdf",
                extension: "Por favor introduzca solo archivos pdf"
            }
        });
    });


    $("input.video").each(function () {
        $(this).rules('add', {
            required: true,
            extension: "mp4|mov|avi",
            messages: {
                required: "Por favor introduzca algun video",
                extension: "Por favor introduzca solo archivos mp4, mov o avi"
            }
        });
    });

    $("input.price").each(function () {
        $(this).rules('add', {
            required: true,
            currency: ["", false],
            messages: {
                required: "Por favor introduzca el precio del nivel",
                currency: "Por favor solo introduzca numeros"
            }
        });
    });
}


$(document).ready(function () {
    /* ------------------- Login ------------------ */
    $("#LoginForm").validate({
        errorElement: "p",
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Por favor ingrese su correo.",
                email: "Por favor ingrese un formato correcto."
            },
            password: {
                required: "Por favor ingrese su contraseña."
            }
        }
    });

    /* ------------------- Signup ------------------ */
    $("#SignupForm").validate({
        errorElement: "p",
        ignore: [],
        rules: {
            avatar: {
                required: true,
                extension: "jpg|png|gif|jpeg"
            },
            names: {
                required: true,
                names: true
            },
            firstsurname: {
                required: true,
                surnames: true
            },
            secondsurname: {
                required: true,
                surnames: true
            },
            gender: {
                required: true
            },
            birthdate:{
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                complexpassword: true
            }

        },
        messages: {
            avatar: {
                required: "Por favor ingrese alguna foto",
                extension: "Solo se aceptan imagenes, jpg, png y gif"
            },
            names: {
                required: "Por favor ingrese su nombre",
                names: "Por favor ingrese correctamente su nombre(s)"
            },
            firstsurname: {
                required: "Por favor ingrese su apellido paterno",
                surnames: "Por favor ingrese su apellido paterno correctamente"
            },
            secondsurname:{
                required: "Por favor ingrese su apellido materno",
                surnames: "Por favor ingrese su apellido materno correctamente"
            },
            gender:{
                required: "Por favor seleccione una opcion"
            },
            birthdate:{
                required: "Por favor seleccione su fecha de cumpleaños"
            },
            email: {
                required: "Por favor ingrese su correo.",
                email: "Por favor ingrese un formato correcto."
            },
            password: {
                required: "Por favor ingrese su contraseña.",
                complexpassword: "La contraseña debe tener 8 caracteres, 1 mayúscula, 1 caracter especial, 1 numero al menos."
            }

        },

        errorPlacement:  function(error, element) {
            if (element.attr("name") == "avatar")
            {
                error.insertAfter("#df_img-container");
            }
            else{
                error.insertAfter(element);
            }
        }

    });

    /* ------------------- Profile ------------------ */
    $("#ProfileForm").validate({
        errorElement: "p",
        ignore: [],
        rules: {
            avatar: {
                // required: true,
                extension: "jpg|png|gif|jpeg"
            },
            names: {
                required: true,
                names: true
            },
            firstsurname: {
                required: true,
                surnames: true
            },
            secondsurname: {
                required: true,
                surnames: true
            },
            gender: {
                required: true
            },
            birthdate:{
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                complexpassword: true
            }

        },
        messages: {
            avatar: {
                required: "Por favor ingrese alguna foto",
                extension: "Solo se aceptan imagenes, jpg, png y gif"
            },
            names: {
                required: "Por favor ingrese su nombre",
                names: "Por favor ingrese correctamente su nombre(s)"
            },
            firstsurname: {
                required: "Por favor ingrese su apellido paterno",
                surnames: "Por favor ingrese su apellido paterno correctamente"
            },
            secondsurname:{
                required: "Por favor ingrese su apellido materno",
                surnames: "Por favor ingrese su apellido materno correctamente"
            },
            gender:{
                required: "Por favor seleccione una opcion"
            },
            birthdate:{
                required: "Por favor seleccione su fecha de cumpleaños"
            },
            email: {
                required: "Por favor ingrese su correo.",
                email: "Por favor ingrese un formato correcto."
            },
            password: {
                required: "Por favor ingrese su contraseña.",
                complexpassword: "La contraseña debe tener 8 caracteres, 1 mayúscula, 1 caracter especial, 1 numero al menos."
            }

        },

        errorPlacement:  function(error, element) {
            if (element.attr("name") == "avatar")
            {
                error.insertAfter("#df_img-container");
            }
            else{
                error.insertAfter(element);
            }
        }

    });

    /* ------------------- School ------------------ */
    $("#SchoolForm").validate({
        errorElement: "p",
        ignore: [],
        rules: {
            avatar: {
                extension: "jpg|png|gif|jpeg"
            },
            schoolname: {
                required: true,
                alphanumericspaces: true
            }
        },
        messages: {
            avatar: {
                extension: "Solo se aceptan imagenes, jpg, png y gif"
            },
            schoolname: {
                required: "Por favor ingrese el nombre de su escuela",
                alphanumericspaces: "Por favor ingrese solamente caracteres alfanumericos y espacios"
            }
        },

        errorPlacement:  function(error, element) {
            if (element.attr("name") == "avatar")
            {
                error.insertAfter("#df_img-container-school");
            }
            else{
                error.insertAfter(element);
            }
        }

    });

    /* ------------------- ShoppingForm ------------------ */
    $("#ShoppingForm").validate({
        errorElement: "p",
        groups:{
            validationgroup: "course levels[]"
        },
        rules: {
            course: {
                require_from_group: [1,".validationgroup"]
            },
            'levels[]':{
                require_from_group: [1,".validationgroup"]
            }
        },
        messages: {
            course:{
                require_from_group: "Elija algun nivel o el curso completo"
            },
            'levels[]': {
                require_from_group: "Elija algun nivel o el curso completo"
            }
        },
        highlight: function(element, errorClass) {
            $("input[name='levels[]']").addClass(errorClass);
            $("#df_completeCourse").addClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $("input[name='levels[]']").removeClass(errorClass);
            $("#df_completeCourse").removeClass(errorClass);
        },
        errorPlacement:  function(error, element) {
            error.insertBefore("#df_shopping-wrapper");
        }
    });

    /* ------------------- course Presentation ------------------ */
    $("#CoursePresentationForm").validate({
        errorElement: "p",
        rules: {
            rating:{
                required: true,
                digits: true,
                range: [0, 10]
            },
            commentary: {
                required: true
            }
        },
        messages: {
            rating:{
                required: "Por favor escriba una calificacion del 0 al 10",
                digits: "Solo se admiten numeros en este campo",
                range: "Recuerde que la calificacion es de 0 a 10"
            },
            commentary:{
                required: "Por favor escriba un comentario"
            }
        }

    });
    
    /* ------------------- Create Course ------------------ */
    $("#createCourseForm").validate({
        errorElement: "p",
        ignore: [],
        rules: {
            image: {
                required: true,
                extension: "jpg|png|gif|jpeg"
            },
            'category[]': {
                required: true
            },
            title: {
                required: true
            },
            description: {
                required: true
            },
            totalprice: {
                required: true,
                currency: ["", false]
            }
        },
        messages: {
            image: {
                required: "Por favor ingrese alguna foto",
                extension: "Solo se aceptan imagenes, jpg, png y gif"
            },
            'category[]': {
                required: "Por favor seleccione al menos una categoria o cree una"
            },
            title: {
                required: "Por favor introduzca el nombre de su curso"
            },
            description: {
                required: "Por favor introduzca la descripcion de su curso"
            },
            totalprice: {
                required: "Por favor introduzca el precio total del curso",
                currency: "Por favor solo introduzca numeros"
            }
        },

        highlight: function(element, errorClass) {
            if($(element).attr("name") === "category[]")
                $(".tokens-container").addClass(errorClass);

            $(element).addClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            if($(element).attr("name") === "category[]")
                $(".tokens-container").removeClass(errorClass);
            $(element).removeClass(errorClass)
        },

        errorPlacement:  function(error, element) {
            if (element.attr("name") == "image")
                error.insertAfter("#df_img-container");
            else if(element.attr("name") == "category[]")
                error.insertAfter("#df_category-container");
            else if(element.attr("name") == "totalprice")
                error.insertAfter("#df_price-container");
            else{
                error.insertAfter(element);
            }
        }

    });

    /* ------------------- Create level ------------------ */
    $("#createLevelForm").validate({
        errorElement: "p",
        errorPlacement:  function(error, element) {
            if (element.hasClass("price")){
                error.insertAfter($(element).parent());
            }
            else{
                error.insertAfter(element);
            }
        }

    });

    /* ------------------- Edit level ------------------ */
    $("#editLevelForm").validate({
        errorElement: "p",
        errorPlacement:  function(error, element) {
            if (element.hasClass("price")){
                error.insertAfter($(element).parent());
            }
            else{
                error.insertAfter(element);
            }
        }

    });

    setErrorListeners();

    $("#categoryForm").validate({
        errorElement: "p",
        rules:{
            'category-title': {
                required: true
            },
            'category-description' : {
                required: true
            }
        },
        messages: {
            'category-title': {
                required: "Por favor ingrese algún título para la categoría",
            },
            'category-description': {
                required: "Por favor ingrese alguna descripción para la categoría"
            }
        },

    })


    /* ------------------- Edit Course ------------------ */
    $("#editCourseForm").validate({
        errorElement: "p",
        ignore: [],
        rules: {
            image: {
                extension: "jpg|png|gif|jpeg"
            },
            'category[]': {
                required: true
            },
            title: {
                required: true
            },
            description: {
                required: true
            },
            totalprice: {
                required: true,
                currency: ["", false]
            }
        },
        messages: {
            image: {
                extension: "Solo se aceptan imagenes, jpg, png y gif"
            },
            'category[]': {
                required: "Por favor seleccione al menos una categoria o cree una"
            },
            title: {
                required: "Por favor introduzca el nombre de su curso"
            },
            description: {
                required: "Por favor introduzca la descripcion de su curso"
            },
            totalprice: {
                required: "Por favor introduzca el precio total del curso",
                currency: "Por favor solo introduzca numeros"
            }
        },

        highlight: function(element, errorClass) {
            if($(element).attr("name") === "category[]")
                $(".tokens-container").addClass(errorClass);

            $(element).addClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            if($(element).attr("name") === "category[]")
                $(".tokens-container").removeClass(errorClass);
            $(element).removeClass(errorClass)
        },

        errorPlacement:  function(error, element) {
            if (element.attr("name") == "image")
                error.insertAfter("#df_img-container");
            else if(element.attr("name") == "category[]")
                error.insertAfter("#df_category-container");
            else if(element.attr("name") == "totalprice")
                error.insertAfter("#df_price-container");
            else{
                error.insertAfter(element);
            }
        }

    });



});
