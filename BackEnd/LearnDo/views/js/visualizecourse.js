$(document).ready(function () {
    $("#finishlevel").click(function (e) {

        let IdVisualizedLevel = $("#IdVisualizedLevel").val();
        $("#status").text("Estatus: Terminado");
        $.ajax({
            url: '/levels/progress',
            type: 'post',
            data: {
                IdLevel: IdVisualizedLevel
            },
            dataType: 'json',
            success: function (response) {
                $.sweetModal({
                content: '¡Nivel terminado, sigue asi, selecciona tu siguiente nivel!',
                icon: $.sweetModal.ICON_SUCCESS
                }); 
            },
            error: function(data) {
                $.sweetModal({
                    content: '¡Al parecer hubo un error!, intentalo mas tarde',
                    icon: $.sweetModal.ICON_ERROR
                }); 
            },
            failure: function (data) { 
                $.sweetModal({
                    content: '¡Al parecer hubo un error!, intentalo mas tarde',
                    icon: $.sweetModal.ICON_ERROR
                }); 
            }
        });
    });
});