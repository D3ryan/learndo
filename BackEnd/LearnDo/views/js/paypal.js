$(document).ready(function () {
    
    let totalprice = 0;

    let levelscheckbox = $("input[name='levels[]']");
    let levelsPrice = $("input[name='levelprice[]'")
    let levels = [];
    let course = {};

    $("input[type='checkbox']").change(function (e) { 
        totalprice = $("#totalprice").val();

        course = {
            IdCourse: $("input[name='course']").val(),
            Price: $("input[name='courseprice']").val(),
            Checked: $("input[name='course']").prop("checked")
        }; 

        levels = [];

        for (let i = 0; i < levelscheckbox.length; i++) {
            const level = levelscheckbox[i];
            levels.push({
                IdLevel: $(level).val(),
                Price: $(levelsPrice[i]).val(),
                Checked: level.checked
            });
        }

        // console.log(levels);
        // console.log(course);
    });

    paypal.Buttons({
        style: {
            layout: 'vertical',
            color:  'blue',
            shape:  'pill',
            label:  'paypal',
            tagline: 'false'
        },
        env:'sandbox',

        createOrder: function(data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount:{
                        value: totalprice
                    }
                }]
            });
        },

        onApprove: function(data, actions) {
            return actions.order.capture().then(function(orderData) {
                $.ajax({
                    url: 'courses/shoppingcart',
                    type: 'post',
                    
                    data: {
                        PaymentMethod: 1,
                        Course: course,
                        Levels: levels
                    },
                    dataType: 'json',
                    success: function (response) {
                        if(typeof response.redirection === "undefined"){
                            $.sweetModal({
                                content: '¡Muchas gracias por su compra!, lo redigiremos a ver sus cursos en un momento',
                                icon: $.sweetModal.ICON_SUCCESS,
                                timeout: 2500,
                                onClose: function () { 
                                    window.location='/LearnDo/Backend/LearnDo/courses/mycourses'
                                }
                            });
                        } else window.location='/LearnDo/Backend/LearnDo/users/login'
                    },
                    error: function (data) {
                        $.sweetModal({
                            content: '¡Al parecer hubo un error!, intentalo mas tarde',
                            icon: $.sweetModal.ICON_ERROR
                        }); 
                    },
                    failure: function (data) {
                        $.sweetModal({
                            content: '¡Al parecer hubo un error!, intentalo mas tarde',
                            icon: $.sweetModal.ICON_ERROR
                        }); 
                    },
                });
            });
        }
    }).render('#paypal-button-container');
});