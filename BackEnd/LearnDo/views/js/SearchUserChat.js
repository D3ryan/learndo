$(document).ready(function () {
    
    $('#SearchChat').click(function(e) {
        $(this).attr('placeholder','');
        $('#buzonDiv').hide();
        $('#BackSearch').removeAttr('hidden');
        $('#tablaUsers').show();
    });
    
    $('#BackSearch').click(function(e) {
        $(this).attr('hidden','');
        $('#SearchChat').attr('placeholder','Buscar');
        $('#buzonDiv').show();
        $('#tablaUsers').hide();
    });

});



function obtener_registros(UserSchool){
    $.ajax({
        url: '/chat/search',
        type: 'POST',
        dataType: 'json',
        data: {UserSchool: UserSchool},
        
        success: function(resultado){
            
            $("#tablaUsers").html(function() {
                var tabla;
                var array = resultado;
                
                if (array != null){
                    tabla = "<table>";
                    for (i=0; i < array.length; i++) {
                        if(array[i].SchoolName == null){
                            tabla+="<tr> <td> <a class='vivian_a' href="+ "/chat/click/" + array[i].IdUser +"> <img class=" + "chat__row-colM-entrada-users-img " + " src=data:;base64," +  array[i].Avatar + "> " + array[i].Names + " " + array[i].FirstSurname + " " + array[i].SecondSurname +  " </a> </td> </tr>";
                        }
                        else{
                            tabla+="<tr> <td> <a class='vivian_a' href="+ "/chat/click/"+ array[i].IdUser +"> <img class=" + "chat__row-colM-entrada-users-img " + " src=data:;base64," +  array[i].Avatar + "> " + array[i].Names + " " + array[i].FirstSurname + " " + array[i].SecondSurname + " (" + array[i].SchoolName + ") </a> </td> </tr>";
                        }
                    }
                    tabla+= "</table>";
                }

                return tabla;
            });
        },
        error: function (data) {
            console.log("Error de error");
        },
        failure: function (data) {
                console.log("Failure de fail");
        },
    })
}

$(document).on('keyup', '#SearchChat', function(){

    var valorBusqueda=$(this).val();

    obtener_registros(valorBusqueda);

});