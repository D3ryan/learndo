$(document).ready(function () {

    let totalprice = 0;

    let levelscheckbox = $("input[name='levels[]']");
    let levelsPrice = $("input[name='levelprice[]'")
    let levels = [];
    let course = {};

    $("#df_completeCourse").click(function (e) { 
        toggleLevelsCheckbox($("#df_completeCourse"));
        calculateTotalPrice();
    });

    $("input[name='levels[]']").click(function (e) {
        checkLevelCheckboxes();
        calculateTotalPrice();
    });

    $(".df_checkbox-label").click(function (e) { 
        e.preventDefault();
    });

    $("input[type='checkbox']").change(function (e) { 
        e.preventDefault();
        if($("#ShoppingForm").valid()){
            $("#paypal-button-container").show();
            $("#cardbutton").show();
        } else{
            $("#paypal-button-container").hide();
            $("#cardbutton").hide();
            $("#cardpayment-container").hide();
        }

        totalprice = $("#totalprice").val();

        course = {
            IdCourse: $("input[name='course']").val(),
            Price: $("input[name='courseprice']").val(),
            Checked: $("input[name='course']").prop("checked")
        }; 

        levels = [];

        for (let i = 0; i < levelscheckbox.length; i++) {
            const level = levelscheckbox[i];
            levels.push({
                IdLevel: $(level).val(),
                Price: $(levelsPrice[i]).val(),
                Checked: level.checked
            });
        }
    });

    $("#cardpayment-button").click(function (e) { 
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/courses/shoppingcart",
            data: {
                PaymentMethod: 2,
                Course: course,
                Levels: levels
            },
            dataType: "json",
            success: function (response) {
                if(typeof response.redirection === "undefined"){
                    $.sweetModal({
                        content: '¡Muchas gracias por su compra!, lo redigiremos a ver sus cursos en un momento',
                        icon: $.sweetModal.ICON_SUCCESS,
                        timeout: 2500,
                        onClose: function () { 
                            window.location='/courses/mycourses'
                        }
                    });
                } else window.location='/users/login'
            },
            error: function (data) {
                $.sweetModal({
                    content: '¡Al parecer hubo un error!, intentalo mas tarde',
                    icon: $.sweetModal.ICON_ERROR
                });
            },
            failure: function (data) {
                $.sweetModal({
                    content: '¡Al parecer hubo un error!, intentalo mas tarde',
                    icon: $.sweetModal.ICON_ERROR
                }); 
            },
        });
    });

    $('#cardForm').card({
        container: '.card-wrapper',
        // number formatting
      formatting: true,
    
      // selectors
      formSelectors: {
        numberInput: 'input[name="number"]',
        expiryInput: 'input[name="expiry"]',
        cvcInput: 'input[name="cvc"]',
        nameInput: 'input[name="name"]'
      },
      cardSelectors: {
        cardContainer: '.jp-card-container',
        card: '.jp-card',
        numberDisplay: '.jp-card-number',
        expiryDisplay: '.jp-card-expiry',
        cvcDisplay: '.jp-card-cvc',
        nameDisplay: '.jp-card-name'
      },
    
      // custom messages
      messages: {
        validDate: 'valid\nthru',
        monthYear: 'month/year'
      },
    
      // custom placeholders
      placeholders: {
        number: '&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;',
        cvc: '&bull;&bull;&bull;',
        expiry: '&bull;&bull;/&bull;&bull;',
        name: 'Full Name'
      },
    
      // enable input masking 
      masks: {
        cardNumber: false
      },
    
      // valid/invalid CSS classes
      classes: {
        valid: 'jp-card-valid',
        invalid: 'jp-card-invalid'
      },
    
      // debug mode
      debug: false
    }); 

    $('#cardbutton').click(function (e) { 
        e.preventDefault();
        $("#cardpayment-container").slideToggle('slow');
    });
});



function toggleLevelsCheckbox() { 
    let levelsCheckbox = $("input[name='levels[]']");
    if($("#df_completeCourse").prop("checked")){
        for (const level of levelsCheckbox) {
            $(level).prop("checked", false);
            $(level).hide();
        }
    } else {
        for (const level of levelsCheckbox) {
            $(level).show();
        }
    }
}

function checkLevelCheckboxes() {
    let levelsCheckbox = $("input[name='levels[]']");
    let levelsChecked = 0;

    for (const level of levelsCheckbox) {
        if(level.checked){
            levelsChecked++;
        }
    }

    if(levelsChecked > 0){
        $("#df_completeCourse").prop("checked", false);
        $("#df_completeCourse").hide();
    }
    else{
        $("#df_completeCourse").show();
    }
}


function calculateTotalPrice() {
    let checkboxes = $("input[type='checkbox']");
    let sum = 0;
    for (const checkbox of checkboxes) {
        if(checkbox.checked){
            sum += parseInt($(checkbox).attr('levelprice'));
        }
    }
    $("#df_totalPrice").text(`$${sum.toString()} MXN`);
    $("#totalprice").val(sum.toString());
}