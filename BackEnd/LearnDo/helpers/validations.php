<?php

class Validations {

    public static function validatePassword($password){

        $error = false;
        $pattern = '/^(?=.*[a-zá-ÿ])(?=.*[A-ZÁ-Ý])(?=.*\d)(?=.*[@$"#\=\':;,._\-+¡!%*¿?&{}\[\]])[A-zÁ-ÿ\d@$"#\=\':;,._\-+¡!%*¿?&{}\[\]]{8,}$/';
        
        if(empty($password) || !preg_match($pattern, $password))
            $error = true;

        return $error;
    }

    public static function validateUserEmail($email){

        $error = false;
        $pattern = '/^([a-zA-ZÁ-ÿ0-9_]+(?:[.-]?[a-zA-ZÁ-ÿ0-9]+)*@[a-zA-ZÁ-ÿ0-9]+(?:[.-]?[a-zA-ZÁ-ÿ0-9]+)*\.[a-zA-ZÁ-ÿ]{2,7})$/';

        if(empty($email) || !preg_match($pattern, $email))
            $error = true;
        
        return $error;
        
    }

    public static function validateUserName($name){
        
        $error = false;
        
        $pattern = '/^[A-zÁ-ÿ]+([-\s]{1}[A-zÁ-ÿ]+)*$/';

        if(empty($name) || !preg_match($pattern, $name))
            $error = true;
        
        return $error;
    }

    public static function validateUserSurname($surname){
        
        $error = false;
        
        $pattern = '/^[A-zÁ-ÿ]+$/';

        if(empty($surname) || !preg_match($pattern, $surname))
            $error = true;
        
        return $error;
    }

    public static function validateUserBirthdate($date){
        
        $error = false;

        $now = new DateTime();
        $birthdate = DateTime::createFromFormat("Y-m-d", $date);

        if($birthdate == false)
            $error = true;
        elseif($now < $date)
            $error = true;

        return $error;
    }

    public static function validateUserGender($gender){
        $error = false;

        if(intval($gender) != 1 && intval($gender) != 2 && intval($gender) != 3)
            $error = true;
        
        return $error;
    }

    public static function validateAlphanumericAndSpaces($string){
        $error = false;

        $pattern = '/^[A-Za-zÁ-ÿ0-9]+(\s+[A-Za-zÁ-ÿ0-9]+)*$/';

        if(empty($string) || !preg_match($pattern, $string))
            $error = true;


        return $error;
    }

    public static function validateNullEmptyString($string){
        $error = false;

        if(empty($string) || is_null($string))
            $error = true;
        
        return $error;
    }

    
    public static function validateNull($object){
        $error = false;

        if(is_null($object))
            $error = true;
        
        return $error;
    }


    
    public static function validateNumeric($string){
        $error = false;

        if(!is_numeric($string))
            $error = true;
        
        return $error;
    }

    public static function throwInputError(){
        return throw new Exception("Parece que hubo un error al ingresar tus datos... sera mejor regresar.");
    }

    public static function throwGeneralError(){
        return throw new Exception("Parece que hubo un error... sera mejor regresar.");
    }


}