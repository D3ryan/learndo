<?php

abstract class File{
    protected const EXT = array('gif', 'png', 'jpg', 'jpeg');
    protected const MIME_TYPE = array("image/gif", "image/png", "image/jpg", "image/jpeg");

    protected const EXT_VIDEO = array('mp4', 'mov', 'avi');
    protected const MIME_TYPE_VIDEO = array("video/mp4", "video/quicktime", "video/x-msvideo");

    protected const EXT_PDF = "pdf";
    protected const MIME_TYPE_PDF = "application/pdf";

    //These methods expect to recieve a parameter like $_FILES["name"]
    public static function ValidateType($file){
        $res = false;

        if(!is_null($file)){
            $ext = pathinfo($file["name"], PATHINFO_EXTENSION);
            $mimetype = $file["type"];
            if(in_array($mimetype, self::MIME_TYPE) && in_array($ext, self::EXT)){
                $res=true;
            }
        }
        return $res;
    }

    public static function ValidateVideoType($file){
        $res = false;

        if(!is_null($file)){
            $ext = pathinfo($file["name"], PATHINFO_EXTENSION);
            $mimetype = $file["type"];
            if(in_array($mimetype, self::MIME_TYPE_VIDEO) && in_array($ext, self::EXT_VIDEO)){
                $res=true;
            }
        }
        return $res;
    }

    public static function ValidatePDFType($file){
        $res = false;

        if(!is_null($file)){
            $ext = pathinfo($file["name"], PATHINFO_EXTENSION);
            $mimetype = $file["type"];
            if(strcmp($mimetype, self::MIME_TYPE_PDF) == 0 && strcmp($ext, self::EXT_PDF) ==0){
                $res=true;
            }
        }
        return $res;
    }

    public static function MakeFileToBinary($file){

        if(!is_null($file)){
            $fileToConvert = fopen($file["tmp_name"], "r");
            $binaryFile = fread($fileToConvert, $file["size"]);
        }
        
        return $binaryFile;
    }

    public static function reArrayFiles(&$file_post)
    {

        $file_array = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_array[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_array;
    }

    public static function UploadVideo($video){

        $userDirectory = "C:/xampp/htdocs" . Template::ROOT_PATH . "resources/videos/" . UserSession::getCurrentSchoolId();
        $serverDirectory = Template::ROOT_PATH . "resources/videos/" . UserSession::getCurrentSchoolId();
        
        $ext = pathinfo($video["name"], PATHINFO_EXTENSION);

        if(!file_exists($userDirectory)){
            mkdir($userDirectory);
        }

        $uniqueId = uniqid();

        $destination = $userDirectory . "/" . $uniqueId . "." . $ext;
        $serverDestination = $serverDirectory . "/" . $uniqueId . "." . $ext;

        move_uploaded_file($video["tmp_name"], $destination);

        return $serverDestination;
    }

    public static function UploadPDF($pdf){
        $userDirectory = "C:/xampp/htdocs" . Template::ROOT_PATH . "resources/pdf/" . UserSession::getCurrentSchoolId();
        $serverDirectory = Template::ROOT_PATH . "resources/pdf/" . UserSession::getCurrentSchoolId();
        
        $ext = pathinfo($pdf["name"], PATHINFO_EXTENSION);

        if(!file_exists($userDirectory)){
            mkdir($userDirectory);
        }

        $uniqueId = uniqid();

        $destination = $userDirectory . "/" . $uniqueId . "." . $ext;
        $serverDestination = $serverDirectory . "/" . $uniqueId . "." . $ext;

        move_uploaded_file($pdf["tmp_name"], $destination);

        return $serverDestination;
    }
}