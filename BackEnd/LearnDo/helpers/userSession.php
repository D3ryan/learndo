<?php

abstract class UserSession{

     public static function startSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function setCurrentUserId($idUser){
        self::startSession();
        $_SESSION['idUser'] = $idUser;
    }

    public static function getCurrentUserId(){
        self::startSession();
        $userId = null;
        if( isset($_SESSION['idUser']) )
            $userId = intval($_SESSION['idUser']);
            
        return $userId;
    }

    public static function setCourseId($idCourse){
        self::startSession();
        $_SESSION['idCourse'] = $idCourse;
    }

    public static function getCourseId(){
        self::startSession();
        $courseId = null;
        if(isset($_SESSION['idCourse']))
            $courseId = intval($_SESSION['idCourse']);
        
        return $courseId;
    }

    
    public static function setCurrentSchoolId($schoolId){
        self::startSession();
        $_SESSION['idSchool'] = $schoolId;
    }

    public static function getCurrentSchoolId(){
        self::startSession();
        $schoolId = null;
        if( isset($_SESSION['idSchool']) )
            $schoolId = intval($_SESSION['idSchool']);

        return $schoolId;
    }

    public static function closeSession(){
        self::startSession();
        session_unset();
        session_destroy();
    }

}